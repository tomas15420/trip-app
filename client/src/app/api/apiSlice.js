import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { Mutex } from "async-mutex";
import { logOut, setCredentials } from "../../features/auth/authSlice";

const mutex = new Mutex();
const baseQuery = fetchBaseQuery({
	baseUrl: process.env.REACT_APP_API_URL,
	credentials: "include",
	prepareHeaders: (headers, { getState }) => {
		const token = getState().auth.token;
		if (token) {
			headers.set("authorization", "Bearer " + token);
		}
		return headers;
	},
});

const baseQueryWithReauth = async (args, api, extraOptions) => {
	await mutex.waitForUnlock();
	let result = await baseQuery(args, api, extraOptions);
	if (result?.error?.status === 401) {
		if (!mutex.isLocked()) {
			const release = await mutex.acquire();
			try {
				console.log("sending refresh token");
				const refreshResult = await baseQuery({ url: "/auth/refreshToken", method: "POST" }, api, extraOptions);
				if (refreshResult.data) {
					const { accessToken, user } = refreshResult.data;
					api.dispatch(setCredentials({ token: accessToken, user }));
				} else {
					console.log("loggin out");
					api.dispatch(logOut());
				}
				result = await baseQuery(args, api, extraOptions);
			} finally {
				release();
			}
		} else {
			await mutex.waitForUnlock();
			result = await baseQuery(args, api, extraOptions);
		}
	}
	return result;
};

export const apiSlice = createApi({
	baseQuery: baseQueryWithReauth,
	tagTypes: ["Trip", "Comment", "User", "Follower", "Following", "Reaction", "Photo", "Rating"],
	endpoints: (builder) => ({}),
});
