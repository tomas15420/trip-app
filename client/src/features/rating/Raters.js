import React, { useState } from "react";
import { useGetRatingsByTripIdQuery } from "./ratingApiSlice";
import LoadingSpinner from "../../components/LoadingSpinner";
import { ListGroup } from "react-bootstrap";
import Stars from "../../components/Stars";
import { Link } from "react-router-dom";
import CustomPagination from "../../components/CustomPagination";

const Raters = ({ trip, ...props }) => {
	const [page, setPage] = useState(1);

	const PER_PAGE = 10;

	const {
		data: ratings,
		isLoading,
		isSuccess,
		isError,
		error,
	} = useGetRatingsByTripIdQuery({
		tripId: trip?.id,
		limit: PER_PAGE,
		offset: (page - 1) * PER_PAGE,
	});

	const handleChangePage = (page) => {
		setPage(page);
	};

	if (isLoading) return <LoadingSpinner />;
	else if (isError) return <p>Error: {error}</p>;
	else if (isSuccess && ratings)
		return (
			<>
				<ListGroup {...props}>
					{ratings.raters.map((rater) => {
						return (
							<ListGroup.Item key={rater.ratings.id} className="d-flex justify-content-between">
								<Link to={`/users/${rater.id}`}>{rater.username}</Link>
								<Stars maxStars={5} stars={rater.ratings.stars} size="xs" />
							</ListGroup.Item>
						);
					})}
				</ListGroup>
				{ratings.total > PER_PAGE && (
					<CustomPagination
						className="d-flex justify-content-center"
						size="sm"
						active={page}
						perPage={PER_PAGE}
						count={ratings.total}
						handleChangePage={handleChangePage}
					/>
				)}
			</>
		);
	return "";
};

export default Raters;
