import React, { useState } from "react";
import { useDeleteRatingMutation, useRateTripMutation } from "./ratingApiSlice";
import Stars from "../../components/Stars";
import Raters from "./Raters";
import { Badge, Button, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import Error from "../../components/Error";

const Rating = ({ trip }) => {
	const rating = trip?.avgStars || 0;
	const MAX_STARS = 5;

	const [error, setError] = useState("");
	const [rateTrip] = useRateTripMutation();
	const [deleteRating] = useDeleteRatingMutation();

	const handleStarClick = async (val) => {
		const stars = val + 1;
		if (stars > 0 && stars <= 5) {
			try {
				await rateTrip({ tripId: trip.id, stars }).unwrap();
			} catch (e) {
				setError(e);
			}
		}
	};

	const handleDeleteRating = async () => {
		try {
			await deleteRating(trip.id).unwrap();
		} catch (e) {
			setError(e);
		}
	};

	return (
		<Card>
			<Card.Body className="m-0 p-0">
				<div className="d-flex flex-column">
					{trip?.avgStars && <Badge className="p-4 fs-1 rounded-top rounded-0">{parseFloat(((rating / MAX_STARS) * 100).toFixed(2))}%</Badge>}
					<Error className="m-3" error={error} onClose={() => setError("")} />
					<Stars
						className="align-self-center mt-3"
						stars={trip?.userStars || 0}
						maxStars={MAX_STARS}
						size="xl"
						handleClick={handleStarClick}
					/>
					<div className="align-self-center mt-2 mb-3">
						{trip?.userStars && (
							<Button variant="danger" size="sm" onClick={handleDeleteRating}>
								<FontAwesomeIcon icon={faXmark} />
								<span className="ms-1">odebrat hodnocení</span>
							</Button>
						)}
					</div>
				</div>
				<hr className="m-0" />

				<div className="mt-3">
					{trip?.avgStars ? <Raters trip={trip} variant="flush" /> : <p className="mt-2 px-3">Výlet zatím nikdo neohodnotil</p>}
				</div>
			</Card.Body>
		</Card>
	);
};

export default Rating;
