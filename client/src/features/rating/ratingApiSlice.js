import { apiSlice } from "../../app/api/apiSlice";

export const ratingApiSlice = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        getRatingsByTripId: builder.query({
            query: ({ tripId, ...filters }) => {
                const params = Object.entries(filters)
                    .filter(([key, value]) => value !== "")
                    .map(([key, value]) => `${key}=${value}`)
                    .join("&");
                return {
                    url: `/trips/${tripId}/ratings${
                        params ? `?${params}` : ""
                    }`,
                };
            },
            providesTags: (result, error, arg) =>
                result
                    ? [
                          ...result?.raters.map(({ ratings }) => ({
                              type: "Rating",
                              id: ratings.id,
                          })),
                          "Rating",
                      ]
                    : ["Rating"],
        }),
        rateTrip: builder.mutation({
            query: ({ tripId, stars }) => ({
                method: "POST",
                url: `/trips/${tripId}/ratings`,
                body: { stars },
            }),
            invalidatesTags: (result, error, { tripId }) => [
                "Rating",
                { type: "Trip", id: tripId },
            ],
        }),
        deleteRating: builder.mutation({
            query: (tripId) => ({
                method: "DELETE",
                url: `/trips/${tripId}/ratings`,
            }),
            invalidatesTags: (result, error, { tripId }) => [
                "Rating",
                { type: "Trip", id: tripId },
            ],
        }),
    }),
});

export const { useGetRatingsByTripIdQuery, useRateTripMutation, useDeleteRatingMutation } =
    ratingApiSlice;
