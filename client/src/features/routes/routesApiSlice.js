import { apiSlice } from "../../app/api/apiSlice";

export const routeApiSlice = apiSlice.injectEndpoints({
	endpoints: (builder) => ({
		getRoute: builder.query({
			query: (route) => ({
				url: `/files/routes/${route}`,
			}),
		}),
	}),
});

export const { useLazyGetRouteQuery } = routeApiSlice;
