import { apiSlice } from "../../app/api/apiSlice";

export const reactionApiSlice = apiSlice.injectEndpoints({
	endpoints: (builder) => ({
		getReactionsByTripId: builder.query({
			query: ({ tripId, ...filters }) => {
				const params = Object.entries(filters)
					.filter(([key, value]) => value !== "")
					.map(([key, value]) => `${key}=${value}`)
					.join("&");
				return {
					url: `/trips/${tripId}/reactions${params ? `?${params}` : ""}`,
				};
			},
			providesTags: (result, error, arg) =>
				result
					? [
							...result?.participants.map((participant) => ({
								type: "Reaction",
								id: participant.reactions.id,
							})),
							"Reaction",
					  ]
					: ["Reaction"],
		}),
	}),
});

export const { useGetReactionsByTripIdQuery } = reactionApiSlice;
