import React from "react";
import { Button, ListGroup, Modal, Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import CustomPagination from "../../components/CustomPagination";

const FollowersModal = ({ title, items, show, handleClose, page, handleChangePage }) => {
	return (
		<Modal show={show} onHide={handleClose}>
			<Modal.Header>
				<Modal.Title>{title}</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<ListGroup className="mb-1">
					{items.users?.map((user, index) => {
						return (
							<ListGroup.Item key={index}>
								<div className="d-flex">
									<Image
										width={40}
										height={40}
										src={user?.avatarPath ? `${user.avatarPath}?width=40&height=40&quality=100&fit=cover` : "/images/noavatar.jpg"}
										rounded
									/>
									<div className="align-self-center ms-2">
										<Link to={`/users/${user?.id}`}>{user.username}</Link>
									</div>
								</div>
							</ListGroup.Item>
						);
					})}
				</ListGroup>
				{items.users?.length >= 10 && (
					<div className="mt-2 d-flex justify-content-center">
						<CustomPagination size="sm" count={items?.total} perPage={10} active={page} handleChangePage={handleChangePage} />
					</div>
				)}
			</Modal.Body>
			<Modal.Footer>
				<Button onClick={handleClose} variant="secondary">
					Zavřít
				</Button>
			</Modal.Footer>
		</Modal>
	);
};

export default FollowersModal;
