import { apiSlice } from "../../app/api/apiSlice";

export const followersApiSlice = apiSlice.injectEndpoints({
	endpoints: (builder) => ({
		getUserFollowers: builder.query({
			query: ({ userId, ...filters }) => {
				const params = Object.entries(filters)
					.filter(([key, value]) => value !== "")
					.map(([key, value]) => `${key}=${value}`)
					.join("&");
				return {
					url: `/users/${userId}/followers${params ? `?${params}` : ""}`,
				};
			},
			providesTags: (result, error, { userId }) => [{ type: "Follower", id: userId }, ["Follower"]],
		}),
		getUserFollowing: builder.query({
			query: ({ userId, ...filters }) => {
				const params = Object.entries(filters)
					.filter(([key, value]) => value !== "")
					.map(([key, value]) => `${key}=${value}`)
					.join("&");
				return {
					url: `/users/${userId}/following${params ? `?${params}` : ""}`,
				};
			},
			providesTags: (result, error, { userId }) => [{ type: "Follower", id: userId }, ["Follower"]],
		}),
		followUser: builder.mutation({
			query: (userId) => ({
				url: `/users/follow-user/${userId}`,
				method: "POST",
			}),
			invalidatesTags: (result, error, id) => ["Follower", "User"],
		}),
		unfollowUser: builder.mutation({
			query: (userId) => ({
				url: `/users/follow-user/${userId}`,
				method: "DELETE",
			}),
			invalidatesTags: (result, error, id) => ["Follower", "User"],
		}),
	}),
});

export const { useGetUserFollowersQuery, useGetUserFollowingQuery, useFollowUserMutation, useUnfollowUserMutation } =
	followersApiSlice;
