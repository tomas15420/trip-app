import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Fragment, useEffect, useRef, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { AsyncTypeahead, Highlighter, Hint, Menu, MenuItem } from "react-bootstrap-typeahead";
import { useLazyGetTripsQuery } from "../trips/tripApiSlice";
import { useLazyGetUsersQuery } from "../users/userApiSlice";
import { useNavigate } from "react-router-dom";

const Search = ({ onSelect, ...props }) => {
	const [searchText, setSearchText] = useState("");
	const [getTrips, { data: trips, isLoading: tripsIsLoading }] = useLazyGetTripsQuery();
	const [getUsers, { data: users, isLoading: usersIsLoading }] = useLazyGetUsersQuery();
	const navigate = useNavigate();
	const searchRef = useRef(null);

	useEffect(() => {
		if (searchText.length > 0) {
			getTrips({ name: searchText, limit: 10 });
			getUsers({ search: searchText, limit: 10 });
		}
	}, [searchText, getTrips, getUsers]);

	const result = [
		...(trips?.trips?.map((trip) => ({ type: "TRIP", id: trip.id, name: trip.name })) || []),
		...(users?.users?.map((user) => ({
			type: "USER",
			id: user.id,
			name: `${user.firstname || ""} ${user.lastname || ""} (${user.username})`,
		})) || []),
	];

	const handleSelect = (selected) => {
		if (selected) {
			if (selected.type === "TRIP") {
				navigate(`/trips/${selected.id}`);
			}
			if (selected.type === "USER") {
				navigate(`/users/${selected.id}`);
			}
		}
	};

	const handleSearchMore = (e) => {
		if (searchRef?.current.state.text) {
			navigate(`/search?q=${searchRef.current.state.text}`);
			searchRef.current.hideMenu();
			searchRef.current.blur();
		}
	};

	const renderMenu = (results, { newSelectionPrefix, paginationText, renderMenuItemChildren, ...menuProps }, state) => {
		let index = 0;
		const types = results.filter((v, i, a) => a.findIndex((t) => t.type === v.type) === i).map((v) => v.type);
		let items = types.map((type) => (
			<Fragment key={type}>
				{index > 0 && <Menu.Divider />}
				{<Menu.Header>{type === "TRIP" ? "Výlety" : "Uživatelé"}</Menu.Header>}
				{results
					.filter((option) => option.type === type)
					.map((option) => {
						const item = (
							<MenuItem key={index} option={option} position={index}>
								<Highlighter search={state.text}>{option.name}</Highlighter>
							</MenuItem>
						);

						index++;
						return item;
					})}
			</Fragment>
		));

		if (items.length) {
			items = (
				<>
					{items}
					<Menu.Divider />
					<div className="d-flex justify-content-center">
						<Button variant="link" className="text-decoration-none text-muted" onClick={handleSearchMore}>
							Zobrazit další výsledky
						</Button>
					</div>
				</>
			);
		}

		return (
			<Menu {...menuProps} maxHeight="600px">
				{items}
			</Menu>
		);
	};

	return (
		<Form {...props}>
			<AsyncTypeahead
				id="search"
				inputProps={{ id: "search" }}
				ref={searchRef}
				filterBy={() => true}
				options={result}
				isLoading={tripsIsLoading && usersIsLoading}
				labelKey={(option) => `${option.name}`}
				onSearch={(q) => setSearchText(q)}
				placeholder="Hledat..."
				useCache={false}
				onChange={(selected) => {
					if (searchRef.current) {
						searchRef.current.clear();
						searchRef.current.blur();
					}
					onSelect();
					handleSelect(selected[0]);
				}}
				emptyLabel="Žádné výsledky nenalezeny"
				renderMenu={renderMenu}
				renderInput={({ inputRef, referenceElementRef, ...inputProps }) => (
					<Hint>
						<InputGroup className="mx-2">
							<Form.Control
								{...inputProps}
								ref={(node) => {
									inputRef(node);
									referenceElementRef(node);
								}}
							/>
							<Button variant="outline-info" disabled={searchRef?.current?.state.text === ""} onClick={handleSearchMore}>
								<FontAwesomeIcon icon={faSearch} />
							</Button>
						</InputGroup>
					</Hint>
				)}
			/>
		</Form>
	);
};

export default Search;
