import React, { Fragment, useEffect, useState } from "react";
import useTitle from "../../hooks/useTitle";
import { useSearchParams } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { useGetTripsQuery } from "../trips/tripApiSlice";
import TripItemLite from "../trips/TripItemLite";
import { useGetUsersQuery } from "../users/userApiSlice";
import UserPreview from "../users/UserPreview";

const SearchPage = () => {
	const [searchParams] = useSearchParams();
	const [query, setQuery] = useState(searchParams.get("q") || "");
	const [tripPage, seTripPage] = useState(1);
	const [userPage, setUserPage] = useState(1);
	const [allTrips, setAllTrips] = useState([]);
	const [allUsers, setAllUsers] = useState([]);
	const { data: trips, isLoading: tripsIsLoading } = useGetTripsQuery({ name: query, limit: 10, offset: (tripPage - 1) * 10 });
	const { data: users, isLoading: usersIsLoading } = useGetUsersQuery({ search: query, limit: 10, offset: (userPage - 1) * 10 });

	useEffect(() => {
		setQuery(searchParams.get("q") || "");
		setAllTrips([]);
		setAllUsers([]);
	}, [searchParams]);

	useTitle(query ? `Výsledky ${query}` : "Vyhledávání");

	useEffect(() => {
		if (!tripsIsLoading) {
			if (trips?.trips) setAllTrips((current) => [...current, ...trips?.trips]);
		}
	}, [trips, setAllTrips, tripsIsLoading]);

	useEffect(() => {
		if (!usersIsLoading) {
			if (users?.users) setAllUsers((current) => [...current, ...users?.users]);
		}
	}, [users, setAllUsers, usersIsLoading]);
	if (!tripsIsLoading && !usersIsLoading && !trips && !users) {
		return (
			<Container>
				<div className="mt-4 d-flex justify-content-center">
					<h3 className="fw-normal">Žádné výsledky nenalezeny</h3>
				</div>
			</Container>
		);
	}

	return (
		<Container>
			<Row>
				{!tripsIsLoading && trips?.total > 0 && (
					<Col className="g-3 mb-3" sm={12}>
						<Card>
							<Card.Header>
								<Card.Title>Výlety</Card.Title>
							</Card.Header>
							<Card.Body className="p-4">
								{allTrips.map((trip) => (
									<Fragment key={trip.id}>
										<TripItemLite trip={trip} className="border-1 mb-3" />
										<hr />
									</Fragment>
								))}
								{trips.total > allTrips.length && (
									<div className="d-flex justify-content-center">
										<Button onClick={() => seTripPage(tripPage + 1)} className="px-4" variant="primary" size="sm">
											Načíst další výslekdy
										</Button>
									</div>
								)}
							</Card.Body>
						</Card>
					</Col>
				)}
				{!usersIsLoading && users?.total > 0 && (
					<Col className="g-3 mb-3">
						<Card>
							<Card.Header>
								<Card.Title>Uživatelé</Card.Title>
							</Card.Header>
							<Card.Body className="p-4">
								{allUsers.map((user) => (
									<Fragment key={user.id}>
										<UserPreview user={user} />
										<hr />
									</Fragment>
								))}
								{users.total > allUsers.length && (
									<div className="d-flex justify-content-center">
										<Button onClick={() => setUserPage(userPage + 1)} className="px-4" variant="primary" size="sm">
											Načíst další výslekdy
										</Button>
									</div>
								)}
							</Card.Body>
						</Card>
					</Col>
				)}
			</Row>
		</Container>
	);
};

export default SearchPage;
