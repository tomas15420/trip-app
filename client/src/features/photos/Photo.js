import { faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { Button } from "react-bootstrap";
import Figure from "react-bootstrap/Figure";
import { Link } from "react-router-dom";
import { useDeletePhotoMutation } from "./photoApiSlice";
import useAuth from "../../hooks/useAuth";
import Error from "../../components/Error";

const Photo = ({ photo }) => {
    const [deletePhoto] = useDeletePhotoMutation();
    const { userId, roles } = useAuth();
    const [error, setError] = useState("");

    const handleDelete = async (photoId) => {
        try {
            await deletePhoto(photoId).unwrap();
        } catch (e) {
            setError(e);
        }
    };

    const created = new Date(photo.createdAt);
    return (
        <article>
            <Error error={error} onClose={() => setError("")}/>
            <Figure className="mb-3" style={{ position: "relative" }}>
                <Figure.Image
                    fluid
                    src={`${photo.path}?width=800&height=800&quality=90`}
                    alt={photo.description}
                />
                {userId && (userId === photo.userId || roles?.includes("ADMIN"))&& <Button
                    variant="danger"
                    className="rounded-0 border border-2 border-dark"
                    style={{ position: "absolute", top: 0, right: 0 }}
                    onClick={() => handleDelete(photo.id)}
                >
                    <FontAwesomeIcon icon={faTrashAlt} />
                </Button>}
                <Figure.Caption className="d-flex flex-wrap">
                    <div>
                        {photo.description && <span className="text-break">{photo.description}</span>}
                    </div>
                    <div className="ms-auto">
                        {photo.user && (
                            <span className="me-1">
                                Přidal:{" "}
                                <Link to={`/users/${photo.userId}`}>
                                    {photo.user.username}
                                </Link>
                            </span>
                        )}
                        <span className="me-1">
                            {created.toLocaleString().slice(0, -3)}
                        </span>
                    </div>
                </Figure.Caption>
            </Figure>
        </article>
    );
};

export default Photo;
