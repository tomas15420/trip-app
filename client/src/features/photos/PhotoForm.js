import React, { useState } from "react";
import FormInput from "../../components/Form/FormInput";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Formik } from "formik";
import { mixed, object, string } from "yup";
import { useParams } from "react-router-dom";
import { usePostPhotoMutation } from "./photoApiSlice";
import Error from "../../components/Error";

const PhotoForm = () => {
	const { tripId } = useParams();
	const [postPhoto] = usePostPhotoMutation();
	const [error, setError] = useState("");

	const schema = object({
		description: string().optional().max(128, "Maximální délka popisu je 128 znaků").min(8, "Minimální délka popisu je 8 znaků"),
		photo: mixed()
			.required("Fotografie je vyžadována")
			.test("fileSize", "Soubor je příliš velký", (value) => !value || (value && value?.size <= 1024 * 1024 * 2))
			.test(
				"fileFormat",
				"Soubor není podporován",
				(value) => !value || (value && ["image/jpeg", "image/png"].includes(value?.type))
			),
	});

	const handleSubmit = async (values, { resetForm }) => {
		try {
			const formData = new FormData();
			formData.append("photo", values.photo);
			formData.append("body", JSON.stringify({ description: values.description }));
			await postPhoto({ tripId, data: formData }).unwrap();
			setError("");
			resetForm({ values: "", touched: false });
			document.querySelector("#photo").value = null;
		} catch (e) {
			setError(e);
		}
	};

	return (
		<>
			<Error error={error} onClose={() => setError("")} />
			<Formik validationSchema={schema} initialValues={{ description: "", photo: "" }} onSubmit={handleSubmit}>
				{({ handleSubmit, isValid, isSubmitting, setFieldValue, setFieldTouched, touched, errors }) => (
					<Form noValidate onSubmit={handleSubmit} encType="multipart/form-data">
						<Form.Group controlId="photo" className="mb-2">
							<Form.Control
								type="file"
								accept="image/png,image/jpeg"
								onChange={(e) => {
									setFieldTouched("photo", true);
									setFieldValue("photo", e.currentTarget.files[0]);
								}}
								isInvalid={touched.photo && !!errors.photo}
							/>
							<Form.Text>Povolené formáty: .jpg, .png (max 2MB)</Form.Text>
							<Form.Control.Feedback type="invalid">{errors.photo}</Form.Control.Feedback>
						</Form.Group>
						<FormInput
							as="textarea"
							controlId="description"
							placeholder="Zde zadejte popis obrázku (nepovinný)"
							className="mb-2"
							maxLength="128"
							validateValid={false}
							optional
							isInvalid={!!errors.description}
							style={{ resize: "none" }}
						/>
						<div className="d-flex justify-content-end">
							<Button disabled={isSubmitting || !isValid} type="submit">
								Přidat fotografii
							</Button>
						</div>
						<hr />
					</Form>
				)}
			</Formik>
		</>
	);
};

export default PhotoForm;
