import { apiSlice } from "../../app/api/apiSlice";

export const photoApiSlice = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        getPhotos: builder.query({
            query: ({ tripId, ...filters }) => {
                const params = Object.entries(filters)
                    .filter(([key, value]) => value !== "")
                    .map(([key, value]) => `${key}=${value}`)
                    .join("&");
                return {
                    url: `/trips/${tripId}/photos${
                        params ? `?${params}` : ""
                    }`,
                };
            },
            providesTags: (result) => (
                result 
                    ? [...result.photos?.map(({ id }) => ({type: 'Photo', id})), 'Photo']
                    : ['Photo'])
        }),
        postPhoto: builder.mutation({
            query: ( {tripId, data}) => ({
                url: `/trips/${tripId}/photos`,
                method: 'POST',
                body: data
            }),
            invalidatesTags: ['Photo'],
        }),
        deletePhoto: builder.mutation({
            query: (photoId) => ({
                url: `/photos/${photoId}`,
                method: "DELETE"
            }),
            invalidatesTags: (result, error, { id }) => [{ type: "Photo", id }],
        })
    }),
});

export const { useLazyGetPhotosQuery, usePostPhotoMutation, useDeletePhotoMutation } = photoApiSlice;
