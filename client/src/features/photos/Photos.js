import React, { useEffect, useMemo } from "react";
import Card from "react-bootstrap/Card";
import { useParams } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import useAuth from "../../hooks/useAuth";
import { useGetTripQuery } from "../trips/tripApiSlice";
import Photo from "./Photo";
import { useLazyGetPhotosQuery } from "./photoApiSlice";
import PhotoForm from "./PhotoForm";

const Photos = () => {
    const { tripId } = useParams();
    const { userId } = useAuth();

    const {
        data: trip,
        isLoading: tripIsLoading,
        isSuccess: tripIsSuccess,
    } = useGetTripQuery(tripId);

    const [
        getPhotos,
        {
            data: photos,
            isLoading: photosIsLoading,
            isSuccess: photoIsSuccess,
            isError: photoIsError,
        },
    ] = useLazyGetPhotosQuery({pollingInterval: 15000, refetchOnFocus: true});

    const tripDate = useMemo(() => new Date(trip.date),[trip]);

    useEffect(() => {
        if (tripIsSuccess) {
            if (tripDate <= new Date()) {
                getPhotos({ tripId: trip.id, order: "createdAt.desc" });
            }
        }
    }, [tripIsSuccess, getPhotos, tripDate, trip?.id]);

    let content;
    if (tripIsLoading || photosIsLoading) content = <LoadingSpinner />;
    else if (tripIsSuccess && tripDate > new Date())
        content = (
            <>
                <Card.Title>Fotogalerie</Card.Title>
                <p className="d-flex justify-content-center">
                    Fotografie lze přidávat až po skončení výletu
                </p>
            </>
        );
    else if (tripIsSuccess && photoIsSuccess) {
        content = (
            <>
                <Card.Title>Fotogalerie</Card.Title>
                {photos?.photos && (
                    <Card.Subtitle className="text-muted mb-3">
                        Počet fotografií: {photos?.total || "0"}
                    </Card.Subtitle>
                )}
                {userId ? (
                    <PhotoForm />
                ) : (
                    <p className="d-flex justify-content-center">
                        Pro přidání fotografie se přihlašte
                    </p>
                )}
                {photos?.total === 0 ? (
                    <p className="d-flex justify-content-center">
                        Buďte první kdo přidá fotografii
                    </p>
                ) : (
                    photos?.photos.map((photo) => (
                        <Photo key={photo.id} photo={photo} />
                    ))
                )}
            </>
        );
    } else if (photoIsError) content = <p>Chyba při načítání obrázků</p>;

    return content;
};

export default Photos;
