import React from "react";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import { useGetTripsQuery } from "../trips/tripApiSlice";
import Stars from "../../components/Stars";
import { Link } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import ErrorPage from "../../components/ErrorPage";

const BestTrips = () => {
	const { data, isLoading, isSuccess, isError, error } = useGetTripsQuery({ order: "avgStars.desc", limit: 10 });
	if (isLoading) return <LoadingSpinner />;
	if (isError) return <ErrorPage error={error} as={Card} />;
	if (isSuccess) {
		return data?.trips ? (
			<Table responsive bordered striped>
				<thead>
					<tr>
						<th>Výlet</th>
						<th>Autor</th>
						<th>Typ</th>
						<th>Datum konání</th>
						<th>Místo konání</th>
						<th>Počet účastníků</th>
						<th>Hodnocení</th>
					</tr>
				</thead>
				<tbody>
					{data?.trips?.map((trip) => {
						const date = new Date(trip.date);
						return (
							<tr key={trip.id}>
								<td>
									<Link to={`/trips/${trip.id}`}>{trip.name}</Link>
								</td>
								<td>
									<Link to={`/users/${trip.userId}`}>{trip.user.username}</Link>
								</td>
								<td>{trip.trip_type.type}</td>
								<td>{date.toLocaleString().slice(0, -3)}</td>
								<td>
									<Link
										to={`/trips?regionId=${trip.startPlace.district.regionId}&districtId=${trip.startPlace.district.id}&municipalityId=${trip.startPlaceId}`}
									>
										{trip.startPlace.name}
									</Link>
								</td>
								<td>{trip.participates}</td>
								<td>
									<Stars size="sm" stars={trip.avgStars} />
								</td>
							</tr>
						);
					})}
				</tbody>
			</Table>
		) : (
			<p>Žádné výlety nejsou k dispozici</p>
		);
	}
};

export default BestTrips;
