import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import { NavLink, Outlet } from "react-router-dom";
import useTitle from "../../hooks/useTitle";

const Laderboard = () => {
	useTitle("Žebříčky")
	return (
		<Container>
			<h1 className="my-4">Žebříčky</h1>
			<Nav variant="pills" className="mb-4">
				<Nav.Item>
					<Nav.Link href="/leaderboard" as={NavLink} to="/leaderboard" end>
						Nejlépe hodnocené výlety
					</Nav.Link>
				</Nav.Item>
				<Nav.Item>
					<Nav.Link href="best-participants" as={NavLink} to="best-participants">
						Nejvíce účastníků
					</Nav.Link>
				</Nav.Item>
				<Nav.Item>
					<Nav.Link href="best-created" as={NavLink} to="best-created">
						Nejvíce vytvořených výletů
					</Nav.Link>
				</Nav.Item>
			</Nav>
			<Outlet />
		</Container>
	);
};

export default Laderboard;
