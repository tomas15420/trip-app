import React from "react";
import Card from 'react-bootstrap/Card'
import Table from "react-bootstrap/Table";
import { Link } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import { useGetUsersQuery } from "../users/userApiSlice";
import ErrorPage from "../../components/ErrorPage";

const BestCreated = () => {
	const { data, isLoading, isSuccess, isError, error } = useGetUsersQuery({ order: "createdTrips.desc", limit: 10 });
	if (isLoading) return <LoadingSpinner />;
	if (isError) return <ErrorPage error={error} as={Card}/>;
	if (isSuccess) {
		return data?.users ? (
			<Table responsive bordered striped>
				<thead>
					<tr>
						<th>Uživatel</th>
						<th>Počet vytvořených výletů</th>
					</tr>
				</thead>
				<tbody>
					{data?.users.map((user) => {
						return (
							<tr key={user.id}>
								<td>
									<Link to={`/users/${user.id}`}>{user.username}</Link>
								</td>
								<td>{user.createdTrips}</td>
							</tr>
						);
					})}
				</tbody>
			</Table>
		) : (
			<p>Žádné výlety nejsou k dispozici</p>
		);
	}
};

export default BestCreated;
