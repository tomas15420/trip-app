import { apiSlice } from "../../app/api/apiSlice";

export const tripApiSlice = apiSlice.injectEndpoints({
	endpoints: (builder) => ({
		getTrips: builder.query({
			query: (filters) => {
				const params = Object.entries(filters)
					.filter(([key, value]) => value !== "")
					.map(([key, value]) => `${key}=${value}`)
					.join("&");
				return `/trips/?${params}`;
			},
			providesTags: (result, error, arg) => (result ? [...result.trips.map(({ id }) => ({ type: "Trip", id })), "Trip"] : ["Trip"]),
		}),
		getTrip: builder.query({
			query: (tripId) => ({
				url: `/trips/${tripId}`,
				validateStatus: (response, { result: isError, count }) => {
					return response.status === 200 && !isError && count > 0;
				},
			}),
			providesTags: (result, error, arg) => (result ? [{ type: "Trip", id: result.id }, "Trip"] : ["Trip"]),
			transformResponse: (responseData) => {
				return responseData?.trips[0];
			},
			transformErrorResponse: (response) => {
				if (response.status === 200 && response.data?.count === 0) return { status: 404, data: response.data };
				return response;
			},
		}),
		createTrip: builder.mutation({
			query: (trip) => {
				return {
					url: "/trips",
					method: "POST",
					body: { ...trip },
				};
			},
			invalidatesTags: ["Trip"],
		}),
		updateTrip: builder.mutation({
			query: ({ tripId, ...trip }) => ({
				url: `/trips/${tripId}`,
				method: "PATCH",
				body: trip,
			}),
			invalidatesTags: (result, error, { tripId }) => [{ type: "Trip", id: tripId }],
		}),
		reactToTrip: builder.mutation({
			query: ({ tripId, reaction }) => ({
				url: `/trips/${tripId}/reactions`,
				method: "POST",
				body: { reaction },
			}),
			invalidatesTags: (result, error, { tripId }) => [{ type: "Trip", id: tripId }, { type: "Reaction" }],
		}),
		deleteTripReaction: builder.mutation({
			query: ({ tripId }) => ({
				url: `/trips/${tripId}/reactions`,
				method: "DELETE",
			}),
			invalidatesTags: (result, error, { tripId }) => [{ type: "Trip", id: tripId }, { type: "Reaction" }],
		}),
		deleteTrip: builder.mutation({
			query: (tripId) => ({
				url: `/trips/${tripId}`,
				method: "DELETE",
			}),
			invalidatesTags: (result, error, { tripId }) => [{ type: "Trip", id: tripId }, { type: "Reaction" }],
		}),
		getTripsTypes: builder.query({
			query: () => "/trips/types",
		}),
	}),
});

export const {
	useGetTripsQuery,
	useLazyGetTripsQuery,
	useGetTripQuery,
	useGetTripsTypesQuery,
	useCreateTripMutation,
	useUpdateTripMutation,
	useReactToTripMutation,
	useDeleteTripMutation,
	useDeleteTripReactionMutation,
} = tripApiSlice;
