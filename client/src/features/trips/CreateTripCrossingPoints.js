import { Formik } from "formik";
import React from "react";
import { useRef } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import FormInput from "../../components/Form/FormInput";
import MunicipalitySearch from "../location/MunicipalitySearch";
import { number, object, string } from "yup";
import CrossingPointsList from "./CrossingPointsList";

const CreateTripCrossingPoints = ({ crossingPoints, setCrossingPoints }) => {
	const searchInput = useRef(null);

	const handleAddCrossingPoint = (values, { resetForm }) => {
		const crossingPoint = values.crossingPoint;
		setCrossingPoints([
			...crossingPoints,
			{ id: crossingPoint.id, name: crossingPoint.name, description: values.crossingPointDesc },
		]);
		resetForm({ values: "" });
		if (searchInput.current) searchInput.current.clear();
	};

	const schema = object({
		crossingPoint: object({
			id: number().min(1, "Chybně zadán Průchozí bod").required("Průchozí bod je vyžadován"),
			name: string().required("Název Průchozího bodu je vyžadován"),
		}).required("Průchozí bod je vyžadován"),
		crossingPointDesc: string().optional().min(5, "Minimální délka popisu je 5 znaků").max(30, "Popis může mít maximálně 30 znaků"),
	});

	return (
		<>
			{crossingPoints.length < 15 && (
				<Formik
					initialValues={{ crossingPoint: "", crossingPointDesc: "" }}
					onSubmit={handleAddCrossingPoint}
					validationSchema={schema}
					validateOnMount
				>
					{({ errors, touched, handleSubmit, handleBlur, setFieldValue, isValid, isSubmitting }) => (
						<Form noValidate onSubmit={handleSubmit}>
							<Form.Group controlId="crossingPoint">
								<Form.Label>Průchozí bod</Form.Label>
								<MunicipalitySearch
									inputProps={{ id: "crossingPoint" }}
									id="crossingPoint"
									ref={searchInput}
									onBlur={handleBlur}
									onChange={(val) => setFieldValue("crossingPoint", val[0])}
									isInvalid={touched.crossingPoint && !!errors.crossingPoint}
									isValid={touched.crossingPoint && !errors.crossingPoint}
									placeholder="Zadejte průchozí bod"
									emptyLabel="Nenalezeny žádné výsledky"
									promptText="Piš pro hledání"
									searchText="Hledám..."
								/>
								<Form.Control.Feedback type="invalid" className={!!touched.crossingPoint && !!errors.crossingPoint && "d-block"}>
									{errors.crossingPoint?.id}
								</Form.Control.Feedback>
							</Form.Group>
							<FormInput type="text" controlId="crossingPointDesc" label="Popis" optional placeholder="Zadejte popis" />
							<div className="d-flex justify-content-center">
								<Button type="submit" className="mt-2 px-4" disabled={!isValid || isSubmitting}>
									Přidat bod
								</Button>
							</div>
						</Form>
					)}
				</Formik>
			)}

			{crossingPoints.length > 0 && <CrossingPointsList crossingPoints={crossingPoints} setCrossingPoints={setCrossingPoints} />}
		</>
	);
};

export default CreateTripCrossingPoints;
