import React from "react";
import Card from "react-bootstrap/Card";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import TripParticipants from "./TripParticipants";

const ParticipantsSideBar = ({ trip }) => {
	const tripDate = new Date(trip?.date);
	const currentDate = new Date();
	return (
		<Card className="my-3">
			<Card.Body>
				<Card.Title as={"h5"}>Účastníci</Card.Title>
				{!trip?.maybe_participates && !trip?.participates ? (
					<p>{currentDate < tripDate ? "Výletu se zatím nikdo neúčastní" : "Výletu se nikdo nezúčastnil"}</p>
				) : (
					<>
						<Tabs fill>
							<Tab eventKey="all" title="Vše">
								<TripParticipants tripId={trip.id} reactionType="ALL" />
							</Tab>
							<Tab eventKey="participate" title="Zúčastní se">
								<TripParticipants tripId={trip.id} reactionType="PARTICIPATE" />
							</Tab>
							<Tab eventKey="maybe_participate" title="Možná se zúčastní">
								<TripParticipants tripId={trip.id} reactionType="MAYBE_PARTICIPATE" />
							</Tab>
						</Tabs>
					</>
				)}
			</Card.Body>
		</Card>
	);
};

export default ParticipantsSideBar;
