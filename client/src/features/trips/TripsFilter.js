import React, { useEffect } from "react";
import {
	useGetRegionsQuery,
	useLazyGetDistrictsByRegionQuery,
	useLazyGetMunicipalitiesByDistrictQuery,
} from "../location/locationApiSlice";
import Form from "react-bootstrap/Form";
import { useGetTripsTypesQuery } from "./tripApiSlice";
import { Col, Row } from "react-bootstrap";
import LoadingSpinner from "../../components/LoadingSpinner";

const TripsFilter = ({ setSearchParamsEx, params }) => {

	const { data: tripTypes, isLoading: tripTypesIsLoading, isError: tripTypesIsError } = useGetTripsTypesQuery();
	const { data: regions, isLoading: regionsIsLoading, isError: regionsIsError } = useGetRegionsQuery();
	const [getDistricts, { data: districts, isLoading: districtsIsLoading, isError: districtsIsError }] =
		useLazyGetDistrictsByRegionQuery(params.region);
	const [getMunicipalities, { data: municipalities, isLoading: municipalitiesIsLoading, isError: municipalitiesIsError }] =
		useLazyGetMunicipalitiesByDistrictQuery(params.district);

	const handleRegionChange = (e) => {
		setSearchParamsEx({
			regionId: e.target.value,
			districtId: "",
			municipalityId: "",
		});
	};

	const handeDistrictChange = (e) => {
		setSearchParamsEx({ districtId: e.target.value, municipalityId: "" });
	};

	const handeMunicipalityChange = (e) => {
		setSearchParamsEx({ municipalityId: e.target.value });
	};

	const handleTypeChange = (e) => {
		setSearchParamsEx({ type: e.target.value });
	};

	const handleUpcomingChange = (e) => {
		setSearchParamsEx({ upcoming: e.target.checked });
	};

	const handleIncludeCPChange = (e) => {
		setSearchParamsEx({ includeCP: e.target.checked });
	};

	useEffect(() => {
		if (params.region !== "") getDistricts(params.region, true);
	}, [params.region, getDistricts]);

	useEffect(() => {
		if (params.district !== "") getMunicipalities(params.district, true);
	}, [params.district, getMunicipalities]);

	let content;
	if (regionsIsLoading || tripTypesIsLoading) {
		content = <LoadingSpinner className="mt-3" />;
	} else if (regionsIsError || districtsIsError || municipalitiesIsError || tripTypesIsError)
		return <p className="d-flex justify-content-center">Vyskytla se chyba při načítání filtrů, zkuste načíst stránku znovu</p>;
	else {
		content = (
			<Form>
				<Row className="gy-1 gy-sm-2">
					<Col lg="4" xs="6">
						<Form.Group controlId="region" className="d-flex">
							<Form.Label column className="me-2">
								Kraj:
							</Form.Label>
							<Form.Select size="sm" onChange={handleRegionChange} value={params.region}>
								<option value="">Vyberte kraj</option>
								{regions?.regions.map((region) => (
									<option key={region.id} value={region.id}>
										{region.name}
									</option>
								))}
							</Form.Select>
						</Form.Group>
					</Col>
					<Col lg="4" xs="6">
						<Form.Group controlId="district" className="d-flex">
							<Form.Label column className="me-2">
								Okres:
							</Form.Label>
							<Form.Select size="sm" onChange={handeDistrictChange} value={params.district}>
								<option value="">Vyberte okres</option>
								{params.region &&
									!districtsIsLoading &&
									districts?.districts.map((district) => (
										<option key={district.id} value={district.id}>
											{district.name}
										</option>
									))}
							</Form.Select>
						</Form.Group>
					</Col>
					<Col lg="4" xs="6">
						<Form.Group controlId="municipality" className="d-flex">
							<Form.Label column className="me-2">
								Obec:
							</Form.Label>
							<Form.Select size="sm" value={params.municipality} onChange={handeMunicipalityChange}>
								<option value="">Vyberte obec</option>
								{params.district &&
									!municipalitiesIsLoading &&
									municipalities?.municipalities.map((municipality) => (
										<option key={municipality.id} value={municipality.id}>
											{municipality.name}
										</option>
									))}
							</Form.Select>
						</Form.Group>
					</Col>
					<Col lg="4" xs="6">
						<Form.Group controlId="type" className="d-flex">
							<Form.Label column className="me-2">
								Typ:
							</Form.Label>
							<Form.Select size="sm" value={params.type || "0"} onChange={handleTypeChange}>
								<option value="0">Vše</option>
								{!tripTypesIsLoading &&
									tripTypes?.map((type) => (
										<option key={type.id} value={type.id}>
											{type.type}
										</option>
									))}
							</Form.Select>
						</Form.Group>
					</Col>
					<Col lg="8" xs="12" className="d-flex align-items-center">
						<Form.Group controlId="upcoming">
							<Form.Check onChange={handleUpcomingChange} type="checkbox" checked={params.upcoming} label="Pouze nadcházející" />
						</Form.Group>
						<Form.Group controlId="includeCP" className="ms-2">
							<Form.Check
								onChange={handleIncludeCPChange}
								type="checkbox"
								checked={params.includeCP}
								label="Zahnrout obec do průjezdných bodů"
							/>
						</Form.Group>
					</Col>
				</Row>
			</Form>
		);
	}
	return content;
};

export default TripsFilter;
