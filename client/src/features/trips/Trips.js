import React, { useCallback, useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import TripSideBar from "./TripSideBar";
import useTitle from "../../hooks/useTitle";
import TripItem from "./TripItem";
import CustomPagination from "../../components/CustomPagination";
import { useLazyGetTripsQuery } from "./tripApiSlice";
import TripsFilter from "./TripsFilter";
import LoadingSpinner from "../../components/LoadingSpinner";
import ErrorPage from "../../components/ErrorPage";
import { useSearchParams } from "react-router-dom";

const Trips = () => {
	useTitle("Výlety");

	const PER_PAGE = 5;

	const [searchParams, setSearchParams] = useSearchParams();

	const parseSearchParams = useCallback(() => {
		const regionId = searchParams.get("regionId") || "";
		const districtId = searchParams.get("districtId") || "";
		const municipalityId = searchParams.get("municipalityId") || "";
		const type = searchParams.get("type") || "0";
		const upcoming = searchParams.get("upcoming") || true;
		const includeCP = searchParams.get("includeCP") || false;
		const page = parseInt(searchParams.get("page")) || 1;

		return {
			region: regionId,
			district: districtId,
			municipality: municipalityId,
			type,
			upcoming: upcoming === "true" || upcoming === true,
			includeCP: includeCP === "true" || includeCP === true,
			page,
		};
	}, [searchParams]);
	const [params, setParams] = useState(parseSearchParams());

	const [getTrips, { data: trips, isLoading, isSuccess, isError, error }] = useLazyGetTripsQuery({
		pollingInterval: 15000,
		refetchOnFocus: true,
		refetchOnMountOrArgChange: true,
	});

	const setSearchParamsEx = (param) => {
		const query = Object.entries({ ...Object.fromEntries(searchParams.entries()), ...param }).reduce(
			(o, [k, v]) => (v === "" ? o : ((o[k] = v), o)),
			{}
		);
		setSearchParams(query);
	};

	useEffect(() => {
		setParams(parseSearchParams());
	}, [searchParams, setParams, parseSearchParams]);

	useEffect(() => {
		const fetchData = async () => {
			const requestParams = {
				startPlaceRegion: params.region,
				startPlaceDistrict: params.district,
				startPlaceId: params.municipality,
				upcoming: params.upcoming,
				includeCP: params.includeCP,
				limit: PER_PAGE,
				offset: (params.page - 1) * PER_PAGE,
				order: "id.desc"
			};
			if (params.type !== "0") requestParams.tripTypeId = params.type;
			await getTrips(requestParams);
		};
		if (params) {
			fetchData();
		}
	}, [params, getTrips]);

	const handleChangePage = (page) => {
		window.scrollTo({ top: 0, behavior: "smooth" });
		setSearchParamsEx({ page: page });
	};

	useEffect(() => {
		handleChangePage(1);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [params?.region, params?.district, params?.municipality, params?.includeCP, params?.upcoming, params?.type]);

	if (isLoading) return <LoadingSpinner className="mt-5" />;
	else if (isError) return <ErrorPage error={error} />;
	else if (isSuccess)
		return (
			<Container as={"main"}>
				<Row className="g-4 my-2">
					<Col md={8}>
						<TripsFilter setSearchParamsEx={setSearchParamsEx} params={params} />
						{trips?.trips && trips.count > 0 ? (
							trips?.trips?.map((trip) => <TripItem key={trip.id} trip={trip} />)
						) : (
							<h5 className="mt-4 text-center">Bohužel, žádné výlety nebyly nenalezeny</h5>
						)}
						<div className="d-flex justify-content-center">
							<CustomPagination
								size="md"
								active={params.page}
								count={trips?.total}
								perPage={PER_PAGE}
								handleChangePage={handleChangePage}
							/>
						</div>
					</Col>
					<Col md={4}>
						<TripSideBar />
					</Col>
				</Row>
			</Container>
		);
};

export default Trips;
