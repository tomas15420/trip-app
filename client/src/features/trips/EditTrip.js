import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import Error from "../../components/Error";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { date, object, string, number, mixed } from "yup";
import LoadingSpinner from "../../components/LoadingSpinner";
import useAuth from "../../hooks/useAuth";
import useTitle from "../../hooks/useTitle";
import { showLogin } from "../auth/authSlice";
import CreateTripCrossingPoints from "./CreateTripCrossingPoints";
import CreateTripFields from "./CreateTripFields";
import { useGetTripQuery, useUpdateTripMutation } from "./tripApiSlice";
import ErrorPage from "../../components/ErrorPage";
import { useLazyGetRouteQuery } from "../routes/routesApiSlice";
import Map from "../../components/Map";

const EditTrip = () => {
	const { tripId } = useParams();
	const { userId, roles } = useAuth();
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const location = useLocation();
	const [error, setError] = useState("");
	const [mapData, setMapData] = useState([]);
	const [crossingPoints, setCrossingPoints] = useState([]);

	const [updateTrip] = useUpdateTripMutation();
	const {
		data: trip,
		isLoading: tripIsLoading,
		isSuccess: tripIsSuccess,
		isError: tripIsError,
		error: tripError,
	} = useGetTripQuery(tripId);
	const [getRoute, { data: route, isSuccess: routeIsSuccess }] = useLazyGetRouteQuery();

	useTitle(`Úprava výletu ${trip?.name}`);

	useEffect(() => {
		if (!userId || (!tripIsLoading && userId !== trip?.userId && !roles?.includes("ADMIN"))) {
			navigate(location?.state?.from || "/", { replace: true });
			if (!userId) dispatch(showLogin({ show: true }));
		}
	}, [userId, navigate, dispatch, location, tripIsLoading, trip?.userId, roles]);

	useEffect(() => {
		if (tripError?.status === 404) {
			navigate("/404", { replace: true });
		}
	}, [tripError, navigate]);

	useEffect(() => {
		if (tripIsSuccess) {
			setCrossingPoints(trip.crossingPoints?.map((cp) => ({ id: cp.id, name: cp.name, description: cp.info?.description })));
			if (trip.route !== null) {
				getRoute(trip.route);
			}
		}
	}, [trip, getRoute, tripIsSuccess]);

	useEffect(() => {
		if (routeIsSuccess) setMapData(route);
	}, [route, routeIsSuccess]);

	const minDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 3);
	const schema = object({
		name: string().required("Název je vyžadován").min(15, "Název je příliš krátký").max(64, "Název je příliš dlouhý"),
		description: string().required("Popis je vyžadován").min(15, "Popis je moc krátký").max(500, "Popis je moc dlouhý"),
		date: date().required("Datum konání je vyžadováno").min(minDate.toDateString(), "Zvolte datum alespoń 3 dny dopředu"),
		distance: number()
			.typeError("Vzádlenost musí být číslo")
			.required("Vzádlenost je vyžadována")
			.min(1, "Výlet musí být dlouhý alespoň 1 km")
			.max(1000, "Výlet nesmí být delší 1000 km"),
		elevation: number()
			.typeError("Převýšení musí být číslo")
			.optional()
			.min(1, "Minimální převýšení musí být 1m")
			.max(10000, "Maximální převýšení může být 10 000m"),
		startPlace: object({
			id: number().required("Startovní pozice je vyžadována").min(1, "Chybně zadána startovní pozice"),
			name: string().optional(),
		}),
		finishPlace: object({
			id: number().required("Cílová pozice je vyžadována").min(1, "Chybně zadána cílová pozice"),
			name: string().optional(),
		}),
		tripType: number().min(1, "Vyberte typ výletu").required("Typ je vyžadován"),
		url: string()
			.matches(
				/[(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/gi,
				"Zadejte platnou URL adresu"
			)
			.optional(),
		route: mixed()
			.optional()
			.test("typeValid", "Můžete použít pouze .gpx soubory", (value) => !value || value?.name.endsWith(".gpx")),
	});

	const handleSubmit = async (values, { resetForm }) => {
		try {
			const body = {
				...values,
				tripId,
				startPlace: values.startPlace.id,
				finishPlace: values.finishPlace.id,
				elevation: Number.isInteger(values.elevation) ? values.elevation : undefined,
				url: values.url.length > 0 ? values.url : undefined,
				crossingPoints: crossingPoints,
			};
			if (mapData.length > 0 && mapData !== route) {
				body.route = mapData;
			} else if (mapData.length === 0 && route) {
				body.route = "delete";
			} else body.route = undefined;
			const res = await updateTrip(body).unwrap();
			resetForm({ values: "" });
			navigate(`/trips/${res.trip?.id}`);
		} catch (e) {
			setError(e);
		}
	};

	if (tripIsLoading) return <LoadingSpinner className="mt-5" />;
	else if (tripIsError) return <ErrorPage error={tripError} />;
	else if (tripIsSuccess) {
		const tripDate = new Date(trip?.date);
		return (
			<Container as="main" className="mt-3">
				<Row className="gy-3 mb-3">
					<Col md="8">
						<Error error={error} onClose={() => setError("")} />
						<Card>
							<Card.Body className="p-4">
								<Card.Title className="mb-3">Úprava výletu {trip?.name}</Card.Title>
								<Formik
									validationSchema={schema}
									initialValues={{
										name: trip?.name || "",
										description: trip?.description || "",
										date: tripDate < minDate ? "" : tripDate,
										tripType: trip?.tripTypeId || "0",
										distance: trip?.distance || "",
										elevation: trip?.elevation || "",
										url: trip?.url || "",
										startPlace: {
											id: trip?.startPlaceId,
											name: trip?.startPlace.name,
										},
										finishPlace: {
											id: trip?.finishPlaceId,
											name: trip?.finishPlace.name,
										},
										route: "",
									}}
									initialTouched={{
										name: true,
										description: true,
										date: true,
										elevation: true,
										distance: true,
										tripType: true,
										startPlace: true,
										finishPlace: true,
										url: true,
									}}
									onSubmit={handleSubmit}
									validateOnMount={true}
								>
									{(props) => (
										<Form noValidate onSubmit={props.handleSubmit}>
											<CreateTripFields
												formikProps={{
													...props,
												}}
												mapData={mapData}
												setMapData={setMapData}
											/>

											{mapData.length > 0 && (
												<div className="d-flex justify-content-end">
													<Button variant="danger" className="mb-1" onClick={() => setMapData([])}>
														Odstranit trasu
													</Button>
												</div>
											)}
											<Map coords={mapData} zoom={9} center={[50.06978230955313, 14.38732867209974]} scrollWheelZoom={true} />

											<div className="d-flex justify-content-center mt-3">
												<Button disabled={!props.isValid || props.isSubmitting} type="submit">
													Upravit výlet
												</Button>
											</div>
										</Form>
									)}
								</Formik>
							</Card.Body>
						</Card>
					</Col>
					<Col md={4}>
						<Card>
							<Card.Body>
								<Card.Title>Průchozí body</Card.Title>
								<CreateTripCrossingPoints crossingPoints={crossingPoints} setCrossingPoints={setCrossingPoints} />
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		);
	}
};

export default EditTrip;
