import { faEdit, faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import Error from "../../components/Error";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import { Link, NavLink, Navigate, Outlet, useNavigate, useParams } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import useAuth from "../../hooks/useAuth";
import useTitle from "../../hooks/useTitle";
import { useDeleteTripMutation, useGetTripQuery } from "./tripApiSlice";
import TripDetails from "./TripDetails";
import TripParticipation from "./TripParticipation";
import Rating from "../rating/Rating";
import ErrorPage from "../../components/ErrorPage";
import ParticipantsSideBar from "./ParticipantsSideBar";

const Trip = () => {
	const { tripId } = useParams();
	const { userId, roles } = useAuth();
	const { data: trip, isLoading, isError, error, isSuccess } = useGetTripQuery(tripId);
	const [deleteTrip] = useDeleteTripMutation(tripId);
	const navigate = useNavigate();
	const [deleteError, setDeleteError] = useState();

	useTitle(`Výlet - ${trip?.name}`);

	const handleDelete = async () => {
		try {
			await deleteTrip(tripId).unwrap();
			navigate("/trips");
		} catch (e) {
			setDeleteError(e);
		}
	};

	if (isLoading) {
		return <LoadingSpinner className="mt-5" />;
	} else if (isError) {
		if (error.status === 404) return <Navigate to="/404" replace/>
		return <ErrorPage error={error} />;
	} else if (isSuccess) {
		return (
			<>
				<Container as={"main"}>
					<Row>
						<Col lg={8}>
							<Error className="my-3" error={deleteError} onClose={() => setDeleteError("")} />
							<Card className="my-3">
								<Card.Body>
									{userId && (trip?.userId === userId || roles?.includes("ADMIN")) && (
										<div className="float-md-end d-flex justify-content-end">
											<Link to={`edit`}>
												<Button role="link">
													<FontAwesomeIcon icon={faEdit} />
												</Button>
											</Link>

											<Button className="ms-1" variant="danger" onClick={handleDelete}>
												<FontAwesomeIcon icon={faTrashAlt} />
											</Button>
										</div>
									)}

									<TripDetails trip={trip} />

									<div className="d-flex justify-content-md-end justify-content-sm-center mt-3 mt-md-0">
										<TripParticipation trip={trip} />
									</div>
								</Card.Body>
							</Card>
							<Card className="mb-4">
								<Card.Header>
									<Nav variant="pills">
										<Nav.Item>
											<Nav.Link as={NavLink} href={`/trips/${tripId}`} to={`/trips/${tripId}`} end>
												Komentáře
											</Nav.Link>
										</Nav.Item>
										<Nav.Item>
											<Nav.Link as={NavLink} href={`/trips/${tripId}/photos`} to={`/trips/${tripId}/photos`}>
												Fotogalerie
											</Nav.Link>
										</Nav.Item>
									</Nav>
								</Card.Header>
								<Card.Body>
									<Outlet />
								</Card.Body>
							</Card>
						</Col>
						<Col lg={4}>
							<Row>
								{isSuccess && new Date(trip.date) < new Date() && (
									<Col className="mt-3" md={12}>
										<Rating trip={trip} />
									</Col>
								)}
								<Col md={12}>
									<ParticipantsSideBar trip={trip} />
								</Col>
							</Row>
						</Col>
					</Row>
				</Container>
			</>
		);
	}
};

export default Trip;
