import { faDownLong, faUpLong, faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";

const CrossingPointsList = ({ crossingPoints, setCrossingPoints }) => {
	const moveItem = (index, pos) => {
		if (index - pos > crossingPoints.length || index - pos < 0) return;
		const newArray = [...crossingPoints];
		const item = newArray[index];
		newArray.splice(index, 1);
		newArray.splice(index - pos, 0, item);

		setCrossingPoints(newArray);
	};

	const deleteItem = (index) => {
		const newArray = [...crossingPoints];
		newArray.splice(index, 1);
		setCrossingPoints(newArray);
	};

	return (
		<ListGroup className="mt-3" as="ol" variant="flush" numbered>
			{crossingPoints.map((cp, index) => (
				<ListGroup.Item as="li" className="d-flex justify-content-between align-items-start" key={index}>
					<div className="ms-2 me-auto">
						<div className="fw-bold">{cp.name}</div>
						<div className="text-break">{cp.description}</div>
					</div>
					{index > 0 && index < crossingPoints.length && (
						<Button size="sm" variant="info" onClick={() => moveItem(index, 1)}>
							<FontAwesomeIcon icon={faUpLong} />
						</Button>
					)}
					{index >= 0 && index < crossingPoints.length - 1 && (
						<Button size="sm" variant="info" className="ms-1" onClick={() => moveItem(index, -1)}>
							<FontAwesomeIcon icon={faDownLong} />
						</Button>
					)}

					<Button size="sm" variant="danger" className="ms-1" onClick={() => deleteItem(index)}>
						<FontAwesomeIcon icon={faXmark} />
					</Button>
				</ListGroup.Item>
			))}
		</ListGroup>
	);
};

export default CrossingPointsList;
