import React, { useEffect } from "react";
import Map from "../../components/Map";
import { useLazyGetRouteQuery } from "../routes/routesApiSlice";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMountainSun, faPersonBiking, faPersonWalking } from "@fortawesome/free-solid-svg-icons";
import CrossingPointLink from "../../components/CrossingPointLink";
import { Image } from "react-bootstrap";

const TripItemLite = ({ trip }) => {
	const [getRoute, { data: route, isSuccess: routeIsSuccess }] = useLazyGetRouteQuery();

	useEffect(() => {
		if (trip?.route) {
			getRoute(trip.route);
		}
	}, [trip, getRoute]);

	return (
		<article className="d-flex mb-3">
			<div className="flex-shrink-0">
				{trip.route && routeIsSuccess ? (
					<Map
						zoom={9}
						center={[50.06978230955313, 14.38732867209974]}
						scrollWheelZoom={false}
						dragging={false}
						doubleClickZoom={false}
						zoomControl={false}
						width="150px"
						height="150px"
						coords={route}
					/>
				) : (
					<Image width={150} height={150} src="/images/noroute.png"></Image>
				)}
			</div>

			<div className={`flex-grow ms-3`}>
				<div>
					<Link className="text-decoration-none fw-bold" to={`/trips/${trip.id}`}>
						{trip.name}
					</Link>
				</div>
				<div className="text-secondary mb-1">
					<Link to={`/users/${trip.userId}`} className="text-decoration-none link-secondary">
						{trip.user.username}
					</Link>
					<span className="ms-1">{new Date(trip.createdAt).toLocaleDateString()}</span>
				</div>
				<div>
					<div className="text-muted">
						<span>
							<FontAwesomeIcon className="me-1" icon={trip.tripTypeId === 1 ? faPersonWalking : faPersonBiking} />
							{trip.distance}km
						</span>
						{trip.elevation && (
							<span className="ms-1">
								<FontAwesomeIcon className="me-1" icon={faMountainSun} />
								{trip.elevation}m
							</span>
						)}
					</div>
					<div>
						<Link
							className="text-decoration-none me-1"
							to={`/trips/?regionId=${trip.startPlace.district.regionId}&districtId=${trip.startPlace.district.id}&municipalityId=${trip.startPlaceId}`}
						>
							{trip.startPlace.name}
						</Link>
						{trip.startPlaceId !== trip.finishPlaceId && (
							<>
								{"-"}
								<Link
									className="text-decoration-none ms-1"
									to={`/trips/?regionId=${trip.finishPlace.district.regionId}&districtId=${trip.finishPlace.district.id}&municipalityId=${trip.finishPlaceId}`}
								>
									{trip.finishPlace.name}
								</Link>
							</>
						)}
						<span className="ms-1">dne {new Date(trip.date).toLocaleString("cs-CZ").slice(0,-3)}</span>
					</div>

					{trip.crossingPoints.length > 0 && (
						<div className="fs-6">
							{"Přes: "}
							{trip.crossingPoints.map((cp, index) => (
								<span key={index}>
									<CrossingPointLink cp={cp} />
									{index < trip.crossingPoints.length - 1 && ", "}
								</span>
							))}
						</div>
					)}
				</div>
			</div>
		</article>
	);
};

export default TripItemLite;
