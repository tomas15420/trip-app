import React, { useState } from "react";
import Badge from "react-bootstrap/Badge";
import ListGroup from "react-bootstrap/ListGroup";
import { Link } from "react-router-dom";
import { useGetReactionsByTripIdQuery } from "../reactions/reactionApiSlice";
import LoadingSpinner from "../../components/LoadingSpinner";
import CustomPagination from "../../components/CustomPagination";

const TripParticipants = ({ tripId, reactionType }) => {
	const [page, setPage] = useState(1);
	const PER_PAGE = 10;

	const {
		data: participants,
		isLoading,
		isError,
		isSuccess,
		error,
	} = useGetReactionsByTripIdQuery({
		tripId,
		limit: PER_PAGE,
		offset: (page - 1) * PER_PAGE,
		reactionType,
	});

	const handleChangePage = (page) => {
		setPage(page);
	};

	let content;
	if (isLoading) content = <LoadingSpinner />;
	if (isError)
		content = (
			<p className="d-flex justify-content-center m-2">Vyskytla se chyba, kód chyby: {error.status}, zkuste stránku načíčst znovu</p>
		);
	if (isSuccess)
		content = (
			<>
				{participants.total ? (
					<>
						<ListGroup variant="flush">
							{participants.participants?.map((participant, index) => {
								const type = participant.reactions.reactionTypeId === "PARTICIPATE" ? "Zúčastní se" : "Možná se zúčastní";
								return (
									<ListGroup.Item key={index} className="d-flex justify-content-between">
										<Link to={`/users/${participant.id}`} >{participant.username}</Link>
										<span>
											<Badge className="ms-1 text-wrap" bg={type === "Zúčastní se" ? "primary" : "secondary"}>
												{type}
											</Badge>
										</span>
									</ListGroup.Item>
								);
							})}
						</ListGroup>
						{participants?.total > PER_PAGE && (
							<>
								<hr />
								<div className="mt-2 d-flex justify-content-center">
									<CustomPagination perPage={PER_PAGE} count={participants?.total} active={page} handleChangePage={handleChangePage} />
								</div>
							</>
						)}
					</>
				) : (
					<p className="d-flex justify-content-center p-2">Touto reakcí zatím nikdo nereagoval</p>
				)}
			</>
		);
	return content;
};

export default TripParticipants;
