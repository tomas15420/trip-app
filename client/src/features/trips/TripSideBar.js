import React from "react";
import ListGroup from "react-bootstrap/ListGroup";
import Badge from "react-bootstrap/Badge";
import { Link } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import { useGetTripsQuery } from "./tripApiSlice";

const TripSideBar = () => {
    const {
        data: trips,
        isLoading,
        isSuccess,
        isError,
        error,
    } = useGetTripsQuery({
        order: "participates.desc",
        limit: 10,
        upcoming: true,
    });

    if (isLoading) return <LoadingSpinner className="mt-4" />;
    if (isError) return <p>{`Vyskytla se chyba při načítání (kód chyby: ${error.status})`}</p>
    if (isSuccess)
        return (
            <section>
                <h3>Nejpopulárnější výlety</h3>
                <ListGroup>
                    {trips?.trips?.map((trip) => {
                        return (
                            <ListGroup.Item
                                key={trip.id}
                                className="d-flex justify-content-between"
                            >
                                <Link to={`/trips/${trip.id}`}>
                                    {trip.name}
                                </Link>
                                <span>
                                    <Badge bg="primary">
                                        {trip.participates}
                                    </Badge>
                                </span>
                            </ListGroup.Item>
                        );
                    })}
                </ListGroup>
            </section>
        );
};
export default TripSideBar;
