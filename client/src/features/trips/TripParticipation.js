import React from "react";
import Button from "react-bootstrap/Button";
import { useReactToTripMutation, useDeleteTripReactionMutation } from "./tripApiSlice";
import useAuth from "../../hooks/useAuth";
import { useDispatch } from "react-redux";
import { showLogin } from "../auth/authSlice";
const TripParticipation = ({ trip }) => {
	const [reactToTrip] = useReactToTripMutation();
	const [deleteTripReaction] = useDeleteTripReactionMutation();
	const { userId } = useAuth();
	const dispatch = useDispatch();

	const handleParticipate = async (tripId, prev, participation) => {
		try {
			if (!userId) return dispatch(showLogin({ show: true }));
			if (!prev || prev !== participation) await reactToTrip({ tripId, reaction: participation }).unwrap();
			else await deleteTripReaction({ tripId }).unwrap();
		} catch (e) {
			alert(e?.status !== 500 && e?.data?.message ? e.data.message : "Chyba při komunikaci se serverem, zkuste to znovu ");
		}
	};

	const participate = trip?.user_participation;
	const tookPlace = new Date(trip.date) < new Date();

	return (
		<>
			<Button
				className="me-2 my-1"
				disabled={tookPlace}
				onClick={() => handleParticipate(trip.id, participate, "PARTICIPATE")}
				role="link"
				variant={participate === "PARTICIPATE" ? "success" : "outline-success"}
			>
				{`Zúčastním se (${trip?.participates || 0})`}
			</Button>
			<Button
				className="my-1"
				disabled={tookPlace}
				onClick={() => handleParticipate(trip.id, participate, "MAYBE_PARTICIPATE")}
				role="link"
				variant={participate === "MAYBE_PARTICIPATE" ? "secondary" : "outline-secondary"}
			>
				{`Možná se zúčastním (${trip?.maybe_participates || 0})`}
			</Button>
		</>
	);
};

export default TripParticipation;
