import React from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import ReactDatePicker, { registerLocale } from "react-datepicker";
import FormInput from "../../components/Form/FormInput";
import { useGetTripsTypesQuery } from "./tripApiSlice";
import "react-datepicker/dist/react-datepicker.css";
import { Field } from "formik";
import cs from "date-fns/locale/cs";
import MunicipalitySearch from "../location/MunicipalitySearch";
import Parser from "@bovan/gpx2geojson";
registerLocale("cs", cs);

const CreateTripFields = ({ formikProps, mapData, setMapData }) => {
	const { touched, values, errors, handleChange, handleBlur, setFieldValue, setFieldTouched } = formikProps;

	const handleGpxLoad = (file) => {
		if (file && file.name.endsWith(".gpx")) {
			const reader = new FileReader();
			reader.onload = async (e) => {
				try {
					const data = e.target.result;
					const json = await Parser(data);
					const coords = json?.geometry?.coordinates?.map((x) => ({ lat: x[0], lng: x[1] }));
					const distance = Math.round(json.properties.distance / 1000);
					const elevation = json.properties.elevationPos;
					setFieldValue("distance", distance);
					if (elevation) setFieldValue("elevation", elevation);
					setMapData(coords);
				} catch (e) {
					alert("Došlo k chybě při nahrávání GPX souboru, zkuste to znovu, nebo vyzkoušejte jiný GPX soubor");
				}
			};
			reader.readAsText(file);
		} else {
			setMapData([]);
			setFieldValue("distance", "");
			setFieldValue("elevation", "");
		}
	};

	const { data: tripTypes, isLoading: tripTypesIsLoading, isSuccess: tripTypesIsSuccess } = useGetTripsTypesQuery();
	return (
		<Row className="gy-2">
			<FormInput md={12} type="text" label="Název" controlId="name" maxLength="64" placeholder="Zadejte název výletu" />
			<FormInput
				md={12}
				type="text"
				label="Popis"
				maxLength="500"
				controlId="description"
				textarea
				rows="5"
				placeholder="Zadejte popis výletu"
			/>
			<Form.Group as={Col} md="6" controlId="date">
				<Form.Label>Datum</Form.Label>
				<Field>
					{() => (
						<>
							<Form.Control
								locale={"cs"}
								as={ReactDatePicker}
								selected={values.date}
								autoComplete="off"
								showTimeSelect
								timeCaption="Čas"
								placeholderText="Zadejte datum konání"
								timeIntervals={15}
								minDate={new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 3)}
								dateFormat="dd.MM.yyyy HH:mm"
								onChange={(date) => {
									setFieldTouched("date");
									setFieldValue("date", date || "");
								}}
								isInvalid={!!touched.date && !!errors.date}
								isValid={!!touched.date && !errors.date}
							/>
							<Form.Control.Feedback type="invalid" className={!!touched.date && !!errors.date && "d-block"}>
								{errors.date}
							</Form.Control.Feedback>
						</>
					)}
				</Field>
			</Form.Group>

			<Form.Group controlId="tripType" as={Col} md={6}>
				<Form.Label>Typ výletu</Form.Label>
				<Form.Select
					value={values.tripType}
					onChange={handleChange}
					onBlur={handleBlur}
					isInvalid={touched.tripType && errors.tripType}
					isValid={touched.tripType && !errors.tripType}
				>
					<option value="0">Vyberte typ výletu</option>
					{!tripTypesIsLoading &&
						tripTypesIsSuccess &&
						tripTypes.map((type) => (
							<option key={type.id} value={type.id}>
								{type.type}
							</option>
						))}
				</Form.Select>
				<Form.Control.Feedback type="invalid">{errors.tripType}</Form.Control.Feedback>
			</Form.Group>
			<FormInput
				type="number"
				label="Vzdálenost"
				controlId="distance"
				md={6}
				placeholder="Zadejte počet kilometrů"
				disabled={mapData.length > 0}
			/>
			<FormInput
				type="number"
				label="Převýšení"
				controlId="elevation"
				md={6}
				optional
				placeholder="Zadejte převýšení v metrech (nepovinné)"
			/>
			<FormInput
				type="text"
				label="Webová stránka"
				controlId="url"
				md={12}
				optional
				placeholder="Zadejte URL pro více informací (nepovinné)"
			/>

			<Form.Group controlId="startPlace.id" as={Col} md={6}>
				<Form.Label>Startovací pozíce</Form.Label>
				<MunicipalitySearch
					id="startPlace.id"
					inputProps={{
						id: "startPlace.id",
					}}
					onChange={(selected) => {
						setFieldValue("startPlace.id", selected[0]?.id || 0);
					}}
					onBlur={handleBlur}
					defaultInputValue={values.startPlace?.name || ""}
					placeholder="Vyhledejte startovací místo"
					isInvalid={!!touched.startPlace && !!errors.startPlace}
					isValid={touched.startPlace && !errors.startPlace}
					emptyLabel="Nenalezeny žádné výsledky"
					promptText="Piš pro hledání"
					searchText="Hledám..."
				/>
				<Form.Control.Feedback type="invalid" className={!!touched.startPlace && !!errors.startPlace && "d-block"}>
					{errors.startPlace?.id}
				</Form.Control.Feedback>
			</Form.Group>
			<Form.Group controlId="finishPlace.id" as={Col} md={6}>
				<Form.Label>Cílová pozíce</Form.Label>
				<MunicipalitySearch
					id="finishPlace.id"
					inputProps={{
						id: "finishPlace.id",
					}}
					onChange={(selected) => {
						setFieldValue("finishPlace.id", selected[0]?.id || 0);
					}}
					onBlur={handleBlur}
					placeholder="Vyhledejte cílové místo"
					isInvalid={!!touched.finishPlace && !!errors.finishPlace}
					isValid={touched.finishPlace && !errors.finishPlace}
					defaultInputValue={values.finishPlace?.name || ""}
					emptyLabel="Nenalezeny žádné výsledky"
					promptText="Piš pro hledání"
					searchText="Hledám..."
				/>
				<Form.Control.Feedback type="invalid" className={!!touched.finishPlace && !!errors.finishPlace && "d-block"}>
					{errors.finishPlace?.id}
				</Form.Control.Feedback>
			</Form.Group>
			<Form.Group controlId="route">
				<Form.Label>Trasa</Form.Label>
				<Form.Control
					type="file"
					accept=".gpx"
					onChange={(e) => {
						setFieldValue("route", e.currentTarget.files[0]);
						handleGpxLoad(e.target.files[0]);
					}}
					onBlur={handleBlur}
					isInvalid={touched.route && !!errors.route}
				/>
				<Form.Text>Povolené formáty: .gpx</Form.Text>
				<Form.Control.Feedback type="invalid">{errors.route}</Form.Control.Feedback>
			</Form.Group>
		</Row>
	);
};

export default CreateTripFields;
