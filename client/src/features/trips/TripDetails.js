import React, { useEffect } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import Map from "../../components/Map";
import { useLazyGetRouteQuery } from "../routes/routesApiSlice";

import CrossingPointLink from "../../components/CrossingPointLink";

const TripDetails = ({ trip }) => {
	const [getRoute, { data, isSuccess }] = useLazyGetRouteQuery();

	useEffect(() => {
		if (trip.route) {
			getRoute(trip.route);
		}
	}, [trip, getRoute]);

	return (
		<>
			<Row>
				<Col lg={7}>
					<Card.Title>{trip.name}</Card.Title>
					<Card.Subtitle className="text-muted">
						Autor: <Link to={`/users/${trip.userId}`}>{trip.user.username}</Link>
						<br />
						Datum: {new Date(trip.createdAt).toLocaleDateString()}
					</Card.Subtitle>
					<hr />
					<span className="fw-bold">Typ výletu: </span>
					<Link to={`/trips/?type=${trip.tripTypeId}`}>{trip.trip_type.type}</Link>
					<br />
					<span className="fw-bold">Datum konání: </span>
					{new Date(trip.date).toLocaleString().slice(0, -3)}
					<br />
					<span className="fw-bold">Start:</span>{" "}
					<Link
						to={`/trips/?regionId=${trip.startPlace.district.regionId}&districtId=${trip.startPlace.district.id}&municipalityId=${trip.startPlaceId}`}
					>
						{trip.startPlace.name}
					</Link>
					<br />
					<span className="fw-bold">Cíl:</span>{" "}
					<Link
						to={`/trips/?regionId=${trip.finishPlace.district.regionId}&districtId=${trip.finishPlace.district.id}&municipalityId=${trip.finishPlaceId}`}
					>
						{trip.finishPlace.name}
					</Link>
					<br />
					<span className="fw-bold">Vzdálenost: {trip.distance}km</span>
					<br />
					{trip.elevation && (
						<>
							<span className="fw-bold">Převýšení: {trip.elevation}m</span>
							<br />
						</>
					)}
					{trip.url && (
						<>
							<span className="fw-bold">Více informací:</span>{" "}
							<a href={trip.url} rel="noreferrer" target="_blank">
								{trip.url}
							</a>
						</>
					)}
				</Col>
				{trip.crossingPoints.length > 0 && (
					<Col className="mt-3 mt-md-0" lg={5}>
						<h5>Přes:</h5>
						<p>
							{trip.crossingPoints.map((municipality, index) => (
								<span key={municipality.id}>
									<CrossingPointLink cp={municipality}>{municipality.name}</CrossingPointLink>
									{index !== trip.crossingPoints.length - 1 && ", "}
								</span>
							))}
						</p>
					</Col>
				)}
				<Col xs={12}>
					<hr />
					<h5>Popis:</h5>
					{trip.description}
					{isSuccess && data?.length > 0 && (
						<div className="mt-3 mb-2">
							<Map coords={data} zoom={9} center={[50.06978230955313, 14.38732867209974]} scrollWheelZoom={true} />
						</div>
					)}
				</Col>
			</Row>
		</>
	);
};

export default TripDetails;
