import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { date, object, string, number, mixed } from "yup";
import useAuth from "../../hooks/useAuth";
import useTitle from "../../hooks/useTitle";
import { showLogin } from "../auth/authSlice";
import CreateTripCrossingPoints from "./CreateTripCrossingPoints";
import CreateTripFields from "./CreateTripFields";
import { useCreateTripMutation } from "./tripApiSlice";
import Error from "../../components/Error";
import Map from "../../components/Map";

const CreateTrip = () => {
	useTitle("Vytvoření výletu");
	const { userId } = useAuth();
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const location = useLocation();
	const [error, setError] = useState("");
	const [mapData, setMapData] = useState([]);
	const [crossingPoints, setCrossingPoints] = useState([]);

	useEffect(() => {
		if (!userId) {
			navigate(location?.state?.from || "/");
			dispatch(showLogin({ show: true }));
		}
	}, [userId, navigate, dispatch, location]);

	const [createTrip] = useCreateTripMutation();

	const minDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 3);
	const schema = object({
		name: string().required("Název je vyžadován").min(15, "Název je příliš krátký").max(64, "Název je příliš dlouhý"),
		description: string().required("Popis je vyžadován").min(15, "Popis je moc krátký").max(500, "Popis je moc dlouhý"),
		date: date().required("Datum konání je vyždaován").min(minDate.toDateString(), "Zvolte datum alespoń 3 dny dopředu"),
		distance: number()
			.typeError("Vzádlenost musí být číslo")
			.required("Vzádlenost je vyžadována")
			.min(1, "Výlet musí být dlouhý alespoň 1 km")
			.max(1000, "Výlet nesmí být delší 1000 km"),
		elevation: number()
			.typeError("Převýšení musí být číslo")
			.optional()
			.min(1, "Minimální převýšení musí být 1m")
			.max(10000, "Maximální převýšení může být 10 000m"),
		startPlace: object({
			id: number().required("Startovní pozice je vyžadována").min(1, "Chybně zadána startovní pozice"),
		}),
		finishPlace: object({
			id: number().required("Cílová pozice je vyžadována").min(1, "Chybně zadána cílová pozice"),
		}),
		tripType: number().min(1, "Vyberte typ výletu").required("Typ je vyžadován"),
		url: string()
			.matches(
				/[(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/gi,
				"Zadejte platnou URL adresu"
			)
			.optional(),
		route: mixed()
			.optional()
			.test("typeValid", "Můžete použít pouze .gpx soubory", (value) => !value || value?.name.endsWith(".gpx")),
	});

	const handleSubmit = async (values, { resetForm }) => {
		try {
			const body = {
				...values,
				startPlace: values.startPlace.id,
				finishPlace: values.finishPlace.id,
				route: mapData.length > 0 ? mapData : undefined,
				elevation: Number.isInteger(values.elevation) ? values.elevation : undefined,
				url: values.url.length > 0 ? values.url : undefined,
				crossingPoints: crossingPoints,
			};
			const res = await createTrip(body).unwrap();
			resetForm({ values: "" });
			navigate(`/trips/${res.trip?.id}`);
		} catch (e) {
			setError(e);
		}
	};

	return (
		<Container as="main" className="mt-3">
			<Row className="gy-3 mb-3">
				<Col md="8">
					<Error error={error} onClose={() => setError("")} />
					<Card>
						<Card.Body className="p-4">
							<Card.Title className="mb-3">Naplánování výletu</Card.Title>
							<Formik
								validationSchema={schema}
								initialValues={{
									name: "",
									description: "",
									route: "",
									date: "",
									tripType: "0",
									distance: "",
									elevation: "",
									url: "",
									startPlace: { id: 0 },
									finishPlace: { id: 0 },
								}}
								onSubmit={handleSubmit}
								validateOnMount={true}
							>
								{(props) => (
									<Form noValidate onSubmit={props.handleSubmit}>
										<CreateTripFields
											formikProps={{
												...props,
											}}
											mapData={mapData}
											setMapData={setMapData}
										/>
										<Map coords={mapData} zoom={9} center={[50.06978230955313, 14.38732867209974]} scrollWheelZoom={true} />

										<div className="d-flex justify-content-center mt-3">
											<Button disabled={!props.isValid || props.isSubmitting} type="submit">
												Vytvořit výlet
											</Button>
										</div>
									</Form>
								)}
							</Formik>
						</Card.Body>
					</Card>
				</Col>
				<Col md={4}>
					<Card>
						<Card.Body>
							<Card.Title>Průchozí body</Card.Title>
							<CreateTripCrossingPoints crossingPoints={crossingPoints} setCrossingPoints={setCrossingPoints} />
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
};

export default CreateTrip;
