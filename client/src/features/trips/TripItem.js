import React, { useEffect } from "react";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import TripParticipation from "./TripParticipation";
import Stars from "../../components/Stars";
import { Col, Row } from "react-bootstrap";
import { useLazyGetRouteQuery } from "../routes/routesApiSlice";
import Map from "../../components/Map";

const TripItem = ({ trip, ...props }) => {
	const [getRoute, { data, isSuccess }] = useLazyGetRouteQuery();

	useEffect(() => {
		if (trip.route) {
			getRoute(trip.route);
		}
	}, [trip, getRoute]);

	return (
		<Card className="my-4" {...props}>
			<Card.Body>
				<div className="d-flex">
					<Card.Title className="flex-fill">
						<Link className="text-decoration-none" to={`/trips/${trip.id}`}>
							{trip.name}
						</Link>
						<Card.Subtitle className="text-muted mt-2">
							Autor:{" "}
							<Link to={`/users/${trip.userId}`} className="text-decoration-none">
								{trip.user.username}
							</Link>
							<br />
							Vytvořeno dne: {new Date(trip.createdAt).toLocaleDateString()}
						</Card.Subtitle>
					</Card.Title>
					{trip.avgStars && <Stars stars={trip.avgStars} maxStars={5} />}
				</div>
				<hr />
				<Row>
					<Col md={6}>
						<div className="card-text">{"Datum konání: " + new Date(trip.date).toLocaleDateString("cs-CZ")}</div>
						<div className="card-text">Start: {trip.startPlace.name}</div>
						<div className="card-text">Cíl: {trip.finishPlace.name}</div>
						<div className="card-text">Typ: {trip.trip_type.type}</div>
						<div className="card-text">Vzdálenost: {trip.distance}km</div>
						{trip.elevation && <div className="card-text">Převýšení: {trip.elevation}m</div>}
					</Col>
					<Col>
						{!!trip.crossingPoints.length && "Přes: "}
						{trip.crossingPoints?.map((crossPoint, index) => {
							return (
								<span key={crossPoint.id}>
									<Link
										to={`/trips/?regionId=${crossPoint.district.regionId}&districtId=${crossPoint.district.id}&municipalityId=${crossPoint.id}`}
									>
										{crossPoint.name}
									</Link>
									{index !== trip.crossingPoints.length - 1 && ", "}
								</span>
							);
						})}
					</Col>
				</Row>
				{isSuccess && data.length > 0 && (
					<>
						<div className="mt-2">
							<Link to={`/trips/${trip.id}`}>
								<Map
									coords={data}
									zoom={9}
									center={[50.06978230955313, 14.38732867209974]}
									height="200px"
									scrollWheelZoom={false}
									dragging={false}
									doubleClickZoom={false}
									zoomControl={false}
								/>
							</Link>
						</div>
					</>
				)}
			</Card.Body>
			<Card.Footer>
				<TripParticipation trip={trip} />
			</Card.Footer>
		</Card>
	);
};

export default TripItem;
