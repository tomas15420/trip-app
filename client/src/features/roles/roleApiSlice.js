import { apiSlice } from "../../app/api/apiSlice";

export const roleApiSlice = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        getRoles: builder.query({
            query: () => "/roles",
            validateStatus: (response, result) =>
                response.status === 200 && !result.isError,
        }),
    }),
});

export const { useLazyGetRolesQuery } = roleApiSlice;
