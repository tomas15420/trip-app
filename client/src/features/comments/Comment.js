import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashCan, faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import { useDeleteCommentMutation } from "./commentApiSlice";
import useAuth from "../../hooks/useAuth";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import Error from "../../components/Error";

const Comment = ({ comment, handleEditButton, ...props }) => {
	const { userId, roles } = useAuth();
	const [deleteComment] = useDeleteCommentMutation();
	const [error, setError] = useState("");

	const handleDelete = async (commentId) => {
		try {
			await deleteComment(commentId).unwrap();
		} catch (e) {
			setError(e);
		}
	};

	const created = new Date(comment.createdAt);
	const edited = new Date(comment.updatedAt);
	return (
		<>
			<Error error={error} onClose={() => setError("")} />
			<article {...props} className="d-flex mb-3 border-bottom" style={{ position: "relative" }}>
				<div className="flex-shrink-0">
					{comment.user ? (
						<Link to={`/users/${comment.user?.id}`}>
							<Image
								rounded
								className="me-3"
								width="80"
								height="80"
								src={comment.user?.avatarPath ? `${comment.user.avatarPath}?width=80&height=80&fit=contain` : "/images/noavatar.jpg"}
							/>
						</Link>
					) : (
						<Image rounded className="me-3" width="80" height="80" src="/images/noavatar.jpg" />
					)}
				</div>
				<div className="flex-grow-1">
					<h5 className="mb-0">
						{comment.user ? <Link to={`/users/${comment.user?.id}`}>{comment.user?.username}</Link> : "Odstraněný uživatel"}
					</h5>
					<small className="text-muted">
						{created.toLocaleString().slice(0, -3)}
						{edited.getTime() !== created.getTime() && (
							<OverlayTrigger
								placement="top"
								overlay={<Tooltip style={{ position: "fixed" }}>Upraveno: {edited.toLocaleString().slice(0, -3)}</Tooltip>}
							>
								{({ ref, ...trigerHandler }) => <FontAwesomeIcon {...trigerHandler} ref={ref} className="ms-1" icon={faQuestionCircle} />}
							</OverlayTrigger>
						)}
					</small>
					<p className="text-break">{comment.text}</p>
				</div>
				{userId && (comment.user?.id === userId || roles.includes("ADMIN")) && (
					<div style={{ position: "absolute", right: 0 }}>
						<Button role="link" variant="primary" size="sm" className="me-1" onClick={() => handleEditButton(comment.id, comment.text)}>
							<FontAwesomeIcon icon={faEdit} />
						</Button>
						<Button role="link" variant="danger" size="sm" onClick={() => handleDelete(comment.id)}>
							<FontAwesomeIcon icon={faTrashCan} />
						</Button>
					</div>
				)}
			</article>
		</>
	);
};

export default Comment;
