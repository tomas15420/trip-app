import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Formik } from "formik";
import { object, string } from "yup";
import { useEditCommentMutation, usePostCommentMutation } from "./commentApiSlice";

import Form from "react-bootstrap/Form";
import Spinner from "react-bootstrap/Spinner";
import Button from "react-bootstrap/Button";
import FormInput from "../../components/Form/FormInput";
import Error from "../../components/Error";

const CommentForm = ({ editCommentId, handleCancelEdit, formik }) => {
	const { tripId } = useParams();

	const [postComment] = usePostCommentMutation();
	const [editComment] = useEditCommentMutation();
	const [error, setError] = useState("");

	const schema = object({
		text: string()
			.required("Komentář musí být vyplněn")
			.min(1, "Komentář musí být alespoň 1 znak dlouhý")
			.max(256, "Komentář může být maximálně 256 znaků dlouhý"),
	});

	const handeSubmit = async (values, { resetForm }) => {
		try {
			if (!editCommentId) {
				await postComment({
					tripId,
					comment: values.text,
				}).unwrap();
			} else {
				await editComment({
					commentId: editCommentId,
					comment: values.text,
				}).unwrap();
				handleCancelEdit();
			}
			resetForm({ values: "" });
		} catch (e) {
			setError(e);
		}
	};

	return (
		<>
			<Error error={error} onClose={() => setError("")} />
			<Formik innerRef={formik} validationSchema={schema} initialValues={{ text: "" }} onSubmit={handeSubmit}>
				{({ handleSubmit, isValid, isSubmitting }) => (
					<Form onSubmit={handleSubmit}>
						<FormInput
							className="mb-2"
							as="textarea"
							controlId="text"
							rows="4"
							maxLength="256"
							placeholder="Zde zadejte váš komentář"
							isInvalid={!isValid}
							style={{ resize: "none" }}
						/>

						<div className="d-flex justify-content-end">
							<Button variant="primary" disabled={!isValid || isSubmitting} type="submit">
								{isSubmitting && <Spinner as="span" className="me-2" animation="border" role="status" size="sm" aria-hidden="true" />}
								{!editCommentId ? "Přidat komentář" : "Upravit komentář"}
							</Button>
							{editCommentId && (
								<Button variant="secondary" className="ms-2" onClick={handleCancelEdit}>
									Zrušit
								</Button>
							)}
						</div>
						<hr />
					</Form>
				)}
			</Formik>
		</>
	);
};

export default CommentForm;
