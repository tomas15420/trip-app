import { apiSlice } from "../../app/api/apiSlice";

export const commentsApiSlice = apiSlice.injectEndpoints({
	endpoints: (builder) => ({
		getComments: builder.query({
			query: ({ tripId, ...filters }) => {
				const params = Object.entries(filters)
					.filter(([key, value]) => value !== "")
					.map(([key, value]) => `${key}=${value}`)
					.join("&");
				return {
					url: `/trips/${tripId}/comments${params ? `?${params}` : ""}`,
				};
			},
			providesTags: (result) => (result ? [...result.comments?.map(({ id }) => ({ type: "Comment", id })), "Comment"] : ["Comment"]),
		}),
		postComment: builder.mutation({
			query: ({ tripId, comment }) => ({
				url: `/trips/${tripId}/comments`,
				method: "POST",
				body: { text: comment },
			}),
			invalidatesTags: ["Comment"],
		}),
		editComment: builder.mutation({
			query: ({ commentId, comment }) => ({
				url: `/comments/${commentId}`,
				method: "PATCH",
				body: { text: comment },
			}),
			invalidatesTags: (result, error, { id }) => [{ type: "Comment", id }],
		}),
		deleteComment: builder.mutation({
			query: (commentId) => ({
				url: `/comments/${commentId}`,
				method: "DELETE",
			}),
			invalidatesTags: (result, error, { id }) => [{ type: "Comment", id }],
		}),
	}),
});

export const { useGetCommentsQuery, usePostCommentMutation, useEditCommentMutation, useDeleteCommentMutation } = commentsApiSlice;
