import React, { useRef, useState } from "react";
import Card from "react-bootstrap/Card";
import { useParams } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import useAuth from "../../hooks/useAuth";
import { useGetCommentsQuery } from "./commentApiSlice";

import CommentForm from "./CommentForm";
import Comment from "./Comment";
import CustomPagination from "../../components/CustomPagination";

const Comments = () => {
	const PER_PAGE = 10;

	const { tripId } = useParams();
	const { userId } = useAuth();

	const [editCommentId, setEditCommentId] = useState(null);
	const [page, setPage] = useState(1);
	const formik = useRef(null);

	const {
		data: comments,
		isLoading,
		isSuccess,
		isError,
	} = useGetCommentsQuery(
		{ tripId, order: "createdAt.desc", limit: PER_PAGE, offset: (page - 1) * PER_PAGE },
		{ refetchOnMountOrArgChange: true }
	);

	const scrollTo = (to) => {
		const scrollTo = document.querySelector(to);
		if (scrollTo) {
			scrollTo.scrollIntoView({ behavior: "auto", alignToTop: true });
		}
	};

	const handleEditButton = (commentId, text) => {
		scrollTo("#comment-heading");
		setEditCommentId(commentId);
		formik.current.setFieldValue("text", text);
	};

	const handleCancelEdit = () => {
		setEditCommentId(null);
		formik.current.resetForm({ values: null });
	};

	if (isLoading) {
		return <LoadingSpinner />;
	} else if (isError) {
		return <p className="d-flex justify-content-center mt-4">Chyba při načítání komentářů</p>;
	} else if (isSuccess) {
		return (
			<>
				<Card.Title id="comment-heading">Komentáře</Card.Title>
				{comments && <Card.Subtitle className="text-muted mb-3">Počet komentářů: {comments.total || "0"}</Card.Subtitle>}
				{userId ? (
					<CommentForm formik={formik} handleCancelEdit={handleCancelEdit} editCommentId={editCommentId} />
				) : (
					<p className="d-flex justify-content-center">Pro přidání komentáře se přihlašte</p>
				)}
				{comments?.total === 0 && <p className="d-flex justify-content-center">Buďte první kdo napíše komentář</p>}
				<section id="comments">
					{comments?.comments.map((comment) => (
						<Comment key={comment.id} comment={comment} handleEditButton={handleEditButton} />
					))}
				</section>
				{comments?.total > PER_PAGE && (
					<div className="d-flex justify-content-center">
						<CustomPagination
							active={page}
							count={comments.total}
							perPage={PER_PAGE}
							handleChangePage={(page) => {
								scrollTo("#comments");
								setPage(page);
							}}
						/>
					</div>
				)}
			</>
		);
	}
	return "";
};

export default Comments;
