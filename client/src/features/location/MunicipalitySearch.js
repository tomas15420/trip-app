import React from "react";
import { useLazyGetMunicipalitiesBySearchQuery } from "./locationApiSlice";
import { AsyncTypeahead } from "react-bootstrap-typeahead";
import { forwardRef } from "react";

const MunicipalitySearch = forwardRef((props, ref) => {
	const [searchMunicipalities, { data: municipalities, isLoading }] = useLazyGetMunicipalitiesBySearchQuery();

	const customSearchMunicipalities = (query) => {
		if (query?.length === 0) return;
		searchMunicipalities(
			{
				search: query,
				limit: 20,
				order: "name.asc",
			},
			true
		);
	};
	return (
		<AsyncTypeahead
			{...props}
			filterBy={() => true}
			ref={ref}
			options={municipalities?.municipalities}
			labelKey={(option) => `${option.name}, ${option.district.name}, ${option.district.region.name}`}
			isLoading={isLoading}
			onSearch={(query) => customSearchMunicipalities(query)}
			onFocus={(e) => customSearchMunicipalities(e.target.value.split(",")[0])}
			useCache={false}
		/>
	);
});

export default MunicipalitySearch;
