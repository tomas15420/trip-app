import { apiSlice } from "../../app/api/apiSlice";

export const locationApiSlice = apiSlice.injectEndpoints({
    endpoints: (builder) => ({
        getRegions: builder.query({
            query: () => "/location/regions?order=name.asc",
        }),
        getDistrictsByRegion: builder.query({
            query: (regionId) =>
                `/location/districts?regionId=${regionId}&order=name.asc`,
        }),
        getMunicipalitiesByDistrict: builder.query({
            query: (districtId) =>
                `/location/municipalities?districtId=${districtId}&limit=200&order=name.asc`,
        }),
        getMunicipalitiesBySearch: builder.query({
            query: (filters) => {
                const params = Object.entries(filters)
                    .filter(([key, value]) => value !== "")
                    .map(([key, value]) => `${key}=${value}`)
                    .join("&");
                return `/location/municipalities?${params}`;
            },
        }),
        getDistricts: builder.query({
            query: (filters) => {
                const params = Object.entries(filters)
                    .filter(([key, value]) => value !== "")
                    .map(([key, value]) => `${key}=${value}`)
                    .join("&");
                return `/location/districts?${params}`;
            },
        }),
    }),
});

export const { useGetRegionsQuery } = locationApiSlice;
export const { useLazyGetDistrictsByRegionQuery } = locationApiSlice;
export const { useLazyGetMunicipalitiesByDistrictQuery } = locationApiSlice;
export const { useLazyGetMunicipalitiesBySearchQuery } = locationApiSlice;
export const { useGetDistrictsQuery} = locationApiSlice;
