import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import FormInput from "../../components/Form/FormInput";
import Alert from "react-bootstrap/Alert";
import { Formik } from "formik";
import { object, string } from "yup";

import { selectShowLogin, setCredentials, showLogin, showRegister } from "./authSlice";
import { useLoginMutation } from "./authApiSlice";
import { useDispatch, useSelector } from "react-redux";
import usePersist from "../../hooks/usePersist";

const LoginModal = () => {
	const [errMsg, setErrMsg] = useState("");
	const [login] = useLoginMutation();
	const dispatch = useDispatch();

	// eslint-disable-next-line
	const [persist, setPersist] = usePersist();

	const show = useSelector(selectShowLogin);

	const schema = object({
		username: string().required("Uživatelské jméno/E-mail je vyžadováno"),
		password: string().required("Heslo je vyžadováno"),
	});

	const handleClose = () => {
		setErrMsg("");
		dispatch(showLogin({ show: false }));
	};

	const handleSubmit = async (values, { resetForm }) => {
		try {
			const userData = await login(values).unwrap();
			dispatch(
				setCredentials({
					token: userData.accessToken,
					user: userData.user,
				})
			);
			setPersist(true);
			resetForm({ values: "" });
			handleClose();
		} catch (e) {
			if (e.status === 400) {
				setErrMsg("Nezadáno uživatelské jméno nebo heslo");
			} else if (e.status === 404) {
				setErrMsg("Uživatel s těmito údaji neexistuje");
			} else if (e.status === 401) {
				setErrMsg("Nesprávné heslo");
			} else {
				setErrMsg("Přihlášení selhalo");
			}
		}
	};

	const handleRegister = (e) => {
		e.preventDefault();
		dispatch(showLogin({ show: false }));
		dispatch(showRegister({ show: true }));
	};

	return (
		<Modal show={show} onHide={handleClose} centered>
			<Modal.Header closeButton>
				<Modal.Title>Přihlášení</Modal.Title>
			</Modal.Header>
			<Formik
				validationSchema={schema}
				initialValues={{ username: "", password: "" }}
				validateOnMount="true"
				onSubmit={handleSubmit}
			>
				{({ handleSubmit, isValid, isSubmiting }) => (
					<Form noValidate onSubmit={handleSubmit}>
						<Modal.Body>
							<Alert variant="danger" className={!errMsg ? "d-none" : undefined}>
								{errMsg}
							</Alert>
							<Row className="g-3">
								<FormInput type="text" md={12} label="Uživatelské jméno/E-mail" controlId="username" validateValid={false} />
								<FormInput type="password" md={12} label="Heslo" controlId="password" autoComplete="password" validateValid={false} />
							</Row>
							<p className="float-sm-end m-0">
								Nemáte účet? <Button variant="link" className="m-0 p-0 border-0" onClick={handleRegister}>registrovat se</Button>
							</p>
							<div className="clearfix"></div>
						</Modal.Body>
						<Modal.Footer>
							<Button disabled={!isValid || isSubmiting} variant="primary" type="submit">
								Přihlásit
							</Button>
							<Button variant="secondary" onClick={handleClose}>
								Zavřít
							</Button>
						</Modal.Footer>
					</Form>
				)}
			</Formik>
		</Modal>
	);
};

export default LoginModal;
