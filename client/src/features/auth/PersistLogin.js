import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Outlet } from "react-router-dom";
import usePersist from "../../hooks/usePersist";
import { useRefreshMutation } from "./authApiSlice";
import { selectCurrentToken, showLogin } from "./authSlice";
import LoadingSpinner from "../../components/LoadingSpinner";
import Alert from "react-bootstrap/Alert";
import Container from "react-bootstrap/Container";

const PersistLogin = () => {
	const [persist] = usePersist();
	const token = useSelector(selectCurrentToken);
	const [success, setSuccess] = useState(false);
	const effectRan = useRef(false);
	const dispatch = useDispatch();
	const [show, setShow] = useState(true);
	const [refresh, { isLoading, isSuccess, isError }] = useRefreshMutation();

	useEffect(() => {
		if (effectRan.current === true || process.env.NODE_ENV !== "development") {
			const verifyRefreshToken = async () => {
				try {
					await refresh();
					setSuccess(true);
				} catch (e) {
					console.log(e);
				}
			};
			if (!token && persist) verifyRefreshToken();
		}
		return () => (effectRan.current = true);
		// eslint-disable-next-line
	}, []);

	let content;
	if (!persist) content = <Outlet />;
	else if (isLoading) content = <LoadingSpinner className="mt-5" />;
	else if (isError) {
		content = (
			<>
				{!token && show && (
					<div className="bg-dark py-2">
						<Container>
							<Alert variant="info" className="m-0" onClose={() => setShow(false)} dismissible>
								<span>Byl jste automaticky odhlášen po delší době neaktivity, prosím</span>
								<Link className="ms-1" role="button" onClick={() => dispatch(showLogin({ show: "true" }))}>
									přihlaste se znovu
								</Link>
							</Alert>
						</Container>
					</div>
				)}

				<Outlet />
			</>
		);
	} else if (isSuccess && success) content = <Outlet />;

	return content;
};

export default PersistLogin;
