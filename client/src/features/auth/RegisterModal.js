import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import FormInput from "../../components/Form/FormInput";
import Alert from "react-bootstrap/Alert";
import { object, string, ref } from "yup";
import { Formik } from "formik";
import { useRegisterMutation } from "./authApiSlice";
import { useDispatch, useSelector } from "react-redux";
import { selectShowRegister, setCredentials, showRegister } from "./authSlice";
import { axiosApi } from "../../app/api/axios";
import usePersist from "../../hooks/usePersist";

const RegisterModal = () => {
	const dispatch = useDispatch();

	const show = useSelector(selectShowRegister);

	const [register] = useRegisterMutation();
	const [errMsg, setErrMsg] = useState("");

	//eslint-disable-next-line
	const [persist, setPersist] = usePersist();

	const handleClose = () => {
		setErrMsg("");
		dispatch(showRegister({ show: false }));
	};
	const handleSubmit = async (values, { resetForm }) => {
		const { emailAgain: _, ...newValues } = values;
		try {
			const res = await register(newValues).unwrap();
			setPersist(true);
			dispatch(
				setCredentials({
					token: res.accessToken,
					user: res.user,
				})
			);
			resetForm({ values: "" });
			handleClose();
		} catch (e) {
			if (e.status === 400) {
				setErrMsg(e.data.message);
			} else {
				setErrMsg("Registrace selhala, zkuste to znovu");
			}
		}
	};

	const schema = object({
		username: string()
			.min(3, "Příliš krátké")
			.max(30, "Příliš dlouhé")
			.required("Uživatelské jéno je vyžadováno")
			.test("usernameCheck", "Uživatelské jméno již někdo používá", async (value) => {
				try {
					const { status, data } = await axiosApi.get(`/users/?username=${value}&limit=1`);
					return status === 200 && data?.count === 0;
				} catch (e) {
					return false;
				}
			}),
		firstname: string()
			.matches(/^[A-ZÁ-Ž][a-zá-ž]{1,29}$/, "Jméno musí začínat velkým písmenem a nesmí obsahovat nepovolené znaky")
			.min(2, "Příliš krátké")
			.max(30, "Příliš dlouhé")
			.test(
				"firstnameCheck",
				"Křestní jméno musí být vyplněno, když je zadáno příjmení",
				(value, context) => !context.parent.lastname || !!value
			),
		lastname: string()
			.matches(/^[A-ZÁ-Ž][a-zá-ž]{1,29}$/, "Příjmení musí začínat velkým písmenem, a nesmí obsahovat nepovolené znaky")
			.min(2, "Příliš krátké")
			.max(30, "Příliš dlouhé")
			.test(
				"lastnameCheck",
				"Příjemní musí být vyplněno, když je zadáno křestní jméno",
				(value, context) => !context.parent.firstname || !!value
			),

		email: string()
			.required("Email je vyžadován")
			.matches(/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/, "Neplatný e-mail")
			.test("emailCheck", "E-Mail již někdo používá", async (value) => {
				if (value) {
					try {
						const { status, data } = await axiosApi.get(`/users/?email=${value}&limit=1`);
						return status === 200 && data?.count === 0;
					} catch (e) {
						return false;
					}
				}
				return true;
			}),
		emailAgain: string()
			.required("Email znovu je vyžadován")
			.oneOf([ref("email"), null], "Email znovu se musí shodovat")
			.matches(/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/, "Neplatný e-mail"),
		password: string()
			.required("Heslo je vyžadováno")
			.matches(
				/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
				"Heslo musí být alespoň 8 znaků dlouhé, obsahovat číslici, velké a malé písmeno"
			),
		passwordAgain: string()
			.required("Heslo znovu je vyžadováno")
			.oneOf([ref("password"), null], "Hesla se musí shodovat")
			.matches(
				/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
				"Heslo musí být alespoň 8 znaků dlouhé, obsahovat číslici, velké a malé písmeno"
			),
	});

	return (
		<Modal show={show} onHide={handleClose} centered>
			<Modal.Header closeButton>
				<Modal.Title>Registrace</Modal.Title>
			</Modal.Header>
			<Formik
				validationSchema={schema}
				validateOnMount="true"
				initialValues={{
					username: "",
					firstname: "",
					lastname: "",
					email: "",
					emailAgain: "",
					password: "",
					passwordAgain: "",
				}}
				onSubmit={handleSubmit}
			>
				{({ handleSubmit, isValid }) => (
					<Form noValidate onSubmit={handleSubmit}>
						<Modal.Body>
							<Alert variant="danger" className={!errMsg ? "d-none" : undefined}>
								{errMsg}
							</Alert>
							<Row className="g-3">
								<FormInput type="text" md={12} label="Uživatelské jméno" controlId="username" autoComplete="nickname" />
								<FormInput type="text" md={6} label="Jméno" controlId="firstname" optional={true} autoComplete="given-name" />
								<FormInput type="text" md={6} label="Příjmení" controlId="lastname" optional={true} autoComplete="family-name" />
								<FormInput type="email" md={12} label="E-mail" controlId="email" autoComplete="email" />
								<FormInput type="email" md={12} label="E-mail znovu" controlId="emailAgain" />
								<FormInput type="password" md={6} label="Heslo" controlId="password" autoComplete="new-password" />
								<FormInput type="password" md={6} label="Heslo znovu" controlId="passwordAgain" autoComplete="new-password" />
							</Row>
						</Modal.Body>
						<Modal.Footer>
							<Button disabled={!isValid} variant="primary" type="submit">
								Registrovat
							</Button>
							<Button variant="secondary" onClick={handleClose}>
								Zavřít
							</Button>
						</Modal.Footer>
					</Form>
				)}
			</Formik>
		</Modal>
	);
};

export default RegisterModal;
