import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
	name: "auth",
	initialState: { user: null, token: null, userId: null },
	reducers: {
		setCredentials: (state, action) => {
			const { user, token } = action.payload;
			state.user = user;
			state.token = token;
		},
		logOut: (state) => {
			state.user = null;
			state.token = null;
			localStorage.setItem("persist", false);
		},
		showLogin: (state, action) => {
			state.showLogin = action.payload.show;
		},
		showRegister: (state, action) => {
			state.showRegister = action.payload.show;
		},
	},
});

export const { setCredentials, logOut, showLogin, showRegister } = authSlice.actions;

export default authSlice.reducer;

export const selectCurrentUser = (state) => state.auth.user;
export const selectCurrentToken = (state) => state.auth.token;
export const selectShowLogin = (state) => state.auth.showLogin;
export const selectShowRegister = (state) => state.auth.showRegister;
