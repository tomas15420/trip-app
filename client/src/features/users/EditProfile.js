import { FieldArray, Formik } from "formik";
import React, { useEffect, useRef, useState } from "react";
import { Button, Card, Container, Form, Image, Row } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import FormInput from "../../components/Form/FormInput";
import useAuth from "../../hooks/useAuth";
import { useGetUserByIdQuery, useUpdateUserMutation } from "./userApiSlice";
import { axiosApi } from "../../app/api/axios";
import { object, string, ref, number, mixed } from "yup";
import LoadingSpinner from "../../components/LoadingSpinner";
import { useGetDistrictsQuery } from "../location/locationApiSlice";
import useTitle from "../../hooks/useTitle";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { useLazyGetRolesQuery } from "../roles/roleApiSlice";
import { useDispatch } from "react-redux";
import { showLogin } from "../auth/authSlice";
import ErrorPage from "../../components/ErrorPage";
import Error from "../../components/Error";

const EditProfile = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	let { userId } = useParams();
	const { userId: authId, roles: authRoles } = useAuth();

	if (!userId) userId = authId;
	else userId = parseInt(userId) || undefined;

	if (!userId) {
		navigate("/", { replace: true });
	}
	const { data: user, isLoading, isError, isSuccess, error } = useGetUserByIdQuery({ userId, include: "district,region,roles" });

	useTitle(`Úprava profilu uživatele ${user?.username}`);

	const { data: districts, isLoading: districtsIsLoading } = useGetDistrictsQuery({ limit: 100, order: "name.asc" });
	const [getRoles, { data: roles, isSuccess: rolesIsSuccess, isLoading: rolesIsLoading }] = useLazyGetRolesQuery();

	const [updateUser] = useUpdateUserMutation();

	const formik = useRef();
	const [avatar, setAvatar] = useState(true);
	const [errorMsg, setErrorMsg] = useState("");

	useEffect(() => {
		if (authRoles.includes("HL_ADMIN")) {
			getRoles();
		}
	}, [authRoles, getRoles]);

	useEffect(() => {
		if (!authId || (authId !== userId && !authRoles.includes("ADMIN"))) {
			navigate("/", { replace: true });
			if (!authId) dispatch(showLogin({ show: true }));
		}
	}, [authId, dispatch, navigate, userId, authRoles]);

	useEffect(() => {
		if (formik.current) {
			const userRoles = rolesIsSuccess ? user.roles.map((role) => role.role) : [];
			formik.current.resetForm({
				values: {
					firstname: user?.firstname || "",
					lastname: user?.lastname || "",
					email: "",
					emailAgain: "",
					actualPassword: "",
					password: "",
					passwordAgain: "",
					district: user?.district?.id || "0",
					avatar: "",
					roles: rolesIsSuccess
						? roles.map((role) => ({ name: role.name, role: role.role, checked: userRoles ? userRoles.includes(role.role) : false }))
						: [],
				},
			});
		}
	}, [user, roles, rolesIsSuccess]);

	const schema = object({
		firstname: string()
			.optional()
			.matches(/^[A-ZÁ-Ž][a-zá-ž]{1,29}$/, "Jméno musí začínat velkým písmenem a nesmí obsahovat nepovolené znaky")
			.min(2, "Příliš krátké")
			.max(30, "Příliš dlouhé")
			.when("lastname", {
				is: (val) => val && val.length > 0,
				then: (schema) => schema.required("Křestní jméno musí být vyplněno, pokud je zadáno příjmení"),
				otherwise: (schema) => schema.optional(),
			}),
		lastname: string()
			.matches(/^[A-ZÁ-Ž][a-zá-ž]{1,29}$/, "Příjmení musí začínat velkým písmenem, a nesmí obsahovat nepovolené znaky")
			.min(2, "Příliš krátké")
			.max(30, "Příliš dlouhé"),
		email: string()
			.optional()
			.matches(/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/, "Neplatný e-mail")
			.test("emailCheck", "E-Mail již někdo používá", async (value) => {
				if (value) {
					try {
						const { status, data } = await axiosApi.get(`/users/?email=${value}`);
						return status === 200 && data?.count === 0;
					} catch (e) {
						return false;
					}
				}
				return true;
			}),
		emailAgain: string()
			.optional()
			.oneOf([ref("email"), null], "Email znovu se musí shodovat")
			.matches(/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/, "Neplatný e-mail")
			.when("email", {
				is: (val) => val && val.length > 0,
				then: (schema) => schema.required("Heslo znovu je vyžadováno"),
				otherwise: (schema) => schema.optional(),
			}),
		password: string()
			.optional()
			.matches(
				/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
				"Heslo musí být alespoň 8 znaků dlouhé, obsahovat číslici, velké a malé písmeno"
			),
		passwordAgain: string()
			.optional()
			.matches(
				/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
				"Heslo musí být alespoň 8 znaků dlouhé, obsahovat číslici, velké a malé písmeno"
			)
			.oneOf([ref("password"), null], "Hesla se musí shodovat")
			.when("password", {
				is: (val) => val && val.length > 0,
				then: (schema) => schema.required("Původní heslo je vyžadováno"),
				otherwise: (schema) => schema.optional(),
			}),
		actualPassword: string().when("password", {
			is: (val) => val && val.length > 0,
			then: (schema) => schema.required("Původní heslo je vyžadováno"),
			otherwise: (schema) => schema.optional(),
		}),
		district: number(),
		avatar: mixed()
			.optional()
			.test("fileSize", "Soubor je příliš velký", (value) => !value || value === "delete" || (value && value?.size <= 1024 * 1024))
			.test(
				"fileFormat",
				"Soubor není podporován",
				(value) => !value || value === "delete" || (value && ["image/jpeg", "image/png"].includes(value?.type))
			),
	});

	const handleSubmit = async (values, { resetForm }) => {
		try {
			let { firstname, lastname, email, password, passwordAgain, actualPassword, district, avatar, roles } = values;

			if (district === "0") {
				district = null;
			}

			if (!firstname) firstname = null;
			if (!lastname) lastname = null;

			const formData = new FormData();
			const body = { firstname, lastname, district };
			if (avatar === "delete") body.avatar = "delete";
			if (email) body.email = email;
			if (password) {
				body.password = password;
				body.passwordAgain = passwordAgain;
				body.actualPassword = actualPassword;
			}
			if (roles && rolesIsSuccess) {
				body.roles = roles.filter((role) => role.checked === true).map((role) => role.role);
			}
			formData.append("body", JSON.stringify(body));
			formData.append("avatar", avatar);

			await updateUser({ userId, data: formData }).unwrap();

			resetForm({ values: "", touched: false });
			navigate(`/users/${userId}`);
		} catch (e) {
			setErrorMsg(e);
		}
	};

	const handleAvatarDelete = () => {
		formik.current.setFieldValue("avatar", "delete");
		setAvatar(false);
	};

	if (isLoading) return <LoadingSpinner className="mt-5" />;
	else if (isError) return <ErrorPage error={error} />;
	else if (isSuccess && !rolesIsLoading) {
		const userRoles = rolesIsSuccess ? user.roles.map((role) => role.role) : [];
		return (
			<Container>
				<h1 className="mt-4">Úprava profilu {user?.username}</h1>
				<Formik
					innerRef={formik}
					validationSchema={schema}
					initialValues={{
						firstname: user?.firstname || "",
						lastname: user?.lastname || "",
						email: "",
						emailAgain: "",
						actualPassword: "",
						password: "",
						passwordAgain: "",
						district: user?.district?.id || "0",
						avatar: "",
						roles: rolesIsSuccess
							? roles.map((role) => ({ name: role.name, role: role.role, checked: userRoles ? userRoles.includes(role.role) : false }))
							: [],
					}}
					initialTouched={{ firstname: true, lastname: true, district: true }}
					onSubmit={handleSubmit}
					validateOnMount={true}
				>
					{({ values, errors, touched, handleSubmit, handleChange, handleBlur, setFieldValue, isValid, isSubmitting }) => (
						<Card className="my-2">
							<Card.Body>
								<Error error={errorMsg} onClose={() => setErrorMsg("")} />
								<Form noValidate onSubmit={handleSubmit} encType="multipart/form-data">
									<Row className="mx-sm-5 g-2">
										<FormInput type="text" label="Jméno" controlId="firstname" optional={true} autoComplete="given-name" />
										<FormInput type="text" md={6} label="Příjmení" controlId="lastname" optional={true} autoComplete="family-name" />
										<FormInput type="email" md={12} label="E-mail" controlId="email" autoComplete="email" optional={true} />
										<FormInput type="email" md={12} label="E-mail znovu" controlId="emailAgain" optional={true} />
										{authId === userId && (
											<FormInput
												type="password"
												md={12}
												label="Původní heslo"
												controlId="actualPassword"
												optional={true}
												validateValid={false}
											/>
										)}
										<FormInput type="password" md={6} label="Heslo" controlId="password" autoComplete="new-password" optional={true} />
										<FormInput
											type="password"
											md={6}
											label="Heslo znovu"
											controlId="passwordAgain"
											autoComplete="new-password"
											optional={true}
										/>
										<Form.Group controlId="district">
											<Form.Label>Okres</Form.Label>
											<Form.Select
												value={values.district}
												onChange={handleChange}
												isValid={values.district !== "0" && touched.district && !errors.district}
												isInvalid={touched.district && !!errors.district}
											>
												<option value="0">Vyberte okres</option>
												{!districtsIsLoading &&
													districts?.districts?.map((district) => {
														return (
															<option key={district.id} value={district.id}>
																{district.name}
															</option>
														);
													})}
											</Form.Select>
											<Form.Control.Feedback type="invalid">{errors.district}</Form.Control.Feedback>
										</Form.Group>
										<Form.Group controlId="avatar" className="d-flex">
											<div className="d-flex flex-column">
												<Image
													className="align-self-center"
													width={80}
													height={80}
													src={
														user?.avatarPath && avatar
															? `${user.avatarPath}?width=80&height=80&fit=contain&quality=100`
															: "/images/noavatar.jpg"
													}
												/>
												{avatar && user?.avatarPath && (
													<Button className="mt-1" onClick={handleAvatarDelete} variant="danger" size="sm">
														{" "}
														<FontAwesomeIcon icon={faTrashAlt} /> Odstranit
													</Button>
												)}
											</div>
											<div className="ms-2 flex-fill">
												<Form.Label>Profilový obrázek</Form.Label>
												<Form.Control
													type="file"
													accept="image/png,image/jpeg"
													onChange={(e) => setFieldValue("avatar", e.currentTarget.files[0])}
													onBlur={handleBlur}
													isInvalid={touched.avatar && !!errors.avatar}
												/>
												<Form.Text>Povolené formáty: .jpg, .png (max 1MB)</Form.Text>
												<Form.Control.Feedback type="invalid">{errors.avatar}</Form.Control.Feedback>
											</div>
										</Form.Group>
										{rolesIsSuccess && (
											<section className="border border-1 mt-4 p-3">
												<h4>Uživatelské role</h4>
												<FieldArray
													name="roles"
													render={() => (
														<>
															{values.roles &&
																values.roles.length > 0 &&
																values.roles.map((role, index) => {
																	const checked = values.roles[index].checked;
																	return (
																		<Form.Check
																			key={index}
																			type="checkbox"
																			onChange={() => setFieldValue(`roles[${index}].checked`, !checked)}
																			checked={checked}
																			label={values.roles[index].name}
																		/>
																	);
																})}
														</>
													)}
												/>
											</section>
										)}
									</Row>
									<div className="d-flex justify-content-center mt-2">
										<Button type="submit" disabled={!isValid || isSubmitting}>
											Uložit změny
										</Button>
									</div>
								</Form>
							</Card.Body>
						</Card>
					)}
				</Formik>
			</Container>
		);
	}
};

export default EditProfile;
