import React from "react";
import { Table } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import { useGetUserReactionsQuery } from "./userApiSlice";
import useAuth from "../../hooks/useAuth";

const UserReactedTrips = () => {
	const { userId } = useParams();
	const { userId: currentUser } = useAuth();
	const { data: trips, isLoading, isSuccess, isError, error } = useGetUserReactionsQuery({ userId, withStartPlace: true });
	if (isLoading) return <LoadingSpinner />;
	else if (isError) return <p>{`Vyskytla se chyba při načítání (kód chyby: ${error.status})`}</p>;
	else if (isSuccess && trips)
		return (
			<Table responsive>
				<thead>
					<tr>
						<th>Výlet</th>
						<th>Datum konání</th>
						<th>Místo konání</th>
						<th>Reakce</th>
						<th>Počet potvrzených</th>
					</tr>
				</thead>
				<tbody>
					{trips?.map((trip) => {
						const date = new Date(trip?.date);
						let type = "Neplatné";
						if (trip.reactions.reactionTypeId === "PARTICIPATE") {
							if (date < new Date()) type = "Zúčastnil se";
							else type = "Zúčastní se";
						} else {
							if (date < new Date()) type = "Možná se zúčastnil";
							else type = "Možná se zúčastní";
						}
						return (
							<tr key={trip.reactions.id}>
								<td>
									<Link to={`/trips/${trip?.id}`}>{trip?.name}</Link>
								</td>
								<td>{date.toLocaleString().slice(0, -3)}</td>
								<td>
									<Link
										to={`/trips/?regionId=${trip?.startPlace?.district?.regionId}&districtId=${trip?.startPlace?.district?.id}&municipalityId=${trip?.startPlace?.id}`}
									>
										{trip?.startPlace?.name}
									</Link>
								</td>
								<td>{type}</td>
								<td>{trip?.participates || 0}</td>
							</tr>
						);
					})}
				</tbody>
			</Table>
		);
	else if (isSuccess && !trips) {
		if (currentUser !== parseInt(userId)) return <p>Uživatel zatím nereagoval na žádné výlety</p>;
		else return <p>Zatím jste nereagoval na žádné výlety</p>;
	}
};

export default UserReactedTrips;
