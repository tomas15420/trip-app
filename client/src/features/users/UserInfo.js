import { formatDistance } from "date-fns";
import cs from "date-fns/locale/cs";
import React, { useEffect, useState } from "react";
import { Button, Col, Image, OverlayTrigger, Row, Table, Tooltip } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
	useFollowUserMutation,
	useUnfollowUserMutation,
	useGetUserFollowersQuery,
	useGetUserFollowingQuery,
} from "../followers/followersApiSlice";
import FollowersModal from "../followers/FollowersModal";
import useAuth from "../../hooks/useAuth";
import Error from "../../components/Error";

const UserInfo = ({ user }) => {
	const registered = new Date(user?.registered);
	const districtId = user?.district?.id;
	const regionId = user?.district?.regionId;
	const { userId } = useAuth();

	const [followersShow, setFollowersShow] = useState(false);
	const [followingShow, setFollowingShow] = useState(false);
	const [followersPage, setFollowersPage] = useState(1);
	const [followingPage, setFollowingPage] = useState(1);
	const [error, setError] = useState("");

	const {
		data: followers,
		isLoading: followersIsLoading,
		isError: followersIsError,
	} = useGetUserFollowersQuery({
		userId: user?.id,
		limit: 10,
		offset: (followersPage - 1) * 10,
	});

	const {
		data: following,
		isLoading: followingIsLoading,
		isError: followingIsError,
	} = useGetUserFollowingQuery({
		userId: user?.id,
		limit: 10,
		offset: (followingPage - 1) * 10,
	});

	const [followUser] = useFollowUserMutation();
	const [unfollowUser] = useUnfollowUserMutation();

	const handleFollowersClose = () => {
		setFollowersShow(false);
	};
	const handleFollowingClose = () => {
		setFollowingShow(false);
	};

	const handleFollowersShow = () => {
		setFollowersShow(true);
	};
	const handleFollowingShow = () => {
		setFollowingShow(true);
	};

	const handleChangeFollowersPage = (page) => {
		setFollowersPage(page);
	};
	const handleChangeFollowingPage = (page) => {
		setFollowingPage(page);
	};

	const handleFollow = async () => {
		try {
			if (userId && userId !== user?.id && user?.following === "TRUE") {
				await unfollowUser(user?.id).unwrap();
			} else if (userId && userId !== user?.id && user?.following === "FALSE") {
				await followUser(user?.id).unwrap();
			}
		} catch (e) {
			setError(e);
		}
	};

	useEffect(() => {
		setFollowersShow(false);
		setFollowingShow(false);
		setFollowersPage(1);
		setFollowingPage(1);
	}, [user]);

	return (
		<>
			<Error error={error} onClose={() => setError("")} />
			<Row>
				<Col md="3" lg="2">
					<div className="d-flex flex-column justify-content-md-center">
						<Image
							className="align-self-center"
							width={130}
							height={130}
							src={user?.avatarPath ? `${user.avatarPath}?width=130&height=150&fit=contain` : "/images/noavatar.jpg"}
							rounded
						/>
						{!followersIsLoading && !followersIsError && (
							<>
								<Button size="sm" variant="warning" className="my-1" onClick={handleFollowersShow} disabled={!followers?.total}>
									{`${followers?.total ? followers?.total : 0}  ${
										followers?.total > 0 && followers?.total < 5 ? "sledující" : "sledujících"
									}`}
								</Button>
								{followersShow && (
									<FollowersModal
										title="Sledující"
										items={{ total: followers?.total, users: followers?.followers }}
										show={followersShow}
										page={followersPage}
										handleClose={handleFollowersClose}
										handleChangePage={handleChangeFollowersPage}
									/>
								)}
							</>
						)}
						{!followingIsLoading && !followingIsError && (
							<>
								<Button size="sm" variant="info" className="mb-1" onClick={handleFollowingShow} disabled={!following?.total}>
									{`${following?.total ? following?.total : 0}  ${
										following?.total > 0 && following?.total < 5 ? "sledovaní" : "sledovaných"
									}`}
								</Button>
								{followingShow && (
									<FollowersModal
										title="Sledování"
										items={{ total: following?.total, users: following?.followings }}
										show={followingShow}
										page={followingPage}
										handleClose={handleFollowingClose}
										handleChangePage={handleChangeFollowingPage}
									/>
								)}
							</>
						)}
					</div>
				</Col>
				<Col>
					<div className="d-flex">
						<h4 className="mb-0">{user?.username}</h4>
						{userId && userId !== user?.id && (
							<Button
								size="sm"
								className="ms-2 align-self-center"
								variant={user?.following === "TRUE" ? "success" : "outline-success"}
								onClick={handleFollow}
							>
								{user?.following === "FALSE" ? "Sledovat uživatele" : "Sleduji uživatele"}
							</Button>
						)}
					</div>
					<hr />
					<Table>
						<tbody>
							{user?.email && (
								<tr>
									<th>E-Mail</th>
									<td>{user?.email}</td>
								</tr>
							)}
							<tr>
								<th>Jméno</th>
								<td>{user?.firstname ? user.firstname : "Neuvedeno"}</td>
							</tr>
							<tr>
								<th>Příjmení</th>
								<td>{user?.lastname ? user.lastname : "Neuvedeno"}</td>
							</tr>
							<tr>
								<th>Okres</th>
								<td>
									{user?.district?.name ? (
										<Link to={`/trips?regionId=${regionId}&districtId=${districtId}`}>
											{user?.district?.name}, {user?.district?.region?.name}
										</Link>
									) : (
										"Neuveden"
									)}
								</td>
							</tr>
							<tr>
								<th>Registrován</th>
								<td>
									<OverlayTrigger
										placement="right"
										overlay={<Tooltip style={{ position: "fixed" }}>{registered.toLocaleString().slice(0, -3)}</Tooltip>}
									>
										{({ ref, ...triggerHandler }) => (
											<span {...triggerHandler} ref={ref}>
												{formatDistance(registered, new Date(), {
													addSuffix: true,
													locale: cs,
												})}
											</span>
										)}
									</OverlayTrigger>
								</td>
							</tr>
						</tbody>
					</Table>
				</Col>
			</Row>
		</>
	);
};

export default UserInfo;
