import React from "react";
import { Image } from "react-bootstrap";
import { Link } from "react-router-dom";

const UserPreview = ({ user }) => {
	return (
		<article className="d-flex mb-3">
			<div className="flex-shrink-0">
				<Link to={`/users/${user.id}`}>
					<Image
						rounded
						className="me-3"
						width="50"
						height="50"
						src={user.avatarPath ? `${user.avatarPath}?width=50&height=50&fit=contain` : "/images/noavatar.jpg"}
					/>
				</Link>
			</div>
			<div className="flex-grow">
				<div>
					<Link to={`/users/${user.id}`} className="text-decoration-none fw-bold">
						{user.username}
					</Link>
				</div>
				{user.firstname && (
					<div className="text-muted">
						<span>{user.firstname}</span>
                        {user.lastname && <span className="ms-1">{user.lastname}</span>}
					</div>
				)}
			</div>
		</article>
	);
};

export default UserPreview;
