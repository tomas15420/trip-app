import React from "react";
import { Table } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import { useGetTripsQuery } from "../trips/tripApiSlice";
import useAuth from "../../hooks/useAuth";

const UserCreatedTrips = () => {
	const { userId } = useParams();
	const { userId: currentUser } = useAuth();

	const {
		data: trips,
		isLoading,
		isSuccess,
		isError,
		error,
	} = useGetTripsQuery(
		{
			userId,
			order: "createdAt.desc",
		},
		{ refetchOnFocus: true, refetchOnMountOrArgChange: true }
	);

	if (isLoading) return <LoadingSpinner />;
	else if (isError) return <p>{`Vyskytla se chyba při načítání (kód chyby: ${error.status})`}</p>;
	else if (isSuccess && trips)
		return (
			<>
				<Table responsive>
					<thead>
						<tr>
							<th>Vytvořeno</th>
							<th>Název</th>
							<th>Datum konání</th>
							<th>Místo konání</th>
							<th>Počet potvrzených</th>
						</tr>
					</thead>
					<tbody>
						{trips?.trips?.map((trip) => {
							const created = new Date(trip?.createdAt);
							const date = new Date(trip?.date);
							return (
								<tr key={trip.id}>
									<td>{created.toLocaleString().slice(0, -3)}</td>
									<td>
										<Link to={`/trips/${trip?.id}`}>{trip.name}</Link>
									</td>
									<td>{date.toLocaleString().slice(0, -3)}</td>
									<td>
										<Link
											to={`/trips/?regionId=${trip?.startPlace?.district?.regionId}&districtId=${trip?.startPlace?.district?.id}&municipalityId=${trip?.startPlaceId}`}
										>
											{trip.startPlace.name}
										</Link>
									</td>
									<td>{trip.participates}</td>
								</tr>
							);
						})}
					</tbody>
				</Table>
			</>
		);
	else if (isSuccess && !trips) {
		if (currentUser !== parseInt(userId)) return <p>Uživatel zatím nevytvořil žádné výlety</p>;
		else return <p>Nevytvořil jste zatím žádné výlety</p>;
	}
};

export default UserCreatedTrips;
