import React, { useEffect } from "react";
import { Button, Card, Col, Container, Nav, Row } from "react-bootstrap";
import { Link, NavLink, Outlet, useNavigate, useParams } from "react-router-dom";
import LoadingSpinner from "../../components/LoadingSpinner";
import useTitle from "../../hooks/useTitle";
import { useGetUserByIdQuery } from "./userApiSlice";
import UserInfo from "./UserInfo";
import ErrorPage from "../../components/ErrorPage";
import useAuth from "../../hooks/useAuth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-regular-svg-icons";

const Profile = () => {
	const { userId } = useParams();
	const { userId: authUserId, roles } = useAuth();
	const navigate = useNavigate();
	const {
		data: user,
		isLoading: userIsLoading,
		isError: userIsError,
		isSuccess: userIsSuccess,
		error: userError,
	} = useGetUserByIdQuery({ userId, include: "district,region" });

	useTitle(`Profil uživatele ${user?.username}`);

	useEffect(() => {
		if (userError?.status === 404) {
			navigate("/404", { replace: true });
		}
	}, [userError, navigate]);

	if (userIsLoading) return <LoadingSpinner className="mt-5" />;
	else if (userIsError) return <ErrorPage error={userError} />;
	else if (userIsSuccess) {
		return (
			<Container as="main">
				<Row className="g-3 mt-3">
					<Col xs="12">
						<Card>
							<Card.Body>
								{authUserId && (authUserId === user.id || roles.includes("ADMIN")) && (
									<div className="float-md-end d-flex justify-content-end">
										<Link to={`edit`}>
											<Button role="link">
												<FontAwesomeIcon icon={faEdit} />
											</Button>
										</Link>
									</div>
								)}
								<UserInfo user={user} />
							</Card.Body>
						</Card>
					</Col>
					<Col>
						<Card className="mb-4">
							<Card.Header>
								<Nav variant="pills">
									<Nav.Item>
										<Nav.Link as={NavLink} href={`/users/${userId}`} to={`/users/${userId}`} end>
											Vytvořené výlety
										</Nav.Link>
									</Nav.Item>
									<Nav.Item>
										<Nav.Link as={NavLink} href={`/users/${userId}/reactions`} to={`/users/${userId}/reactions`}>
											Reakce na výlety
										</Nav.Link>
									</Nav.Item>
								</Nav>
							</Card.Header>
							<Card.Body className="p-4">
								<Outlet />
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		);
	}
};

export default Profile;
