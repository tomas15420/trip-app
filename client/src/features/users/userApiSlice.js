import { apiSlice } from "../../app/api/apiSlice";

export const userApiSlice = apiSlice.injectEndpoints({
	endpoints: (builder) => ({
		getUsers: builder.query({
			query: (filters) => {
				const params = Object.entries(filters)
					.filter(([key, value]) => value !== "")
					.map(([key, value]) => `${key}=${value}`)
					.join("&");
				return {
					url: `/users/${params ? `?${params}` : ""}`,
				};
			},
			providesTags: (result, error, arg) =>
				result
					? [
							...result?.users.map(({ id }) => ({
								type: "User",
								id,
							})),
							"User",
					  ]
					: ["User"],
		}),
		getUserById: builder.query({
			query: ({ userId, ...filters }) => {
				const params = Object.entries(filters)
					.filter(([key, value]) => value !== "")
					.map(([key, value]) => `${key}=${value}`)
					.join("&");
				return {
					url: `/users/${userId}${params ? `?${params}` : ""}`,
					validateStatus: (response, { result: isError, count }) => {
						return response.status === 200 && !isError && count > 0;
					},
				};
			},
			providesTags: (result, error, arg) => (result ? [{ type: "User", id: result?.id }, "User"] : ["User"]),
			transformResponse: (response) => response?.users[0],
			transformErrorResponse: (response) => {
				if (response.status === 200 && response.data?.count === 0) return { status: 404, data: response.data };
				return response;
			},
		}),
		getUserReactions: builder.query({
			query: ({ userId, ...filters }) => {
				const params = Object.entries(filters)
					.filter(([key, value]) => value !== "")
					.map(([key, value]) => `${key}=${value}`)
					.join("&");
				return {
					url: `/users/${userId}/reactions${params ? `?${params}` : ""}`,
				};
			},
			providesTags: (result, error, arg) =>
				result
					? [
							...result?.map((trip) => ({
								type: "Reaction",
								id: trip.reactions.id,
							})),
							"Reaction",
					  ]
					: ["Reaction"],
		}),
		updateUser: builder.mutation({
			query: ({ userId, data }) => ({
				method: "PATCH",
				url: `/users/${userId}`,
				body: data,
			}),
			invalidatesTags: (result, error, { userId }) => [{ type: "User", id: userId }],
		}),
	}),
});

export const { useGetUsersQuery, useLazyGetUsersQuery, useGetUserByIdQuery, useGetUserReactionsQuery, useUpdateUserMutation } =
	userApiSlice;
