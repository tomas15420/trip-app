import { useSelector } from "react-redux";
import { selectCurrentUser } from "../features/auth/authSlice";

const useAuth = () => {
	const user = useSelector(selectCurrentUser);
	if (user) {
		const { userId, username, roles } = user;
		return { userId, username, roles };
	}

	return { userid: undefined, username: undefined, roles: undefined };
};
export default useAuth;
