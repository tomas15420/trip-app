import React from "react";
import { Button, Col, Row } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import { useDispatch } from "react-redux";
import { showRegister } from "../features/auth/authSlice";
import useAuth from "../hooks/useAuth";
import { useNavigate } from "react-router-dom";
import { useGetTripsQuery } from "../features/trips/tripApiSlice";
import TripItem from "../features/trips/TripItem";
import { Link } from "react-router-dom";
import useTitle from "../hooks/useTitle";

const Home = () => {
	const dispatch = useDispatch();
	const { userId } = useAuth();
	const navigate = useNavigate();

	useTitle("Domov");

	const handleButton = () => {
		if (!userId) dispatch(showRegister({ show: true }));
		else navigate("/trips/new");
	};

	const { data: trips, isLoading, isError, isSuccess } = useGetTripsQuery({ order: "id.desc", limit: 10 });

	return (
		<>
			<div className="bg-dark py-5">
				<Container className="px-5">
					<Row className="gx-5 align-items-center justify-content-center">
						<Col lg={8} xl={7} xxl={6}>
							<div className="my-5 text-center text-xl-start">
								<h1 className="display-5 fw-bolder text-white mb-2">
									Vítejte v aplikaci na plánování turistických a cyklistických výletů
								</h1>
								<p className="lead fw-normal text-white-50 mb-4">
									Nebaví vás turistika osamotě? Procházejte naplánované výlety ve Vašem okolí uživateli z celého Česka!
								</p>
								<div className="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start">
									<Button size="lg" variant="primary" className="px-4 me-sm-3" as={Link} to="/trips">
										Procházet výlety
									</Button>
									<Button size="lg" variant="outline-light" className="px-4 me-sm-3" onClick={handleButton}>
										{userId ? "Naplánovat výlet" : "Registrovat se"}
									</Button>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
			{isError && (
				<Container className="mb-4">
					<h3 className="my-3">Vyskytla se chyba, zkuste načíst stránku znovu</h3>
				</Container>
			)}
			{!isLoading && isSuccess && (
				<Container className="mb-4">
					<h1 className="my-3 d-flex justify-content-center">Nově naplánované výlety</h1>
					<Row className="g-4">
						{trips.count > 0 && trips.trips.map((trip) => (
							<Col lg={6} key={trip.id}>
								<TripItem key={trip.id} className="m-0 h-100" trip={trip} />
							</Col>
						))}
						{trips.count === 0 && <p>Ještě nebyly přidány žádné výlety</p>}
					</Row>
				</Container>
			)}
		</>
	);
};

export default Home;
