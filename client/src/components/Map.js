import L from "leaflet";
import React, { useEffect, useRef } from "react";
import { MapContainer, Polyline, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { useCallback } from "react";

const Map = ({ coords, width, height, ...props }) => {
	const mapRef = useRef(null);

	const fitBounds = useCallback(
		(target) => {
			if (target && coords?.length > 0) {
				const bounds = L.latLngBounds(coords);
				target.fitBounds(bounds);
			}
		},
		[coords]
	);

	useEffect(() => {
		fitBounds(mapRef.current);
	}, [coords, fitBounds]);

	return (
		<MapContainer
			style={{ width: width || "100%", height: height || "400px" }}
			{...props}
			ref={mapRef}
			whenReady={(e) => {
				fitBounds(e.target);
			}}
		>
			<TileLayer
				attribution=''
				url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
			/>
			{coords?.length > 0 && <Polyline pathOptions={{ fillColor: "red", color: "red" }} positions={coords} />}
		</MapContainer>
	);
};

export default Map;
