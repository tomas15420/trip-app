import React, { useEffect, useState } from "react";
import { Alert } from "react-bootstrap";

const Error = ({ error, onClose, ...props }) => {
	const [show, setShow] = useState(false);

	useEffect(() => {
		setShow(true);
	}, [error]);

	const handleClose = () => {
		setShow(false);
		onClose();
	};

	const errors = error?.data?.message ? [error.data.message] : error?.data?.errors?.map((err) => <div>{err.msg}</div>);

	return (
		!!error && (
			<Alert show={show} onClose={handleClose} variant="danger" dismissible {...props}>
				{/* {JSON.stringify(error)}
				<hr /> */}
				{error.status !== 500
					? errors?.map((err, idx) => <span key={idx}>{err}</span>)
					: "Chyba při komunikaci se serverem, zkuste to znovu"}
			</Alert>
		)
	);
};

export default Error;
