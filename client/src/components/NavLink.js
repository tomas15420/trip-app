import React from "react";
import Nav from "react-bootstrap/Nav";
import { NavLink as RouterLink } from "react-router-dom";

const NavLink = ({ to, end, as, onSelect, onClick, children }) => {
	let Component;
	if (!as) Component = Nav.Link;
	else Component = as;
	return (
		<Component
			onClick={(e) => {
				if (typeof onSelect !== "undefined") onSelect(e);
				if (typeof onClick !== "undefined") onClick(e);
			}}
			className={({active}) => active ? "active" : undefined}
			as={to ? RouterLink : undefined}
			to={to}
			end={end}
		>
			{children}
		</Component>
	);
};

export default NavLink;
