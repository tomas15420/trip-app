import React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHouse } from "@fortawesome/free-solid-svg-icons";

const NotFound = () => {
	return (
		<Container as={"main"}>
			<div className="d-flex justify-content-center">
				<div className="d-flex flex-column my-4">
					<h1 className="mb-4 text-center">Uups</h1>
					<h2 className="text-center mb-2">404 Stránka nenalezena</h2>
					<p className="text-center mb-4">Omlouváme se, vyskytal se chyba, požadována stránka nebyla nalezena</p>
					<div className="d-flex justify-content-center">
						<Button as={Link} to="/">
							<FontAwesomeIcon icon={faHouse} /> Zpět domů
						</Button>
					</div>
				</div>
			</div>
		</Container>
	);
};

export default NotFound;
