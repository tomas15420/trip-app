import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { useLogoutMutation } from "../features/auth/authApiSlice";
import { useDispatch } from "react-redux";
import { showLogin, showRegister } from "../features/auth/authSlice";
import useAuth from "../hooks/useAuth";
import NavLink from "./NavLink";
import Search from "../features/search/Search";

const Header = () => {
	const { username, userId } = useAuth();
	const { pathname } = useLocation();
	const [expanded, setExpanded] = useState(false);

	const dispatch = useDispatch();

	const handleShowRegister = (e) => {
		e.target.blur();
		dispatch(showRegister({ show: true }));
	};
	const handleShowLogin = (e) => {
		e.target.blur();
		dispatch(showLogin({ show: true }));
	};
	const [logout] = useLogoutMutation();

	const handleLogout = async () => {
		try {
			await logout().unwrap();
		} catch (e) {
			alert("Odhlášení selhalo, zkuste to znovu");
		}
	};

	const handleSelect = () => {
		setExpanded(false);
	};

	return (
		<header>
			<Navbar expanded={expanded} expand="lg" bg="dark" variant="dark" onToggle={() => setExpanded(!expanded)}>
				<Container>
					<NavLink to="/" as={Navbar.Brand} onSelect={handleSelect}>
						Trip-App
					</NavLink>
					<Navbar.Toggle aria-controls="main-nav" />
					<Navbar.Collapse id="main-nav">
						<div className="d-flex w-100 flex-column align-items-stretch flex-lg-row">
							<Nav>
								<NavLink to="/" onSelect={handleSelect}>
									Domů
								</NavLink>
								<NavLink to="/leaderboard" onSelect={handleSelect}>
									Žebříčky
								</NavLink>
								<NavLink to="/trips" end onSelect={handleSelect}>
									Výlety
								</NavLink>
							</Nav>
							<Search onSelect={handleSelect} className="flex-grow-1 mx-lg-5 order-lg-0 order-last" />
							<Nav>
								{username && (
									<NavDropdown title={username} id="main-nav-dropdown">
										<NavLink as={NavDropdown.Item} onSelect={handleSelect} to={`/users/${userId}`}>
											Můj profil
										</NavLink>
										<NavLink as={NavDropdown.Item} onSelect={handleSelect} end to="/trips/new" state={{ from: pathname }}>
											Naplánovat výlet
										</NavLink>
										<NavLink as={NavDropdown.Item} onSelect={handleSelect} to="/edit-profile">
											Upravit profil
										</NavLink>
										<NavDropdown.Divider />
										<NavLink as={NavDropdown.Item} onSelect={handleSelect} onClick={handleLogout}>
											Odhlásit se
										</NavLink>
									</NavDropdown>
								)}
								{!username && (
									<>
										<NavLink onSelect={handleSelect} onClick={handleShowLogin}>
											Přihlásit se
										</NavLink>
										<NavLink onSelect={handleSelect} onClick={handleShowRegister}>
											Registrace
										</NavLink>
									</>
								)}
							</Nav>
						</div>
					</Navbar.Collapse>
				</Container>
			</Navbar>
		</header>
	);
};

export default Header;
