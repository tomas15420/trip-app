import React from "react";
import { Pagination } from "react-bootstrap";

const CustomPagination = ({ size, active, count, perPage, handleChangePage, ...props }) => {
	const pages = Math.ceil(count / perPage);
	if (!pages) return undefined;

	const startPage = active + 2 > pages ? (pages - 4 > 1 ? pages - 4 : 1) : active > 3 ? active - 2 : 1;
	const endPage = startPage + 4 > pages ? pages : startPage + 4;

	let pageElements = [];
	for (let i = startPage; i <= endPage; i++) {
		pageElements.push(
			<Pagination.Item
				key={i}
				active={i === active}
				onClick={() => {
					if (i !== active) handleChangePage(i);
				}}
			>
				{i}
			</Pagination.Item>
		);
	}
	return (
		<Pagination {...props} size={size ? size : "sm"}>
			<Pagination.First disabled={active === 1} onClick={() => handleChangePage(1)} />
			{pageElements}
			<Pagination.Last disabled={active === endPage} onClick={() => handleChangePage(pages)} />
		</Pagination>
	);
};

export default CustomPagination;
