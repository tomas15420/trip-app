import React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHouse } from "@fortawesome/free-solid-svg-icons";

const ErrorPage = ({ error, as }) => {
	const Component = as || Container;
	const errorMessages = new Map();
	errorMessages.set(400, "Bad request");
	errorMessages.set(204, "Žádný obsah");
	errorMessages.set(500, "Server error");

	return (
		<Component>
			<div className="d-flex justify-content-center">
				<div className="d-flex flex-column my-4">
					<h1 className="mb-4 text-center">Uups, něco se nepovedlo</h1>
					<h2 className="text-center mb-2">
						{errorMessages.has(error.status)
							? `${error.status} - ${errorMessages.get(error.status)}`
							: `${error.status} - ${error?.error ? error.error : error?.data?.message}`}
					</h2>
					<p className="text-center mb-4">Omlouváme se, vyskytal se chyba, zkuste to prosím později</p>
					<div className="d-flex justify-content-center">
						<Button as={Link} to="/">
							<FontAwesomeIcon icon={faHouse} /> Zpět domů
						</Button>
					</div>
				</div>
			</div>
		</Component>
	);
};

export default ErrorPage;
