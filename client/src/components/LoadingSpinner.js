import React from "react";
import { Spinner } from "react-bootstrap";

const LoadingSpinner = (props) => {
    return (
        <div
            className="d-flex justify-content-center align-items-center"
        >
            <Spinner animation="border" role="status" {...props}>
                <span className="visually-hidden">Načítání...</span>
            </Spinner>
        </div>
    );
};

export default LoadingSpinner;
