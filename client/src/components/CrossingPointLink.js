import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { Link } from "react-router-dom";

const CrossingPointLink = ({ cp, ...props }) => {
	const desc = cp.info?.description;
	const style = !desc ? { textDecoration: "none" } : { textDecoration: "underline dotted", textUnderlineOffset: "3px" };

	const TooltipLink = ({ children, title, ...props }) => (
		<OverlayTrigger placement="top" overlay={<Tooltip style={{ position: "fixed" }}>{title}</Tooltip>}>
			<Link {...props}>{children}</Link>
		</OverlayTrigger>
	);
	const Component = desc ? TooltipLink : Link;

	return (
		<Component
			to={`/trips/?regionId=${cp.district.regionId}&districtId=${cp.district.id}&municipalityId=${cp.id}`}
			style={style}
			title={desc}
			{...props}
			className="text-nowrap"
		>
			{cp.name}
		</Component>
	);
};

export default CrossingPointLink;
