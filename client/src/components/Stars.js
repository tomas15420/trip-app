import React, { useState, useEffect } from "react";
import useAuth from "../hooks/useAuth";
import { faStar, faStarHalfAlt } from "@fortawesome/free-solid-svg-icons";
import { faStar as faStarRegular } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Stars = ({ stars, maxStars, handleClick, size, ...props }) => {
    if (!maxStars) maxStars = 5;
    const interactive = typeof handleClick !== "undefined";
    const { userId } = useAuth();
    const [selectedRating, setSelectedRating] = useState(stars);

    useEffect(() => {
        setSelectedRating(stars);
    }, [stars]);

    const handleSelectedRating = (val) => {
        if (userId) setSelectedRating(val + 1);
    };

    const handleMouseLeave = () => {
        if (userId) setSelectedRating(stars);
    };

    const fullStars = Math.floor(selectedRating);
    const halfStars = selectedRating - fullStars >= 0.5 ? 1 : 0;
    return (
        <span {...props} onMouseLeave={() => interactive && handleMouseLeave()}>
            {[...Array(maxStars)].map((_, i) => {
                return (
                    <FontAwesomeIcon
                        key={i}
                        size={size}
                        icon={
                            halfStars && i === fullStars + halfStars - 1
                                ? faStarHalfAlt
                                : i < fullStars + halfStars
                                ? faStar
                                : faStarRegular
                        }
                        className={
                            i < fullStars || i < fullStars + halfStars
                                ? "text-warning"
                                : "text-secondary"
                        }
                        style={interactive && userId && { cursor: "pointer" }}
                        onMouseEnter={() =>
                            interactive && handleSelectedRating(i)
                        }
                        onClick={() => interactive && handleClick(i)}
                    />
                );
            })}
        </span>
    );
};

export default Stars;
