import { Field } from "formik";
import React from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";

const FormInput = ({ md, controlId, label, optional, validateValid, textarea, isValid, isInvalid, disabled, ...props }) => {
	if (validateValid === undefined) validateValid = true;
	return (
		<Form.Group as={Col} md={md} controlId={controlId}>
			{label && <Form.Label>{label}</Form.Label>}
			<Field name={controlId}>
				{({ field, meta }) => (
					<>
						<Form.Control
							as={textarea ? "textarea" : "input"}
							disabled={disabled}
							{...field}
							{...props}
							isValid={
								isValid !== undefined
									? isValid
									: meta.touched &&
									  !meta.error &&
									  (!optional || (optional === true && field.value !== "")) &&
									  validateValid === true &&
									  disabled !== true
							}
							isInvalid={isInvalid !== undefined ? isInvalid : meta.touched && meta.error}
						/>
						<Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
					</>
				)}
			</Field>
		</Form.Group>
	);
};

export default FormInput;
