import React from "react";
import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";

const Foooter = () => {
	return (
		<footer className="footer bg-dark">
			<Container>
				<div className="d-flex justify-content-between align-items-center">
					<ul id="footer-links-list" className="me-4">
						<li>
							<Link to="/" className="link-secondary">
								Domů
							</Link>
						</li>
						<li>
							<Link to="/leaderboard" className="link-secondary">
								Žebříčky
							</Link>
						</li>
						<li>
							<Link to="/trips" className="link-secondary">
								Výlety
							</Link>
						</li>
					</ul>
					<p className="text-white text-end mb-0">Projekt byl vytvořen jako bakalářská práce | Tomáš Mach 2023</p>
				</div>
			</Container>
		</footer>
	);
};

export default Foooter;
