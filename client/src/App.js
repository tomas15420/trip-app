import { Route, Routes, Navigate } from "react-router-dom";
import RegisterModal from "./features/auth/RegisterModal";
import LoginModal from "./features/auth/LoginModal";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import Trips from "./features/trips/Trips";
import Trip from "./features/trips/Trip";
import Comments from "./features/comments/Comments";
import Photos from "./features/photos/Photos";
import CreateTrip from "./features/trips/CreateTrip";
import EditTrip from "./features/trips/EditTrip";
import Profile from "./features/users/Profile";
import UserCreatedTrips from "./features/users/UserCreatedTrips";
import UserReactedTrips from "./features/users/UserReactedTrips";
import EditProfile from "./features/users/EditProfile";
import PersistLogin from "./features/auth/PersistLogin";
import Laderboard from "./features/leaderboard/Laderboard";
import BestTrips from "./features/leaderboard/BestTrips";
import BestParticipants from "./features/leaderboard/BestParticipants";
import BestCreated from "./features/leaderboard/BestCreated";
import SearchPage from "./features/search/SearchPage";

const App = () => (
	<>
		<Header />
		<RegisterModal />
		<LoginModal />

		<Routes>
			<Route element={<PersistLogin />}>
				<Route index element={<Home />} />
				<Route path="/leaderboard" element={<Laderboard />}>
					<Route index element={<BestTrips />} />
					<Route path="best-participants" element={<BestParticipants />} />
					<Route path="best-created" element={<BestCreated />} />
				</Route>
				<Route path="/trips" element={<Trips />} />
				<Route path="/trips/new" element={<CreateTrip />} />
				<Route path="/trips/:tripId/edit" element={<EditTrip />} />
				<Route path="/trips/:tripId" element={<Trip />}>
					<Route index element={<Comments />} />
					<Route path="photos" element={<Photos />} />
				</Route>
				<Route path="/users/:userId/edit" element={<EditProfile />} />
				<Route path="/edit-profile" element={<EditProfile />} />
				<Route path="/users/:userId" element={<Profile />}>
					<Route index element={<UserCreatedTrips />} />
					<Route path="reactions" element={<UserReactedTrips />} />
				</Route>
				<Route path="/search" element={<SearchPage />} />
				<Route path="/404" element={<NotFound />} />
				<Route path="*" element={<Navigate replace to="/404" />} />
			</Route>
		</Routes>
		<Footer />
	</>
);

export default App;
