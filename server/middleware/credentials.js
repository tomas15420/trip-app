const credentials = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3500");
    res.header("Access-Control-Allow-Credentials", true);
    next();
};

module.exports = credentials;

