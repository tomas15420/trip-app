const db = require("../models/");
const User = db.user;
const Role = db.role;

exports.authUser = (withRoles, requireAuth = true) => {
    return async (req, res, next) => {
        const user = req.user;
        try {
            const authenticatedUser = user?.userId
                ? await User.findByPk(user.userId, {
                    attributes: ["id","username","avatar","avatarPath","email"]
                })
                : undefined;
            if (requireAuth && !authenticatedUser)
                return res.status(401).json({
                    success: false,
                    message: "Uživatel není autorizován",
                });
            const roles = withRoles && authenticatedUser
                ? (await authenticatedUser.getRoles()).map((role) => `${role.role}`)
                : [];
            req.auth = { user: authenticatedUser, roles };
            next();
        } catch (e) {
            res.status(500).json({ success: false, message: e.message });
        }
    };
};
