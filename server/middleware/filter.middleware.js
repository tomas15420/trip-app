exports.filter = (allowedFilters) => {
	return (req, res, next) => {
		let { limit, offset, order } = req.query;
		if (!(limit = parseInt(limit)) || limit < 1) limit = 30;
		if (limit > 200) return res.status(400).json({ success: false, message: "Maximální limit výpisu je 200 položek" });
		if (!(offset = parseInt(offset)) || offset < 0) offset = 0;

		let options = { limit, offset, where: {} };
		if (order) {
			order = order.split(".");
			if (order.length !== 2) return res.status(400).json({ success: false, message: "Řazení zadáno v chybném formátu" });
			order[1] = order[1].toLowerCase();
			if (order[1] !== "desc" && order[1] !== "asc")
				return res.status(400).json({ success: false, message: "Řazení zadáno v chybném formátu" });

			options.order = [order];
		}
		delete req.query.limit;
		delete req.query.offset;
		delete req.query.order;
		const filter = { ...req.query, ...req.params };
		if (typeof allowedFilters === "string") allowedFilters = [allowedFilters];
		Object.keys(filter).forEach((key) => {
			if (allowedFilters?.length && filter[key] && allowedFilters.includes(key)) options.where[key] = filter[key];
		});
		req.filterOptions = options;
		next();
	};
};
