const { verify, TokenExpiredError } = require("jsonwebtoken");

const getToken = (req) => {
	const authHeader = req.headers.authorization || req.headers.Authorization;
	if (authHeader?.startsWith("Bearer ")) {
		return (token = authHeader.split(" ")[1]);
	}
	return null;
};

const validateToken = (req, res, next) => {
	if (req.originalUrl === "/api/v1/auth/refreshToken") return next();
	const token = getToken(req);
	if (token) {
		try {
			const user = verify(token, process.env.ACCESS_TOKEN_SECRET);
			req.user = user;
		} catch (error) {
			if (error instanceof TokenExpiredError) return res.status(401).json({ success: false, message: "Token vypršel" });
			return res.status(401).json({ success: false, message: "Neplatný token" });
		}
	}
	return next();
};

module.exports = { validateToken };
