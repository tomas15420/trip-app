module.exports = (req, res, next) => {
	if (req.body?.body) {
		req.body = JSON.parse(req.body.body);
	}
	return next();
};
