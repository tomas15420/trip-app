const { body } = require("express-validator");
const { REGEX_URL } = require("../config/regex.config");

exports.createTripValidation = [
	body("name")
		.notEmpty()
		.withMessage("Název je vyžadován")
		.isString()
		.isLength({ max: 64 })
		.withMessage("Název může mít maximálně 64 znaků")
		.isLength({ min: 15 })
		.withMessage("Název musí mít minimálně 15 znaků"),
	body("description")
		.notEmpty()
		.withMessage("Popis je vyžadován")
		.isString()
		.isLength({ min: 15, max: 500 })
		.withMessage("Popis musí mít nejméně 15 znaků a může mít maximálně 500 znaků"),
	body("date").notEmpty().withMessage("Datum je vyžadován").isISO8601().withMessage("Neplatný formát data"),
	body("date").toDate().isAfter(new Date().toString()).withMessage("Datum musí být déle než je aktuální čas"),
	body("tripType").notEmpty().withMessage("Typ výletu je vyžadován").isInt({ min: 1 }).withMessage("Neplatný typ výletu"),
	body("url").optional({ checkFalsy: true }).matches(REGEX_URL).withMessage("Neplatná adresa k mapě"),
	body("startPlace")
		.notEmpty()
		.withMessage("Startovní pozice je vyžadována")
		.isInt({ min: 1 })
		.withMessage("Neplatý startovací bod"),
	body("finishPlace").notEmpty().withMessage("Cílová pozice je vyžadována").isInt({ min: 1 }).withMessage("Neplatný konečný bod"),
	body("distance")
		.notEmpty()
		.withMessage("Vzdálenost je vyžadována")
		.isInt({ min: 1, max: 1000 })
		.withMessage("Vzdálenost musí být minimálně 1km dlouhá a maximálně 1000km dlouhá"),
	body("elevation")
		.optional({ checkFalsy: true })
		.isInt({ min: 1, max: 10000 })
		.withMessage("Převýšení může být minimálně 1m a maximálně 10000m"),
	body("route")
		.optional()
		.custom((val) => {
			if (val === "delete") return true;
			if (!Array.isArray(val)) throw new Error("Trasa musí být pole");
			val.forEach((point) => {
				if (typeof point !== "object" || !("lat" in point) || !("lng" in point)) throw new Error("Trasa je v chybném formátu");
			});
			return true;
		}),
	body("crossingPoints")
		.optional()
		.isArray({ max: 15 })
		.withMessage("Maximálně můžete zadat 15 průjezdných bodů")
		.custom((val) => {
			val.forEach((point) => {
				if (typeof point !== "object" || !("id" in point)) throw new Error("Průjezdné body jsou ve špatném formátu");
				if (point.description) {
					if (point.description.length < 5) throw new Error("Popis může mít mínimálně 5 znaků");
					if (point.description.length > 30) throw new Error("Popis může mít maxilmálně 30 znaků");
				}
			});
			return true;
		}),
];
