const {body, param} = require('express-validator');
const allowedValues = require('../config/reactionTypes.config');

exports.reactToTripValidation = [
    param('tripId').notEmpty().isInt({min:1}).withMessage('Neplatné id výletu'),
    body('reaction').notEmpty().isIn(allowedValues).withMessage('Chybná reakce')
];

