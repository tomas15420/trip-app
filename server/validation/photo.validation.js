const { body } = require("express-validator");

exports.addPhotoValidation = [
    body("description")
        .optional({ checkFalsy: true })
        .isString()
        .isLength({ min: 8, max: 128 })
        .withMessage(
            "Popis musí mít nejméně 8 znaků a může mít maximálně 128 znaků"
        ),
];
