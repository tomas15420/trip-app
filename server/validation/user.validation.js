const { body } = require("express-validator");
const { REGEX_PASSWORD, REGEX_LASTNAME } = require("../config/regex.config");

exports.editUserValidation = [
    body("username")
        .optional({ checkFalsy: true })
        .isString()
        .isLength({ min: 5, max: 30 })
        .withMessage(
            "Délka uživatelského jména musí být minimálně 5 a maxmimálně 30 znaků"
        ),
    body("firstname")
        .optional({checkFalsy: true})
        .isString()
        .isLength({ min: 3, max: 30 })
        .withMessage("Jméno musí být mezi 3-30 znaky")
        .matches(REGEX_LASTNAME)
        .withMessage("Jméno musí začínat velkým písmenem"),
    body("lastname")
        .optional({ checkFalsy: true })
        .isString()
        .isLength({ min: 3, max: 30 })
        .withMessage("Příjmení musí být mezi 3-30 znaky")
        .matches(REGEX_LASTNAME)
        .withMessage("Příjmení musí začínat velkým písmenem"),
    body("email")
        .optional({ checkFalsy: true })
        .isString()
        .isLength({ max: 50 })
        .withMessage("Délka emailu může být maximálně 50 znaků")
        .isEmail()
        .withMessage("Neplatný email"),
    body("password")
        .optional({ checkFalsy: true })
        .matches(REGEX_PASSWORD)
        .withMessage(
            "Heslo musí být alespoň 8 znaků dlouhé a musí obsahovat velké písmeno a číslici"
        ),
    body("district")
        .optional({ nullable: true })
        .isInt({ min: 1 })
        .withMessage("Neplatný okres"),
    body("roles").optional().isArray(),
];
