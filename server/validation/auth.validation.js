const { body, param } = require("express-validator");
const {
    REGEX_PASSWORD,
    REGEX_FIRSTNAME,
    REGEX_LASTNAME,
    REGEX_EMAIL,
} = require("../config/regex.config");

exports.registerValidation = [
    body("username")
        .notEmpty()
        .withMessage("Uživatelské jméno je vyžadováno")
        .isString()
        .isLength({ min: 3, max: 30 })
        .withMessage(
            "Délka uživatelského jména musí být minimálně 5 a maxmimálně 30 znaků"
        ),
    body("firstname")
        .optional({checkFalsy: true})
        .isString()
        .trim()
        .isLength({ min: 2, max: 30 })
        .withMessage("Jméno musí být mezi 2-30 znaky")
        .matches(REGEX_FIRSTNAME)
        .withMessage("Jméno musí začínat velkým písmenem"),
    body("lastname")
        .optional({checkFalsy: true})   
        .isString()
        .trim()
        .isLength({ min: 2, max: 30 })
        .withMessage("Příjmení musí být mezi 2-30 znaky")
        .matches(REGEX_LASTNAME)
        .withMessage("Příjmení musí začínat velkým písmenem"),
    body("email")
        .notEmpty()
        .withMessage("E-mail je vyžadován")
        .isString()
        .isLength({ max: 50 })
        .withMessage("Délka emailu může být maximálně 50 znaků")
        .isEmail()
        .withMessage("Neplatný email"),
    body("password")
        .notEmpty()
        .withMessage("Heslo je vyžadováno")
        .matches(REGEX_PASSWORD)
        .withMessage(
            "Heslo musí být alespoň 8 znaků dlouhé a musí obsahovat velké písmeno a číslici"
        ),
    body("passwordAgain")
        .notEmpty()
        .withMessage("Pole je vyždováno")
        .custom((value, { req }) => {
            if (req.body.password !== value) {
                throw new Error("Hesla se neshodují");
            }
            return true;
        }),
];

exports.loginValidation = [
    body("username").notEmpty().withMessage("Uživatelské jméno je vyžadováno"),
    body("password").notEmpty().withMessage("Heslo je vyžadováno"),
];
