const {param, query} = require('express-validator')

exports.getTripValidation = [
    param('tripId').optional().isInt({min: 1}).withMessage('Neplatné ID výletu'),
    query('userId').optional().isInt({min: 1}).withMessage('Neplatné ID uživatele'),
    query('upcoming').optional().isBoolean().withMessage('Neplatná hodnota'),
    query('crossingPoint').optional().isInt({min: 1}).withMessage('Neplatná hodnota průchozího bodu'),
    query('startPlace').optional().isInt({min: 1}).withMessage('Neplatná hodnota startovacího bodu'),
    query('finishPlace').optional().isInt({min: 1}).withMessage('Neplatná hodnota konečného bodu'),
    query('tripType').optional().isInt({min: 1}).withMessage('Neplatná hodnota typu výletu'),
    query('limit').optional().isInt({min: 1, max: 50}).withMessage('Neplatná hodnota omezení'),
    query('offset').optional().isInt({min: 0}).withMessage('Neplatná hodnota odsazení'),
]