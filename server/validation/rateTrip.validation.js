const {body, param} = require('express-validator');

exports.rateTripValidation = [
    param('tripId').notEmpty().isInt({min:1}).withMessage('Neplatné id výletu'),
    body('stars').notEmpty().isInt({max:5, min:1}).withMessage('Rozmezí hodnocení 1 - 5')
];
