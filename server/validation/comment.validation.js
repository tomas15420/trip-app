const {body, param} = require('express-validator');

exports.createCommentValidation = [
    body('text').notEmpty().withMessage('Text komentáře je vyžadován').isString().isLength({min: 1, max: 256}).withMessage('Délka textu musí být minimálně 1 a maxmimálně 256 znaků'),
    param('tripId').notEmpty().withMessage('Parametr ID výletu je vyžadován').isInt({min: 1}).withMessage('Chybné ID výletu'),
];

exports.updateCommentValidation = [
    body('text').notEmpty().withMessage('Text komentáře je vyžadován').isString().isLength({min: 1, max: 256}).withMessage('Délka textu musí být minimálně 1 a maxmimálně 256 znaků'),
    param('id').notEmpty().withMessage('Parametr ID komentáře je vyžadován').isInt({min: 1}).withMessage('Chybné ID komentáře'),
];