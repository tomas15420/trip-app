const express = require("express");
const cors = require("cors");
const { validateToken } = require("./middleware/jwt.middleware");
const path = require("path");
const app = express();
const cookieParser = require("cookie-parser");
require("dotenv").config();

const whiteListCors = [];

if (process.env.FRONTEND_URL) {
	whiteListCors.push(process.env.FRONTEND_URL);
}

const corsOptions = {
	origin: (origin, callback) => {
		if (whiteListCors.indexOf(origin) !== -1 || !origin) {
			callback(null, true);
		} else {
			callback(new Error("Not allowed by CORS: " + origin));
		}
	},
	credentials: true,
	optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

const db = require("./models");
const PORT = 3500;

const router = express.Router();
require("./routes/auth.router")(router);
router.use(validateToken);

require("./routes/user.router")(router);
require("./routes/location.router")(router);
require("./routes/trip.router")(router);
require("./routes/comment.router")(router);
require("./routes/image.router")(router);
require("./routes/role.router")(router);
require("./routes/photo.router")(router);

app.use("/api/v1", router);
app.use("/api/v1/files/routes/", express.static(path.join(__dirname, "files/routes")));

db.sequelize
	.sync({ force: false, alter: false })
	.then(() => {
		console.log("Resync database");
		app.listen(PORT, (error) => {
			if (error) console.log("error occured " + error);
			else console.log("app is running on " + process.env.BASE_URL);
		});
	})
	.catch((e) => {
		console.log("error occured while syncing database", e);
	});
