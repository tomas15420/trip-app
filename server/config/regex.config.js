module.exports = {
    REGEX_PASSWORD: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
    REGEX_EMAIL: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
    REGEX_FIRSTNAME: /^[A-ZÁ-Ž][a-zá-ž]{2,29}$/,
    REGEX_LASTNAME: /^[A-ZÁ-Ž][a-zá-ž]{2,29}$/,
    REGEX_URL: /[(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/ig
}