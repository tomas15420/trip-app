const photo = require("../controllers/photo.controller");
const { authUser } = require("../middleware/requireUser.middleware");

module.exports = (app) => {
	const router = require("express").Router();

	router.delete("/:photoId", authUser(true), photo.deletePhoto);

	app.use("/photos", router);
};
