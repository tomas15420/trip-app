const trip = require("../controllers/trip.controller");
const rating = require("../controllers/rating.controller");
const comment = require("../controllers/comment.controller");
const reaction = require("../controllers/reaction.controller");
const tripTypes = require("../controllers/tripType.controller");
const photos = require("../controllers/photo.controller");
const { createTripValidation } = require("../validation/createTrip.validation");
const { rateTripValidation } = require("../validation/rateTrip.validation");
const { getTripValidation } = require("../validation/getTrip.validation");
const { createCommentValidation } = require("../validation/comment.validation");
const { reactToTripValidation } = require("../validation/react.validation");
const { validate } = require("../middleware/validation.middleware");
const fileUpload = require("express-fileupload");
const { addPhotoValidation } = require("../validation/photo.validation");
const { authUser } = require("../middleware/requireUser.middleware");
const { filter } = require("../middleware/filter.middleware");
const formParserMiddleware = require("../middleware/formParser.middleware");

module.exports = (app) => {
	const router = require("express").Router();

	//Types
	router.route("/types").get(tripTypes.getTripTypes);

	//Trip
	router.post("/", authUser(), createTripValidation, validate, trip.createTrip);
	router.get(
		"/:id?",
		filter(["id", "userId", "startPlaceId", "finishPlaceId", "tripTypeId"]),
		getTripValidation,
		validate,
		trip.getTrips
	);
	router
		.route("/:tripId")
		.patch(authUser(true), createTripValidation, validate, trip.updateTrip)
		.delete(authUser(true), trip.deleteTrip);

	//Rating
	router
		.route("/:tripId/ratings")
		.post(authUser(), rateTripValidation, validate, rating.rateTrip)
		.get(filter(), rating.getTripRatings)
		.delete(authUser(), rating.removeRating);

	//Comments
	router
		.route("/:tripId/comments")
		.get(filter("tripId"), comment.getComments)
		.post(authUser(true), createCommentValidation, validate, comment.createComment);

	//Reactions
	router
		.route("/:tripId/reactions")
		.get(filter(), reaction.getTripReactions)
		.post(authUser(), reactToTripValidation, validate, reaction.react)
		.delete(authUser(), reaction.deleteReaction);

	//Photos
	const fileUploadOptions = {
		createParentPath: true,
		limits: { fileSize: 2 * 1024 * 1024 },
	};
	router
		.route("/:tripId/photos")
		.get(filter("tripId"), photos.getPhotos)
		.post(authUser(), fileUpload(fileUploadOptions), formParserMiddleware, addPhotoValidation, validate, photos.addPhoto);

	app.use("/trips", router);
};
