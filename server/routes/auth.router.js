const authController = require("../controllers/auth.controller");
const { validate } = require("../middleware/validation.middleware");
const { registerValidation, loginValidation } = require("../validation/auth.validation");

module.exports = (app) => {
	const router = require("express").Router();

	router.post("/register", registerValidation, validate, authController.register);
	router.post("/login", loginValidation, validate, authController.login);
	router.post("/refreshToken", authController.refreshToken);
	router.post("/logout", authController.logout);

	app.use("/auth", router);
};
