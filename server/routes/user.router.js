const user = require("../controllers/user.controller");
const follower = require("../controllers/follower.controller");
const reaction = require("../controllers/reaction.controller");
const rating = require("../controllers/rating.controller");

const { editUserValidation } = require("../validation/user.validation");
const { validate } = require("../middleware/validation.middleware");

const fileUpload = require("express-fileupload");
const { authUser } = require("../middleware/requireUser.middleware");
const { filter } = require("../middleware/filter.middleware");
const formParseMiddleware = require("../middleware/formParser.middleware");

module.exports = (app) => {
	const router = require("express").Router();

	router.get("/:id?", authUser(true, false),filter(["id","username","email"]),user.getUsers);

	const fileUploadOptions = {
		createParentPath: true,
		limits: { fileSize: 1 * 1024 * 1024 },
	};

	router.patch("/:userId", authUser(true) ,fileUpload(fileUploadOptions), formParseMiddleware, editUserValidation, validate, user.updateUser);

	router.route("/follow-user/:userId").post(authUser(false), follower.followUser).delete(authUser(false), follower.unfollowUser);

	router.get("/:userId/ratings", filter(), rating.getUserRatings);

	router.get("/:userId/followers", filter(), follower.getFollowers);
	router.get("/:userId/following", filter(), follower.getFollowing);

	router.get("/:userId/reactions", filter(), reaction.getUserReactions);
	app.use("/users", router);
};
