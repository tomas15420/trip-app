const image = require("../controllers/image.controller")

module.exports = (app) => {
    const router = require("express").Router();

    router.get("/avatars/:filename", image.getImage);
    router.get("/photos/:filename", image.getImage);
    
    app.use("/images", router);
};
