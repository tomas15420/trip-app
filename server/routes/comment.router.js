const comment = require("../controllers/comment.controller");

const {updateCommentValidation} = require('../validation/comment.validation');
const {validate} = require('../middleware/validation.middleware');
const { authUser } = require("../middleware/requireUser.middleware");
const { filter } = require("../middleware/filter.middleware");

module.exports = (app) => {
    const router = require("express").Router();
    router.route("/:id")
        .get(filter("id") ,comment.getComments)
        .delete(authUser(true), comment.deleteComment)
        .patch(authUser(true), updateCommentValidation, validate, comment.updateComment);

    app.use("/comments", router);
};
