const location = require("../controllers/location.controller");
const { filter } = require("../middleware/filter.middleware");

module.exports = (app) => {
	const router = require("express").Router();

	router.get("/municipalities/:id?", filter(["id", "districtId"]), location.getMunicipalities);
	router.get("/regions", filter(), location.getRegions);
	router.get("/districts", filter("regionId"), location.getDistricts);

	app.use("/location", router);
};
