const role = require("../controllers/role.controller");
const { authUser } = require("../middleware/requireUser.middleware");

module.exports = (app) => {
	const router = require("express").Router();

	router.get("/", authUser(true), role.getRoles);

	app.use("/roles", router);
};
