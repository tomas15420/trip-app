module.exports = (sequelize, DataTypes) => {
    const Municipality = sequelize.define("municipalities", {
        id: {
            type: DataTypes.INTEGER, 
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(128),
            allowNull: false,
        },
        PSC:{
            type: DataTypes.CHAR(6),
            allowNull: false,
        },
        ZUJ:{
            type: DataTypes.CHAR(6),
            allowNull: false,
        }
    },
    {timestamps: false});

    return Municipality;
}