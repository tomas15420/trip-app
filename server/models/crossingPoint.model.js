module.exports = (sequelize, DataTypes) => {
    const crossingPoint = sequelize.define("crossing_points",{
        id:{
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
    }) 

    return crossingPoint;
};