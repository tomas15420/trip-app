const {createRefreshToken} = require('../middleware/jwt.middleware');

module.exports = (sequelize, DataTypes) => {
    const RefreshToken = sequelize.define('refreshToken', {
        id:{
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        token:{
            allowNull: false,
            type: DataTypes.STRING
        },
        expiration:{
            allowNull: false,
            type: DataTypes.DATE
        }
    });

    RefreshToken.createToken = async (user) => {
        const refreshToken = createRefreshToken(user);
        let expiration = new Date();
        expiration.setMinutes(expiration.getMinutes() + 10);
        await RefreshToken.create({
            token: refreshToken,
            userId: user.id,
            expiration: expiration,
        });
        return refreshToken;
    };

    return RefreshToken;
};
