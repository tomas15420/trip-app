module.exports = (sequelize, DataTypes) => {
    const Comment = sequelize.define("comments",{
        id: {
            type: DataTypes.INTEGER, 
            primaryKey: true,
            autoIncrement: true
        },
        text: {
            type: DataTypes.STRING(256),
            allowNull: false,
        },
    });

    return Comment;
}