module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define("roles", {
        id: {
            type: DataTypes.INTEGER, 
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(30),
            allowNull: false,
        },
        role: {
            type: DataTypes.STRING(20),
            allowNull: false
        }
    });

    Role.sync().then(async () => {
        try{
            const isEmpty = !await Role.count();
            if(isEmpty){
                Role.bulkCreate(require('../config/roles.config'));        
            }
        }catch(e){
            console.log('Error syncing TripType table' + e.message);
        }
    })

    return Role;
}