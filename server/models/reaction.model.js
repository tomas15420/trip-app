module.exports = (sequelize, DataTypes) => {
    const Reaction = sequelize.define(
        "reactions",
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
        },
        {
            indexes: [{ unique: true, fields: ["userId", "tripId"] }],
        }
    );

    return Reaction;
};
