const Sequelize = require("sequelize");

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
	host: process.env.DB_HOST,
	dialect: "mysql",
});

const db = {};

db.user = require("../models/user.model")(sequelize, Sequelize.DataTypes);
db.role = require("../models/role.model")(sequelize, Sequelize.DataTypes);
db.region = require("../models/region.model")(sequelize, Sequelize.DataTypes);
db.district = require("../models/district.model")(sequelize, Sequelize.DataTypes);
db.refreshToken = require("../models/refreshToken.model")(sequelize, Sequelize.DataTypes);
db.municipality = require("../models/municipality.model")(sequelize, Sequelize.DataTypes);
db.trip = require("../models/trip.model")(sequelize, Sequelize.DataTypes);
db.crossingPoint = require("../models/crossingPoint.model")(sequelize, Sequelize.DataTypes);
db.rating = require("../models/rating.model")(sequelize, Sequelize.DataTypes);
db.tripType = require("../models/tripType.model")(sequelize, Sequelize.DataTypes);
db.comment = require("../models/comment.model")(sequelize, Sequelize.DataTypes);
db.reaction = require("../models/reaction.model")(sequelize, Sequelize.DataTypes);
db.reactionType = require("../models/reactionType.model")(sequelize, Sequelize.DataTypes);
db.photo = require("../models/photo.model")(sequelize, Sequelize.DataTypes);

db.role.belongsToMany(db.user, {
	through: "user_roles",
});

db.user.belongsToMany(db.role, {
	through: "user_roles",
});

db.refreshToken.belongsTo(db.user, {
	onDelete: "CASCADE",
	foreignKey: { allowNull: false },
});
db.user.hasMany(db.refreshToken);

db.district.belongsTo(db.region, {
	foreignKey: { allowNull: false },
});
db.region.hasMany(db.district);

db.municipality.belongsTo(db.district, {
	foreignKey: { allowNull: false },
});
db.district.hasMany(db.municipality);

db.trip.belongsTo(db.user, {
	onDelete: "CASCADE",
	foreignKey: { allowNull: false },
});
db.user.hasMany(db.trip);

db.municipality.hasMany(db.trip, {
	as: "startPlace",
	foreignKey: {
		name: "startPlaceId",
		allowNull: false,
	},
});
db.trip.belongsTo(db.municipality, {
	as: "startPlace",
	foreignKey: {
		name: "startPlaceId",
		allowNull: false,
	},
});

db.municipality.hasMany(db.trip, {
	as: "finishPlace",
	foreignKey: {
		name: "finishPlaceId",
		allowNull: false,
	},
});
db.trip.belongsTo(db.municipality, {
	as: "finishPlace",
	foreignKey: {
		name: "finishPlaceId",
		allowNull: false,
	},
});

db.trip.belongsToMany(db.municipality, {
	foreignKey: { name: "tripId", allowNull: false },
	as: "crossingPoints",
	through: {
		model: db.crossingPoint,
		unique: false,
	},
});

db.municipality.belongsToMany(db.trip, {
	foreignKey: { name: "municipalityId", allowNull: false },
	through: {
		model: db.crossingPoint,
		unique: false,
	},
});

//Trip rating
db.user.belongsToMany(db.trip, {
	through: db.rating,
	as: "ratedTrips",
	foreignKey: { name: "userId", allowNull: false },
});
db.trip.belongsToMany(db.user, {
	through: db.rating,
	as: "raters",
	foreignKey: { name: "tripId", allowNull: false },
});

//Trip Type
db.trip.belongsTo(db.tripType, {
	onDelete: "RESTRICT",
	foreignKey: { allowNull: false },
});
db.tripType.hasMany(db.trip);

//Comments
db.comment.belongsTo(db.trip, {
	onDelete: "CASCADE",
	foreignKey: { allowNull: false },
});
db.trip.hasMany(db.comment);

db.comment.belongsTo(db.user);
db.user.hasMany(db.comment);

//Followers
db.user.belongsToMany(db.user, {
	as: "following",
	through: "followers",
	foreignKey: "followerId",
});

db.user.belongsToMany(db.user, {
	as: "follower",
	through: "followers",
	foreignKey: "followingId",
});

//Reaction Types
db.reaction.belongsTo(db.reactionType, {
	onDelete: "RESTRICT",
	as: "reactionType",
	foreignKey: { allowNull: false, name: "reactionTypeId" },
});
db.reactionType.hasMany(db.reaction);

//Reactions
db.user.belongsToMany(db.trip, { through: db.reaction, as: "userReactions" });
db.trip.belongsToMany(db.user, { through: db.reaction, as: "tripReactions" });

db.reaction.belongsTo(db.user, {
	onDelete: "CASCADE",
	foreignKey: {
		allowNull: false,
	},
});
db.user.hasMany(db.reaction);

db.reaction.belongsTo(db.trip, {
	onDelete: "CASCADE",
	foreignKey: {
		allowNull: false,
	},
});
db.trip.hasMany(db.reaction);

db.user.belongsTo(db.district);
db.district.hasMany(db.user);

db.photo.belongsTo(db.trip);
db.trip.hasMany(db.photo);

db.photo.belongsTo(db.user);
db.user.hasMany(db.photo);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
