module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define(
        "users",
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            firstname: {
                type: DataTypes.STRING(30),
                allowNull: true,
            },
            lastname: {
                type: DataTypes.STRING(30),
                allowNull: true,
            },
            username: {
                type: DataTypes.STRING(30),
                allowNull: false,
                unique: "username",
            },
            email: {
                type: DataTypes.STRING(50),
                allowNull: false,
                unique: "email",
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            avatar: {
                type: DataTypes.STRING(128),
                allowNull: true,
            },
            avatarPath: {
                type: DataTypes.VIRTUAL,
                get() {
                    const url = process.env.BASE_URL;
                    if (this.avatar) return url + "images/avatars/" + this.avatar;
                    return null;
                },
                set(value) {
                    throw new Error("Nemůžete nastavovat hodnotu avatarPath");
                },
            },
        },
        {
            createdAt: "registered",
            updatedAt: "updated",
        }
    );

    return User;
};
