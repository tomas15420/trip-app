module.exports = (sequelize, DataTypes) => {
    const District = sequelize.define("districts", {
        id: {
            type: DataTypes.INTEGER, 
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(64),
            allowNull: false,
        },
        code: {
            type: DataTypes.CHAR(6),
            allowNull: false,
        }
    },
    {timestamps: false});

    return District;
}