module.exports = (sequelize, DataTypes) => {
    const Photo = sequelize.define("photos", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING(128),
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING(128),
            allowNull: true,
        },
        path: {
            type: DataTypes.VIRTUAL,
            get() {
                const url = process.env.BASE_URL;
                if (this.name) return url + "images/photos/" + this.name;
                return null;
            },
            set(value) {
                throw new Error("Nemůžete nastavovat hodnotu path");
            },
        }
    });
    return Photo;
};
