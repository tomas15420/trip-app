module.exports = (sequelize, DataTypes) => {
	const ReactionType = sequelize.define(
		"reaction_types",
		{
			id: {
				primaryKey: true,
				type: DataTypes.STRING(32),
			},
		},
		{ timestamps: false }
	);

	ReactionType.sync().then(async () => {
		try {
			const isEmpty = !(await ReactionType.count());
			if (isEmpty) {
				const types = require("../config/reactionTypes.config").map((item) => {
					return { id: item };
				});
				ReactionType.bulkCreate(types);
			}
		} catch (e) {
			console.log("Error syncing TripType table" + e.message);
		}
	});

	return ReactionType;
};
