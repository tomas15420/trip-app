module.exports = (sequelize, DataTypes) => {
    const TripType = sequelize.define("trip_types", {
        id: {
            type: DataTypes.INTEGER, 
            primaryKey: true,
            autoIncrement: true
        },
        type: {
            type: DataTypes.STRING(32),
            allowNull: false,
            unique: 'type'
        }
    })

    TripType.sync().then(async () => {
        try{
            const isEmpty = !await TripType.count();
            if(isEmpty){
                const types = require('../config/tripTypes.config').map(item => {return {type: item}});
                TripType.bulkCreate(types);        
            }
        }catch(e){
            console.log('Error syncing TripType table' + e.message);
        }
    })

    return TripType;
}