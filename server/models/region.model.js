module.exports = (sequelize, DataTypes) => {
    const Region = sequelize.define("regions", {
        id: {
            type: DataTypes.INTEGER, 
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(20),
            allowNull: false,
        },
        code:{
            type: DataTypes.CHAR(5),
            allowNull: false
        }
    },
    {timestamps: false});

    return Region;
}