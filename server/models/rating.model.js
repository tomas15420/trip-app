module.exports = (sequelize, DataTypes) => {
    const Rating = sequelize.define(
        "ratings",
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            stars: {
                type: DataTypes.INTEGER,
                allowNull: false,
                validate: {
                    max: 5,
                    min: 1,
                },
            },
        },
        {
            indexes: [{ unique: true, fields: ["userId", "tripId"] }],
        }
    );

    return Rating;
};
