module.exports = (sequelize, DataTypes) => {
	const Trip = sequelize.define("trips", {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		name: {
			type: DataTypes.STRING(64),
			allowNull: false,
		},
		description: {
			type: DataTypes.STRING(500),
			allowNull: true,
		},
		date: {
			type: DataTypes.DATE,
			default: DataTypes.NOW,
			allowNull: false,
		},
		url: {
			type: DataTypes.STRING,
			allowNull: true,
		},
		distance: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		elevation: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
		route: {
			type: DataTypes.STRING(128),
			allowNull: true,
		},
		routePath: {
			type: DataTypes.VIRTUAL,
			get() {
				const url = process.env.BASE_URL;
				if (this.route) return url + "files/routes/" + this.route;
				return null;
			},
			set(value) {
				throw new Error("Nemůžete nastavovat hodnotu avatarPath");
			},
		},
	});

	return Trip;
};
