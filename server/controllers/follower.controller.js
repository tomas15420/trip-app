const db = require("../models");
const User = db.user;

module.exports = {
	followUser: async (req, res) => {
		const { user } = req.auth;
		const { userId } = req.params;
		if (user.id == userId)
			return res.status(400).json({
				success: false,
				message: "Nemůžete sledovat sám sebe",
			});

		try {
			const toFollow = await User.findByPk(userId);
			if (!toFollow)
				return res.status(404).json({
					success: false,
					message: "Uživatel s tímto ID neexistuje",
				});

			if (await user.hasFollowing(toFollow))
				return res.status(400).json({
					success: false,
					message: "Uživatele už sledujete",
				});

			await user.addFollowing(toFollow);
			res.json({ success: true, userId: toFollow.id });
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	unfollowUser: async (req, res) => {
		const { user } = req.auth;
		const { userId } = req.params;
		if (user.id == userId)
			return res.status(400).json({
				success: false,
				message: "Nemůžete přestat sledovat sám sebe",
			});
		try {
			const toUnFollow = await User.findByPk(userId);
			if (!toUnFollow)
				return res.status(404).json({
					success: false,
					message: "Uživatel s tímto ID neexistuje",
				});

			if (!(await user.hasFollowing(toUnFollow)))
				return res.status(400).json({
					success: false,
					message: "Uživatele nesledujete",
				});
			await user.removeFollowing(toUnFollow);
			res.json({
				success: true,
				userId: toUnFollow.id,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	getFollowers: async (req, res) => {
		const { userId } = req.params;
		const { filterOptions } = req;
		try {
			const user = await User.findByPk(userId);
			if (!user) return res.status(404).json({ success: false, message: "Uživatel s tímto ID nenalezen" });

			const followers = await user.getFollower({
				joinTableAttributes: [],
				attributes: ["id", "username", "registered", "avatar", "avatarPath"],
				...filterOptions,
			});

			const total = await user.countFollower();
			res.json({ success: true, count: followers.length, total, followers });
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	getFollowing: async (req, res) => {
		const { userId } = req.params;
		const { filterOptions } = req;
		try {
			const user = await User.findByPk(userId);
			if (!user) return res.status(404).json({ success: false, message: "Uživatel s tímto ID nenalezen" });

			const followings = await user.getFollowing({
				joinTableAttributes: [],
				attributes: ["id", "username", "registered", "avatar", "avatarPath"],
				...filterOptions,
			});
			const total = await user.countFollowing();
			res.json({ success: true, count: followings.length, total, followings });
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},
};
