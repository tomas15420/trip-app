const db = require("../models");
const Role = db.role;

module.exports = {
    getRoles: async (req, res) => {
        const { roles: userRoles } = req.auth; 
        try {
            if (!userRoles.includes("HL_ADMIN"))
                return res
                    .status(403)
                    .json({ success: false, message: "Neoprávněný přístup" });
            const roles = await Role.findAll({ attributes: ["role", "name"] });
            res.json(roles);
        } catch (e) {
            res.status(500).json({ success: false, message: e.message });
        }
    },
};
