const db = require("../models");
const Comment = db.comment;
const User = db.user;
const Trip = db.trip;

module.exports = {
	getComments: async (req, res) => {
		try {
			const { filterOptions } = req;

			const comments = await Comment.findAll({
				attributes: ["id", "text", "createdAt", "updatedAt"],
				include: [
					{
						model: User,
						attributes: ["id", "username", "avatar", "avatarPath"],
					},
				],
				...filterOptions,
			});
			const total = await Comment.count({ where: filterOptions.where });

			res.json({
				success: true,
				count: comments.length,
				total,
				comments,
			});
		} catch (e) {
			res.status(500).json({ succes: false, message: e.message });
		}
	},

	createComment: async (req, res) => {
		const { user } = req.auth;
		const { tripId } = req.params;
		const { text } = req.body;

		try {
			const trip = await Trip.findByPk(tripId);
			if (!trip)
				return res.status(404).json({
					success: false,
					message: "Výlet s tímto ID neexistuje",
				});

			const comment = await Comment.create({
				text,
				userId: user.id,
				tripId: trip.id,
			});
			res.status(201).json({ success: true, comment });
		} catch (e) {
			res.status(500).json({ succes: false, message: e.message });
		}
	},

	deleteComment: async (req, res) => {
		const { user, roles } = req.auth;
		const { id } = req.params;
		try {
			const comment = await Comment.findByPk(id);
			if (!comment)
				return res.status(404).json({
					success: false,
					message: "Komentář s tímto ID neexistuje",
				});
			if (comment.userId !== user.id && !roles.includes("ADMIN"))
				return res.status(403).json({
					success: false,
					message: "Na odstranění tohoto komentáře nemáte oprávnění",
				});
			await comment.destroy();

			res.json({
				success: true,
				message: "Komentář odstraněn",
			});
		} catch (e) {
			res.status(500).json({ succes: false, message: e.message });
		}
	},
	updateComment: async (req, res) => {
		const { user, roles } = req.auth;

		const { id } = req.params;
		const { text } = req.body;
		try {
			let comment = await Comment.findByPk(id);
			if (!comment)
				return res.status(404).json({
					success: false,
					message: "Komentář s tímto ID neexistuje",
				});
			if (comment.userId !== user.id && !roles.includes("ADMIN"))
				return res.status(403).json({
					success: false,
					message: "Neoprávněný přístup",
				});

			comment = await comment.update({ text });

			res.json({
				success: true,
				comment,
			});
		} catch (e) {
			res.status(500).json({ succes: false, message: e.message });
		}
	},
};
