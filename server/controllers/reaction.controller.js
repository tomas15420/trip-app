const db = require("../models");
const Reaction = db.reaction;
const User = db.user;
const Trip = db.trip;
const ReactionType = db.reactionType;

module.exports = {
	getTripReactions: async (req, res) => {
		const { filterOptions } = req;
		const { tripId } = req.params;
		try {
			const trip = await Trip.findByPk(tripId);
			if (!trip) return res.status(404).json({ success: false, message: "Výlet s tímto ID neexistuje" });
			const reactionTypeQuery = req.query.reactionType;

			let reactionType;
			if (reactionTypeQuery && reactionTypeQuery !== "ALL") {
				reactionType = await ReactionType.findByPk(reactionTypeQuery);
				if (!reactionType) return res.status(400).json({ success: false, message: "Tato reakce neexistuje" });
			}

			const participants = await trip.getTripReactions({
				attributes: { exclude: ["email", "password", "updated", "districtId"] },
				joinTableAttributes: ["id", "reactionTypeId", "createdAt"],
				through: { where: reactionType ? { reactionTypeId: reactionType.id } : {} },
				...filterOptions,
			});

			const participates =
				!reactionType || reactionType.id === "PARTICIPATE"
					? await trip.countTripReactions({ through: { where: { reactionTypeId: "PARTICIPATE" } } })
					: 0;
			const maybeParticipates =
				!reactionType || reactionType.id === "MAYBE_PARTICIPATE"
					? await trip.countTripReactions({ through: { where: { reactionTypeId: "MAYBE_PARTICIPATE" } } })
					: 0;
			res.json({
				count: participants.count,
				participates,
				maybeParticipates,
				total: participates + maybeParticipates,
				participants,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	getUserReactions: async (req, res) => {
		const { filterOptions } = req;
		const { userId } = req.params;

		try {
			const user = await User.findByPk(userId);
			if (!user) return res.status(404).json({ success: false, message: "Uživatel s tímto ID neexistuje" });
			const trips = await user.getUserReactions({
				include: [{ model: db.municipality, as: "startPlace", include: [db.district] }],
				joinTableAttributes: ["id", "createdAt", "reactionTypeId"],
				attributes: {
					include: [
						[
							db.sequelize.literal("(SELECT COUNT (*) FROM reactions WHERE tripId=trips.id AND reactionTypeId='PARTICIPATE')"),
							"participates",
						],
						[
							db.sequelize.literal("(SELECT COUNT (*) FROM reactions WHERE tripId=trips.id AND reactionTypeId='MAYBE_PARTICIPATE')"),
							"maybeParticipates",
						],
					],
				},
				...filterOptions,
			});
			res.json(trips);
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	react: async (req, res) => {
		const { user } = req.auth;
		const { tripId } = req.params;
		const { reaction } = req.body;
		
		try {
			const trip = await Trip.findByPk(tripId);
			if (!trip) return res.status(404).json({ success: false, message: "Výlet s tímto ID neexistuje" });

			const reactionType = await ReactionType.findOne({
				where: { id: reaction },
			});

			if (!reactionType)
				return res.status(400).json({
					success: false,
					message: "Neplatná reakce na výlet",
				});

			const prevReaction = await Reaction.findOne({
				where: { tripId, userId: user.id },
			});

			let reactionRecord;
			if (!prevReaction) {
				reactionRecord = await reactionType.createReaction({
					tripId,
					userId: user.id,
				});
			} else {
				if (prevReaction.reactionTypeId != reactionType.id) {
					reactionRecord = await prevReaction.setReactionType(reactionType);
				} else reactionRecord = prevReaction;
			}
			res.json({ success: true, reactionRecord });
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	deleteReaction: async (req, res) => {
		const { user } = req.auth;
		const { tripId } = req.params;
		try {
			const trip = await Trip.findByPk(tripId);
			if (!trip) return res.status(404).json({ success: false, message: "Výlet s tímto ID neexistuje" });

			const reaction = await Reaction.findOne({
				where: { tripId, userId: user.id },
			});

			if (!reaction)
				return res.status(400).json({
					success: false,
					message: "Na tento výlet jste ještě nereagoval",
				});

			await reaction.destroy();

			res.json({
				success: true,
				message: `Reakce na výlet ${trip.name} odstraněna`,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},
};
