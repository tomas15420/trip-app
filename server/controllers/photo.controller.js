const db = require("../models");
const fs = require("fs");
const path = require("path");
const { v4: uuid } = require("uuid");
const Photo = db.photo;
const Trip = db.trip;
const User = db.user;

module.exports = {
	getPhotos: async (req, res) => {
		const { filterOptions } = req;
		try {
			const photos = await Photo.findAll({
				include: [
					{
						attributes: ["username", "avatar", "avatarPath"],
						model: User,
					},
					{
						model: Trip,
						attributes: ["name", "description", "date"],
					},
				],
				...filterOptions,
			});

			const total = await Photo.count({ where: filterOptions.where });
			res.json({ count: photos.length, total, photos });
		} catch (e) {
			res.status(400).json({ success: false, message: e.message });
		}
	},

	addPhoto: async (req, res) => {
		const { user } = req.auth;
		const { tripId } = req.params;
		const file = req.files?.photo;

		try {
			if (!file)
				return res.status(400).json({
					success: false,
					message: "Žádný obrázek nebyl nahrán",
				});

			if (file.truncated)
				return res.status(413).json({
					success: false,
					message: "Soubor je příliš velký",
				});

			if (!["image/png", "image/jpeg"].includes(file.mimetype))
				return res.status(422).json({ success: false, message: "Nepodporovaný typ souboru" });

			const trip = await Trip.findByPk(tripId);
			if (!trip) return res.status(404).json({ success: false, message: "Výlet s tímto ID neexistuje" });
			if (trip.date > new Date())
				return res.status(400).json({
					success: false,
					message: "Fotografie lze přidávat až po skončení výletu",
				});

			let { description } = req.body;
			if(!description) description = null;

			const filename = uuid() + path.extname(file.name);

			const photo = await db.sequelize.transaction(async (t) => {
				const photo = await Photo.create(
					{
						name: filename,
						description,
						userId: user.id,
						tripId: trip.id,
					},
					{ transaction: t }
				);
				try {
					await file.mv(path.join(__dirname, "..", "/images/photos", filename));
				} catch (e) {
					throw new Error("Nahrání souboru selhalo");
				}
				return photo;
			});

			res.json({ success: true, photo });
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	deletePhoto: async (req, res) => {
		const { user, roles } = req.auth;
		const { photoId } = req.params;
		try {
			const photo = await Photo.findByPk(photoId);
			if (!photo)
				return res.status(404).json({
					success: false,
					message: "Fotografie nebyla nalezena",
				});

			if (photo.userId !== user.id && !roles.includes("ADMIN"))
				return res.status(403).json({ success: false, message: "Na odstranění této fotografie nemáte oprávnění" });

			const filePath = path.join(__dirname, "..", "images", "photos", photo.name);
			if (fs.existsSync(filePath)) {
				await fs.promises.unlink(filePath);
			}
			await photo.destroy();
			res.json({ success: true, deletedPhoto: photo });
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},
};
