const { Op } = require("sequelize");
const db = require("../models");
const fs = require("fs");
const path = require("path");
const { v4: uuid } = require("uuid");
const Trip = db.trip;
const Municipality = db.municipality;
const CrossingPoint = db.crossingPoint;
const TripType = db.tripType;

module.exports = {
	getTrips: async (req, res) => {
		try {
			let { filterOptions } = req;
			const { upcoming, includeCP, startPlaceDistrict, startPlaceRegion, name } = req.query;

			const currentDate = new Date().toISOString();
			if (upcoming === "true") filterOptions.where.date = { [Op.gt]: currentDate };

			let startPlaceWhere = startPlaceDistrict ? { id: startPlaceDistrict } : startPlaceRegion ? { regionId: startPlaceRegion } : {};

			const { user } = req;

			if (name) {
				filterOptions.where.name = { [Op.like]: "%" + name + "%" };
			}

			if (filterOptions.order)
				filterOptions.order = db.sequelize.literal(`${filterOptions.order[0][0]} ${filterOptions.order[0][1]}`);

			if (includeCP === "true" && filterOptions.where.startPlaceId) {
				const optionss = {
					attributes: [db.sequelize.fn("DISTINCT", db.sequelize.col("tripId"))],
					where: {
						municipalityId: filterOptions.where.startPlaceId,
					},
				};
				const subQuery = await db.sequelize
					.getQueryInterface()
					.queryGenerator.selectQuery("crossing_points", optionss, CrossingPoint)
					.slice(0, -1);

				const startPlaceId = filterOptions.where.startPlaceId;
				delete filterOptions.where.startPlaceId;
				startPlaceWhere = {};

				filterOptions.where = {
					...filterOptions.where,
					[Op.or]: [
						{
							id: {
								[Op.in]: db.sequelize.literal(`(${subQuery})`),
							},
						},
						{ startPlaceId },
					],
				};
			}

			let includeAttributes = [
				[db.sequelize.literal("(SELECT AVG(stars) FROM ratings WHERE tripId=trips.id)"), "avgStars"],
				[
					db.sequelize.literal(
						"(SELECT COUNT(*) FROM reactions WHERE reactions.reactionTypeId = 'PARTICIPATE' AND reactions.tripId=trips.id)"
					),
					"participates",
				],
				[
					db.sequelize.literal(
						"(SELECT COUNT(*) FROM reactions WHERE reactions.reactionTypeId = 'MAYBE_PARTICIPATE' AND reactions.tripId=trips.id)"
					),
					"maybe_participates",
				],
				[
					db.sequelize.literal(
						`(SELECT reactions.reactionTypeId FROM reactions WHERE reactions.tripId=trips.id AND reactions.userId=${
							user?.userId || null
						})`
					),
					"user_participation",
				],
			];

			if (user) {
				includeAttributes.push([
					db.sequelize.literal(`(SELECT stars FROM ratings WHERE userId=${user.userId} AND tripId=trips.id)`),
					"userStars",
				]);
			}

			let options = {
				attributes: {
					include: includeAttributes,
				},
				include: [
					{
						model: db.user,
						attributes: ["username"],
						required: true,
					},
					{
						model: TripType,
						attributes: ["type"],
						required: true,
					},
					{
						model: Municipality,
						as: "startPlace",
						attributes: ["name", "districtId"],
						required: true,
						include: {
							required: true,
							model: db.district,
							attributes: ["id", "name", "regionId"],
							as: "district",
							where: startPlaceWhere,
						},
					},
					{
						model: Municipality,
						as: "finishPlace",
						attributes: ["name", "districtId"],
						required: true,
						include: {
							as: "district",
							required: true,
							attributes: ["id", "name", "regionId"],
							model: db.district,
						},
					},
					{
						model: Municipality,
						required: false,
						as: "crossingPoints",
						through: {
							as: "info",
							attributes: ["description"],
						},
						attributes: ["id", "name", "districtId"],
						include: {
							as: "district",
							attributes: ["id", "name", "regionId"],
							model: db.district,
						},
						order: ["id", "asc"],
					},
				],
				...filterOptions,
			};
			const trips = await Trip.findAll(options);

			const count = await Trip.count({
				include: [
					{
						model: Municipality,
						as: "startPlace",
						required: true,
						include: {
							model: db.district,
							as: "district",
							where: startPlaceWhere,
						},
					},
				],
				where: filterOptions.where,
			});

			res.json({
				success: true,
				count: trips.length,
				total: count,
				trips,
			});
		} catch (err) {
			res.status(500).json({ success: false, message: err.message });
		}
	},

	createTrip: async (req, res) => {
		const { user } = req.auth;

		const { name, description, date, startPlace, finishPlace, crossingPoints, tripType, url, distance, elevation, route } =
			req.body;

		const tripTypeRecord = await TripType.findByPk(tripType);
		if (!tripTypeRecord) return res.status(400).json({ success: false, message: "Neplatný typ výletu" });
		const startRecord = await Municipality.findByPk(startPlace);
		if (!startRecord) return res.status(404).json({ success: false, message: "Startovací bod nenalezen" });
		const finishRecord = await Municipality.findByPk(finishPlace);
		if (!finishRecord) return res.status(404).json({ success: false, message: "Cílový bod nenalezen" });

		try {
			await db.sequelize.transaction(async (t) => {
				const targetDirector = path.join(__dirname, "..", "files", "routes");
				let filename;
				if (typeof route == "object") {
					do filename = uuid() + ".json";
					while (fs.existsSync(path.join(targetDirector, filename)));
				}

				const trip = await Trip.create(
					{
						name,
						description,
						date,
						url,
						tripTypeId: tripTypeRecord.id,
						startPlaceId: startRecord.id,
						finishPlaceId: finishRecord.id,
						userId: user.id,
						distance: distance,
						elevation: elevation,
						route: filename,
					},
					{ transaction: t }
				);

				if (crossingPoints && crossingPoints.length > 0) {
					for (let i = 0; i < crossingPoints.length; i++) {
						const municipality = await Municipality.findByPk(crossingPoints[i].id);
						if (!municipality) throw new Error("Neplatný průchozí bod");

						const description = crossingPoints[i]?.description;
						await trip.addCrossingPoint(municipality, {
							through: { description: description },
							transaction: t,
						});
					}
				}

				try {
					fs.mkdirSync(targetDirector, { recursive: true });
					if (typeof route == "object") fs.writeFileSync(path.join(targetDirector, filename), JSON.stringify(route));
				} catch (e) {
					throw new Error("Chyba při ukládání trasy");
				}

				res.status(201).json({
					success: true,
					trip,
				});
			});
		} catch (err) {
			res.status(400).json({ success: false, message: err.message });
		}
	},
	updateTrip: async (req, res) => {
		const { user, roles } = req.auth;
		const { tripId } = req.params;

		const { name, description, date, startPlace, finishPlace, crossingPoints, tripType, url, distance, elevation, route } =
			req.body;

		try {
			const trip = await Trip.findByPk(tripId);
			if (!trip) return res.status(400).json({ success: false, message: "Neplatné ID výletu" });
			if (trip.userId != user.id && !roles.includes("ADMIN"))
				return res.status(403).json({ success: false, message: "Neoprávněný přístup" });

			const tripTypeRecord = await TripType.findByPk(tripType);
			if (!tripTypeRecord) return res.status(400).json({ success: false, message: "Neplatný typ výletu" });
			const startRecord = await Municipality.findByPk(startPlace);
			if (!startRecord) return res.status(404).json({ success: false, message: "Startovací bod nenalezen" });
			const finishRecord = await Municipality.findByPk(finishPlace);
			if (!finishRecord) return res.status(404).json({ success: false, message: "Cílový bod nenalezen" });

			await db.sequelize.transaction(async (t) => {
				const targetDirector = path.join(__dirname, "..", "files", "routes");
				let filename;
				if (route && route !== "delete" && typeof route === "object") {
					do filename = uuid() + ".json";
					while (fs.existsSync(path.join(targetDirector, filename)));
				}

				const options = {
					name,
					description,
					date,
					url,
					tripTypeId: tripTypeRecord.id,
					startPlaceId: startRecord.id,
					finishPlaceId: finishRecord.id,
					distance: distance,
					elevation: elevation,
				};

				if (route && route !== "delete") {
					options.route = filename;
				} else if (route === "delete") {
					options.route = null;
				}

				await trip.update(options, { transaction: t });

				await trip.setCrossingPoints([]);

				if (crossingPoints && crossingPoints.length > 0) {
					for (let i = 0; i < crossingPoints.length; i++) {
						const municipality = await Municipality.findByPk(crossingPoints[i].id);
						if (!municipality) throw new Error("Neplatný průchozí bod");

						const description = crossingPoints[i].description;
						await trip.addCrossingPoint(municipality, {
							through: { description: description },
							transaction: t,
						});
					}
				}
				if (route) {
					if (trip.route) {
						if (fs.existsSync(path.join(targetDirector, trip.route))) {
							fs.unlinkSync(path.join(targetDirector, trip.route));
						}
					}
					if (route !== "delete" && typeof route === "object") {
						try {
							fs.mkdirSync(targetDirector, { recursive: true });
							fs.writeFileSync(path.join(targetDirector, filename), JSON.stringify(route));
						} catch (e) {
							throw new Error("Chyba při ukládání trasy");
						}
					}
				}

				res.status(201).json({
					success: true,
					trip,
				});
			});
		} catch (err) {
			res.status(400).json({ success: false, message: err.message });
		}
	},
	deleteTrip: async (req, res) => {
		const { user, roles } = req.auth;
		const { tripId } = req.params;
		try {
			const trip = await Trip.findByPk(tripId);
			if (!trip) return res.status(404).json({ success: false, message: "Neplatné ID výletu" });
			if (trip.userId != user.id && !roles.includes("ADMIN"))
				return res.status(403).json({ success: false, message: "Neoprávněný přístup" });

			if (trip.route) {
				const routePath = path.join(__dirname, "..", "files", "routes", trip.route);
				if (fs.existsSync(routePath)) {
					fs.unlinkSync(routePath);
				}
			}

			await trip.destroy();
			res.json({
				success: true,
				message: `Výlet ${trip.name} (${trip.id}) úspěšně odstraněn`,
			});
		} catch (err) {
			res.status(500).json({ success: false, message: err.message });
		}
	},
};
