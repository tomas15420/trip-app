const db = require("../models");
const TripType = db.tripType;

module.exports = {
    getTripTypes: async (req, res) => {
        try{
            const types = await TripType.findAll({attirbutes: ['id', 'type']});
            res.json(types);
        }catch(e){
            res.status(500).json({success: false, message: e.message});
        }
        
    }
}