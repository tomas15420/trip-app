const { Op } = require("sequelize");
const db = require("../models");
const Municipality = db.municipality;
const District = db.district;
const Region = db.region;

module.exports = {
	getMunicipalities: async (req, res) => {
		let { filterOptions } = req;
		const { search } = req.query;
		if (search) {
			filterOptions.where.name = { [Op.like]: `%${search}%` };
		}
		try {
			const municipalities = await Municipality.findAll({
				attributes: { exclude: ["districtId"] },
				include: [
					{
						model: District,
						attributes: { exclude: ["regionId"] },
						include: [Region],
					},
				],
				...filterOptions,
			});

			const total = await Municipality.count({ where: filterOptions.where });
			res.json({
				success: true,
				count: municipalities.length,
				total,
				municipalities,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	getRegions: async (req, res) => {
		const { filterOptions } = req;
		try {
			const regions = await Region.findAll(filterOptions);
			res.json({
				success: true,
				count: regions.length,
				regions,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	getDistricts: async (req, res) => {
		const { filterOptions } = req;
		const { search } = req.query;

		if (search) {
			filterOptions.where.name = { [Op.like]: `%${search}%` };
		}

		let { withRegions } = req.query;
		try {
			const districts = await District.findAll({ include: withRegions !== undefined ? Region : undefined, ...filterOptions });
			const total = await District.count({ where: filterOptions.where });
			res.json({
				success: true,
				count: districts.length,
				total,
				districts,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},
};
