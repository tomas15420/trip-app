const db = require("../models");
const bcrypt = require("bcrypt");
const fs = require("fs");
const path = require("path");
const { Op } = require("sequelize");
const { v4: uuid } = require("uuid");
const User = db.user;
const District = db.district;
const Region = db.region;
const Role = db.role;

module.exports = {
	getUsers: async (req, res) => {
		let { include } = req.query;
		let { filterOptions } = req;
		const { search } = req.query;

		if (search) {
			filterOptions.where = {
				[Op.and]: [
					{
						[Op.or]: [
							{ username: { [Op.like]: "%" + search + "%" } },
							{ email: { [Op.like]: "%" + search + "%" } },
							{ firstname: { [Op.like]: "%" + search + "%" } },
							{ lastname: { [Op.like]: "%" + search + "%" } },
						],
					},
					{ ...filterOptions.where },
				],
			};
		}

		const { user, roles } = req.auth;
		if (include) include = include.toLowerCase().split(",");
		try {
			let attributes = [
				"id",
				"username",
				"firstname",
				"lastname",
				"registered",
				"avatar",
				"avatarPath",
				[db.sequelize.literal(`(SELECT COUNT(*) FROM trips WHERE userId=users.id)`), "createdTrips"],
			];
			if (user) {
				if (roles?.includes("ADMIN") || user?.id == req.params?.id) attributes.push("email");
				attributes.push([
					db.sequelize.literal(
						`(SELECT CASE WHEN EXISTS (SELECT * FROM followers WHERE followerId=${user.id} AND followingId=users.id) THEN "TRUE" ELSE "FALSE" END)`
					),
					"following",
				]);
			}

			let options = {
				attributes,
				...filterOptions,
				include: [],
			};

			if (include && include.includes("district")) {
				if (include.includes("region")) options.include.push({ model: District, include: Region });
				else options.include.push({ model: District });
			}
			if (include && roles.includes("HL_ADMIN") && include.includes("roles")) {
				options.include.push({
					model: Role,
					attributes: ["role"],
					through: { attributes: [] },
				});
			}
			const users = await User.findAll(options);
			const total = await User.count({ where: filterOptions.where });

			res.json({
				success: true,
				count: users.length,
				total,
				users,
			});
		} catch (e) {
			res.status(400).json({ success: false, message: e.message });
		}
	},
	updateUser: async (req, res) => {
		const { user, roles: userRoles } = req.auth;
		const { userId } = req.params;
		const { username, actualPassword, password, email, firstname, lastname, district, avatar, roles } = req.body;

		if (!firstname && lastname)
			return res.status(400).json({ success: false, message: "Křestní jméno musí být vyplněno když je zadáno příjmení" });

		try {
			const editUser = await User.findByPk(userId);
			if (!editUser) return res.status(404).json({ success: false, message: "Uživatel nenalezen" });

			if (editUser.id !== user.id && !userRoles?.includes("ADMIN"))
				return res.status(403).json({ success: false, message: "Neoprávněný přístup" });

			if (password && editUser.id === user.id && (!actualPassword || !(await bcrypt.compare(actualPassword, editUser.password))))
				return res.status(400).json({ success: false, message: "Zadejte správné aktuální heslo" });

			if (username) {
				const duplicate = await User.findOne({ where: { username } });
				if (duplicate && duplicate.id !== editUser.id)
					return res.status(400).json({
						success: false,
						message: "Uživatel s tímto uživatelským jménem již existuje",
					});
				editUser.username = username;
			}
			if (firstname || firstname === null) {
				editUser.firstname = firstname;
			}
			if (lastname || lastname === null) {
				editUser.lastname = lastname;
			}
			if (email) {
				const duplicate = await User.findOne({ where: { email } });
				if (duplicate && duplicate.id !== editUser.id)
					return res.status(400).json({
						success: false,
						message: "Uživatel s tímto E-mailem již existuje",
					});
				editUser.email = email;
			}
			if (password) {
				const hashedPssw = await bcrypt.hash(password, 10);
				editUser.password = hashedPssw;
			}
			if (district !== undefined) {
				if (district !== null) {
					const districtRecord = await District.findByPk(district);
					if (!districtRecord)
						return res.status(404).json({
							success: false,
							message: "Neplatný okres",
						});
					editUser.districtId = districtRecord.id;
				} else {
					editUser.districtId = null;
				}
			}
			let file;
			const prevAvatar = editUser.avatar;
			if ((avatar === "delete" || req.files?.avatar) && editUser.avatar) editUser.avatar = null;
			if (req.files) {
				file = req.files.avatar;
				if (file.truncated)
					return res.status(413).json({
						success: false,
						message: "Soubor je příliš velký",
					});
				if (!["image/png", "image/jpeg"].includes(file.mimetype))
					return res.status(422).json({
						success: false,
						message: "Nepodporovaný soubor",
					});
				const filename = uuid() + path.extname(file.name);
				editUser.avatar = filename;
			}
			await db.sequelize.transaction(async (t) => {
				await editUser.save({ transaction: t });
				if (roles && userRoles.includes("HL_ADMIN")) {
					let editRoles = [];
					for (const role of roles) {
						const roleRecord = await Role.findOne({
							where: { role },
						});
						if (!roleRecord) throw new Error(`Role ${role} je neplatná`);
						editRoles.push(roleRecord);
					}
					await editUser.setRoles(editRoles, { transaction: t });
				}
				if (prevAvatar && (avatar === "delete" || req.files?.avatar)) {
					const filePath = path.join(__dirname, "..", "images/avatars", prevAvatar);
					if (fs.existsSync(filePath)) {
						await fs.promises.unlink(filePath);
					}
				}
				if (file) {
					try {
						await file.mv(path.join(__dirname, "..", "images/avatars", editUser.avatar));
					} catch (e) {
						throw new Error("Nepodařilo se nahrát soubor");
					}
				}
			});

			const { password: _, ...updatedUser } = editUser.dataValues;
			res.json({ success: true, updatedUser });
		} catch (e) {
			res.status(400).json({ success: false, message: e.message });
		}
	},
};
