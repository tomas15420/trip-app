const path = require("path");
const sharp = require("sharp");
const fs = require("fs");

module.exports = {
	getImage: async (req, res) => {
		const { filename } = req.params;
		const filePath = path.join(__dirname, "..", "images", path.dirname(req.path), filename);

		if (!fs.existsSync(filePath))
			return res.status(404).json({ success: false, message: `Soubor ${path.dirname(req.path)}/${filename} neexistuje` });

		const width = parseInt(req.query.width) || null;
		const height = parseInt(req.query.height) || null;
		const quality = parseInt(req.query.quality) || 80;
		const fit = req.query.fit || "inside";

		const extension = path.extname(filename).toLowerCase();
		let format = "jpeg";
		if (extension === ".png") {
			format = "png";
		}

		try {
			const image = sharp(filePath);
			if (width || height) {
				image.resize(width, height, {
					fit,
					withoutEnlargement: true,
					background: { r: 255, g: 255, b: 255, alpha: 0 },
				});
			}

			const buffer = await image.toFormat(format, { quality }).toBuffer();
			res.set("Content-Type", `image/${format}`);
			res.send(buffer);
		} catch (e) {
			return res.status(500).json({ success: false, message: e.message });
		}
	},
};
