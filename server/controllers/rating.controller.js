const db = require("../models");
const Trip = db.trip;
const Rating = db.rating;
const User = db.user;

module.exports = {
	getTripRatings: async (req, res) => {
		const { filterOptions } = req;
		try {
			const { tripId } = req.params;
			const trip = await Trip.findByPk(tripId);
			if (!trip)
				return res.status(404).json({
					success: false,
					message: "Výlet nenalezen",
				});
			const order = filterOptions.order ? [[db.sequelize.col(filterOptions.order[0][0]), filterOptions.order[0][1]]] : undefined;

			const raters = await trip.getRaters({
				attributes: ["id", "username"],
				joinTableAttributes: ["id", "createdAt", "stars"],
				...filterOptions,
				order,
			});
			const total = await trip.countRaters();

			res.json({
				success: true,
				count: raters.length,
				total,
				raters,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	getUserRatings: async (req, res) => {
		const { filterOptions } = req;
		try {
			const { userId } = req.params;
			const user = await User.findByPk(userId);
			if (!user) return res.status(404).json({ success: false, message: "Uživatel nenalezen" });
			const order = filterOptions.order ? [[db.sequelize.col(filterOptions.order[0][0]), filterOptions.order[0][1]]] : undefined;

			const trips = await user.getRatedTrips({
				attributes: ["id", "name"],
				joinTableAttributes: ["id", "createdAt", "stars"],
				...filterOptions,
				order,
			});

			const total = await user.countRatedTrips();

			res.json({
				success: true,
				total,
				count: trips.length,
				trips,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	rateTrip: async (req, res) => {
		const { user } = req.auth;
		const { stars } = req.body;
		const { tripId } = req.params;
		try {
			const trip = await Trip.findByPk(tripId);
			if (!trip) return res.status(404).json({ success: false, message: "Výlet s tímto ID neexistuje" });

			if (new Date(trip.date).getTime() > new Date().getTime())
				return res.status(400).json({ success: false, message: "Výlet ještě neproběhl" });

			const rating = await Rating.findOne({
				where: { tripId: trip.id, userId: user.id },
			});

			if (!rating) {
				await trip.addRater(user, { through: { stars } });
			} else {
				await rating.update({ stars });
			}
			res.json({
				success: true,
				message: `Výlet ${trip.name} ohodnocen ${stars} hvězdičkami`,
			});
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},

	removeRating: async (req, res) => {
		const { user } = req.auth;
		const { tripId } = req.params;

		try {
			const trip = await Trip.findByPk(tripId);
			if (!trip) return res.status(404).json({ success: false, message: "Výlet s tímto ID neexistuje" });
			if (!(await user.hasRatedTrip(trip))) return res.status(400).json({ success: false, message: "Výlet jste nehodnotil" });
			await user.removeRatedTrip(trip);
			res.json({ success: true });
		} catch (e) {
			res.status(500).json({ success: false, message: e.message });
		}
	},
};
