const bcrypt = require("bcrypt");
const db = require("../models");
const User = db.user;
const RefreshToken = db.refreshToken;
const Role = db.role;
const { Op } = require("sequelize");
const { sign, verify, TokenExpiredError } = require("jsonwebtoken");

const refreshTokenExpiresTime = 14 * 24 * 60 * 60 * 1000;
const refreshTokenExpires = "14d";
const accessTokenExpires = "15m";

module.exports = {
	register: async (req, res) => {
		try {
			const { username, password, firstname, lastname, email } = req.body;
			if (!firstname && lastname)
				return res.status(400).json({ success: false, message: "Křestní jméno musí být vyplněno když je zadáno příjmení" });
			if (firstname && !lastname)
				return res.status(400).json({ success: false, message: "Příjmení musí být vyplněno, když je zadáno křestní jméno" });

			const foundUser = await User.findOne({
				where: {
					[Op.or]: [{ username: { [Op.like]: username } }, { email: { [Op.like]: email } }],
				},
			});

			if (foundUser)
				return res.status(400).json({
					success: false,
					message: "Účet s těmito údaji již existuje",
				});

			const hashedPssw = await bcrypt.hash(password, 10);

			await db.sequelize.transaction(async (t) => {
				const user = await User.create(
					{
						username,
						email,
						firstname,
						lastname,
						password: hashedPssw,
					},
					{ transaction: t }
				);

				let roles = await Role.findAll({
					where: user.id !== 1 ? { role: "USER" } : {},
				});
				if (!roles?.length) throw new Error("Uživatelské role nenalezeny");
				user.addRoles(roles, { transaction: t });

				roles = roles.map((role) => role.role);

				const accessToken = sign(
					{
						userId: user.id,
						username: user.username,
						email: user.email,
						roles,
					},
					process.env.ACCESS_TOKEN_SECRET,
					{ expiresIn: accessTokenExpires }
				);
				const refreshToken = sign({ userId: user.id }, process.env.REFRESH_TOKEN_SECRET, { expiresIn: refreshTokenExpires });
				const expiresIn = new Date().getTime() + refreshTokenExpiresTime;

				await RefreshToken.create(
					{
						token: refreshToken,
						expiration: expiresIn,
						userId: user.id,
					},
					{ transaction: t }
				);

				res.cookie("refreshToken", refreshToken, {
					httpOnly: true,
					sameSite: "None",
					maxAge: refreshTokenExpiresTime,
					secure: true,
				});

				res.json({
					success: true,
					accessToken,
					user: {
						username: user.username,
						userId: user.id,
						roles,
					},
				});
			});
		} catch (err) {
			res.status(500).json({ success: false, message: err.message });
		}
	},
	login: async (req, res) => {
		const { username, password } = req.body;
		const refreshToken = req.cookies?.refreshToken;

		try {
			const user = await User.findOne({
				where: {
					[Op.or]: [{ username: username }, { email: username }],
				},
			});

			if (!user)
				return res.status(404).json({
					success: false,
					message: "Uživatel s těmito údaji neexistuje",
				});
			if (!(await bcrypt.compare(password, user.password))) return res.status(401).json({ success: false, message: "Špatné heslo" });

			const roles = (await user.getRoles()).map((item) => item.role);
			const accessToken = sign(
				{
					userId: user.id,
					username: user.username,
					email: user.email,
					roles,
				},
				process.env.ACCESS_TOKEN_SECRET,
				{ expiresIn: accessTokenExpires }
			);
			const expiresIn = new Date().getTime() + refreshTokenExpiresTime;
			const newRefreshToken = sign({ userId: user.id }, process.env.REFRESH_TOKEN_SECRET, { expiresIn: refreshTokenExpires });

			await RefreshToken.create({
				token: newRefreshToken,
				expiration: expiresIn,
				userId: user.id,
			});

			res.cookie("refreshToken", newRefreshToken, {
				httpOnly: true,
				sameSite: "None",
				maxAge: refreshTokenExpiresTime,
				secure: true,
			});

			if (refreshToken) {
				const foundToken = await RefreshToken.findOne({
					token: refreshToken,
				});
				if (foundToken) {
					await foundToken.destroy();
				}
			}

			res.json({
				success: true,
				user: {
					username: user.username,
					userId: user.id,
					roles,
				},
				accessToken,
			});
		} catch (err) {
			res.status(500).json({ success: false, message: err.message });
		}
	},
	refreshToken: async (req, res) => {
		const refreshToken = req.cookies?.refreshToken;
		if (!refreshToken) return res.status(401).json({ success: false, message: "Neoprávněný přístup" });

		res.clearCookie("refreshToken", {
			httpOnly: true,
			sameSite: "None",
			secure: true,
		});

		try {
			const dbRefreshToken = await RefreshToken.findOne({
				where: { token: refreshToken },
			});

			if (!dbRefreshToken) {
				return res.status(401).json({
					success: false,
					message: "Neplatný refresh token",
				});
			}

			await RefreshToken.destroy({
				where: { id: dbRefreshToken?.id },
			});

			if (dbRefreshToken.expiration < new Date().getTime()) {
				return res.status(401).json({
					success: false,
					message: "Refresh token vypršel",
				});
			} else {
				const user = await User.findOne({
					where: { id: dbRefreshToken.userId },
				});

				const decoded = verify(refreshToken, process.env.REFRESH_TOKEN_SECRET);

				if (decoded?.userId !== user?.id)
					return res.status(401).json({
						success: false,
						message: "Neplatný refresh token",
					});

				const roles = (await user.getRoles()).map((item) => {
					return item.role;
				});
				const accessToken = sign(
					{
						userId: user.id,
						username: user.username,
						email: user.email,
						roles,
					},
					process.env.ACCESS_TOKEN_SECRET,
					{ expiresIn: accessTokenExpires }
				);
				const newRefreshToken = sign({ userId: user.id }, process.env.REFRESH_TOKEN_SECRET, { expiresIn: refreshTokenExpires });

				const expiresIn = new Date().getTime() + refreshTokenExpiresTime;
				await RefreshToken.create({
					token: newRefreshToken,
					expiration: expiresIn,
					userId: user.id,
				});

				res.cookie("refreshToken", newRefreshToken, {
					httpOnly: true,
					sameSite: "None",
					maxAge: refreshTokenExpiresTime,
					secure: true,
				});

				return res.json({
					accessToken,
					user: {
						username: user.username,
						userId: user.id,
						roles,
					},
				});
			}
		} catch (err) {
			if (err instanceof TokenExpiredError)
				return res.status(401).json({
					success: false,
					message: "Refresh token vypršel",
				});
			return res.status(500).json({ success: false, message: err.message });
		}
	},
	logout: async (req, res) => {
		const refreshToken = req.cookies?.refreshToken;
		if (!refreshToken) return res.status(401).json({ success: false, message: "Token nebyl poskytnut" });

		try {
			const dbRefreshToken = await RefreshToken.findOne({
				where: { token: refreshToken },
			});
			if (dbRefreshToken) {
				await RefreshToken.destroy({ where: { token: refreshToken } });
			}

			res.clearCookie("refreshToken", {
				httpOnly: true,
				sameSite: "None",
				secure: true,
			});

			res.json({
				success: true,
			});
		} catch (err) {
			res.status(500).json({ success: false, message: err.message });
		}
	},
};
